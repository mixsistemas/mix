export interface IPromocao {
  pro_pk: number;
  pro_fk_parceiro: number;
  pro_c_img?: string;
  pro_c_promocao: string;
  pro_f_de: number;
  pro_f_desconto?: number;
  pro_f_por: number;
}
