//#region 3rd
import { AfterContentInit, Component, OnDestroy, OnInit } from "@angular/core";
//#endregion

//#region 3rd
import * as $ from "jquery";
import { Subscription } from "rxjs";
//#endregion

//#region app services
import { BsCoreServices } from "./modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, AfterContentInit, OnDestroy {
  //#region publics
  sidemenu: boolean;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // Monitora solicitação de fechamento do sidemenu.
    this._subs.push(
      BsCoreServices.onSidemenuCloseEvent.subscribe((status: boolean) => {
        setTimeout(() => {
          // console.log(status);
          this.onCloseSidemenuClick();
        }, 0);
      })
    );
  }

  ngAfterContentInit() {
    this.sidemenu = $(".sidemenu").hasClass("sidemenu--is-visible");
    // this._checkActive();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region methods
  onCloseSidemenuClick() {
    $(".sidemenu").removeClass("sidemenu--is-visible");
    this.sidemenu = $(".sidemenu").hasClass("sidemenu--is-visible");
    // this._checkActive();
  }

  onToggleSidemenuClick() {
    $(".sidemenu").toggleClass("sidemenu--is-visible");
    this.sidemenu = $(".sidemenu").hasClass("sidemenu--is-visible");
    // this._checkActive();
  }
  //#endregion
}
