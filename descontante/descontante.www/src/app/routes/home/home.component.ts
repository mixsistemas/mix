//#region 3rd
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app models
import { IPromocao } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { BsCoreServices } from "../../modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "des-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss", "thumbs.scss"]
})
export class HomeComponent implements OnInit {
  //#region publics
  isMobile =
    Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <=
    768;
  promocoes: IPromocao[] = null;
  //#endregion

  //#region lifecycles
  ngOnInit() {
    BsCoreServices.onSidemenuCloseEvent.emit(true);

    this.promocoes = [
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 1,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "lojas-americanas.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S8",
        pro_fk_parceiro: 2,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "carrefour.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 3,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "casas-bahia.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 4,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "walmart.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 1,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "lojas-americanas.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S8",
        pro_fk_parceiro: 2,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "carrefour.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 3,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "casas-bahia.png"
      },
      {
        pro_pk: 1,
        pro_c_promocao: "Samsung Galaxy S5",
        pro_fk_parceiro: 4,
        pro_f_de: 980,
        pro_f_por: 490,
        pro_f_desconto: 50,
        pro_c_img: "walmart.png"
      }
    ];
  }
  //#endregion
}
