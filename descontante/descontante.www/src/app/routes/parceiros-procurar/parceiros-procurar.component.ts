//#region 3rd
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app services
import { BsCoreServices } from "../../modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "des-parceiros-procurar",
  templateUrl: "./parceiros-procurar.component.html",
  styleUrls: ["./parceiros-procurar.component.scss"]
})
export class ParceirosProcurarComponent implements OnInit {
  //#region lifecycles
  ngOnInit() {
    BsCoreServices.onSidemenuCloseEvent.emit(true);
  }
  //#endregion
}
