//#region 3rd
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app services
import { BsCoreServices } from "../../modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "des-parceiros-participar",
  templateUrl: "./parceiros-participar.component.html",
  styleUrls: ["./parceiros-participar.component.scss"]
})
export class ParceirosParticiparComponent implements OnInit {
  //#region lifecycles
  ngOnInit() {
    BsCoreServices.onSidemenuCloseEvent.emit(true);
  }
  //#endregion
}
