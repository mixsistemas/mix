//#region 3rd
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app services
import { BsCoreServices } from "../../modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "des-como-funciona",
  templateUrl: "./como-funciona.component.html",
  styleUrls: ["./como-funciona.component.scss"]
})
export class ComoFuncionaComponent implements OnInit {
  //#region lifecycles
  ngOnInit() {
    BsCoreServices.onSidemenuCloseEvent.emit(true);
  }
  //#endregion
}
