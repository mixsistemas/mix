//#region 3rd
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app services
import { BsCoreServices } from "../../modules/_bs/_core/_services";
//#endregion

@Component({
  selector: "des-contato",
  templateUrl: "./contato.component.html",
  styleUrls: ["./contato.component.scss"]
})
export class ContatoComponent implements OnInit {
  //#region lifecycles
  ngOnInit() {
    BsCoreServices.onSidemenuCloseEvent.emit(true);
  }
  //#endregion
}
