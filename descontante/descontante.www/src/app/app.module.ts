//#region ng
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
import { AppRoutingModule } from "./app-routing.module";
import { BsCoreModule } from "./modules/_bs/_core/bs-core.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import {
  ComoFuncionaComponent,
  ContatoComponent,
  HomeComponent,
  ParceirosParticiparComponent,
  ParceirosProcurarComponent
} from "./routes";
//#endregion

@NgModule({
  imports: [AppRoutingModule, BrowserModule, BsCoreModule],
  declarations: [
    AppComponent,
    ComoFuncionaComponent,
    ContatoComponent,
    HomeComponent,
    ParceirosParticiparComponent,
    ParceirosProcurarComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
