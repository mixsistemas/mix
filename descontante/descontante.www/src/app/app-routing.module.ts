//#region ng
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region app components
import {
  ComoFuncionaComponent,
  ContatoComponent,
  HomeComponent,
  ParceirosParticiparComponent,
  ParceirosProcurarComponent
} from "./routes";
//#endregion

//#region routes
const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "como-funciona",
    component: ComoFuncionaComponent
  },
  {
    path: "contato",
    component: ContatoComponent
  },
  {
    path: "parceiros-procurar",
    component: ParceirosProcurarComponent
  },
  {
    path: "parceiros-participar",
    component: ParceirosParticiparComponent
  },
  { path: "**", pathMatch: "full", redirectTo: "/home" }
];
//#endregion

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
