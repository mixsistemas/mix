// vars
var gulp = require("gulp");

var src = {
  shared: "_shared/**/*.*"
};

var dest = {
  shared: {
    www: "./descontante.com.br/src/app/modules"
  }
};

// tasks
gulp.task("shared", function() {
  gulp.src(src.shared).pipe(gulp.dest(dest.shared.www));
});

// watch task
gulp.task("default", function() {
  gulp.watch(src.shared, ["shared"]);
});
