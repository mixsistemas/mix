//#region ng
import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot
} from "@angular/router";
//#endregion

//#region ionic
import { Storage } from "@ionic/storage";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { IViaCep } from "../_mix/_models/_interfaces";
import { KEY_LOCAL } from "../_shared/_models/consts";
//#endregion

@Injectable({
  providedIn: "root"
})
export class LocalGuard implements CanActivate {
  //#region constructor
  constructor(private _router: Router, private _storage: Storage) {}
  //#endregion
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    // const OK = Math.random() > 0.5 ? true : false;
    return new Promise(resolve => {
      this._storage
        .get(KEY_LOCAL)
        .then((val: IViaCep) => {
          // console.log(val);
          if (!val) {
            this._router.navigate(["/sel-local"]);
            resolve(false);
          } else {
            resolve(true);
          } // else
        })
        .catch(err => {
          resolve(false);
        });
    });
  }
}
