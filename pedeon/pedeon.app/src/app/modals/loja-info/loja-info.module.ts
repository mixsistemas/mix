//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app components
import {
  LojaInfoAvaliacoesModalComponent,
  LojaInfoDetalhesModalComponent
} from ".";
//#endregion

//#region app modals
import { LojaInfoModalPage } from "..";
//#endregion

@NgModule({
  imports: [CommonModule, ComponentsModule, FormsModule, IonicModule],
  declarations: [
    // components
    LojaInfoAvaliacoesModalComponent,
    LojaInfoDetalhesModalComponent,
    // modals
    LojaInfoModalPage
  ],
  entryComponents: [LojaInfoModalPage]
})
export class LojaInfoModalModule {}
