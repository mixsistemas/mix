//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { LojaInfoTab } from "../../modules/_shared/_models/_enums";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-loja-info",
  templateUrl: "./loja-info.page.html",
  styleUrls: ["./loja-info.page.scss"]
})
export class LojaInfoModalPage implements OnInit {
  //#region comm
  @Input() tab: LojaInfoTab = LojaInfoTab.detalhes;
  @Input() loja: ILoja;
  //#endregion

  //#region publics
  Tab = LojaInfoTab;
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {}
  //#endregion

  //#region app methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }
  //#endregion
}
