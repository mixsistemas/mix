//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController } from "@ionic/angular";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IFormaPgto, ILoja } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { FormasPgtoService, HorariosService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-loja-info-detalhes",
  templateUrl: "./detalhes.component.html",
  styleUrls: ["./detalhes.component.scss"]
})
export class LojaInfoDetalhesModalComponent implements OnInit {
  //#region comm
  @Input() loja: ILoja;
  //#endregion

  //#region publics
  formas: IFormaPgto[];
  horarios: any;
  url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(
    private _formasServ: FormasPgtoService,
    private _horariosServ: HorariosService,
    public _loadingCtrl: LoadingController,
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      this._buscaHorarios();
      this._buscaFormas();
    }, 0);
  }
  //#endregion

  //#region functions
  private async _buscaHorarios() {
    // console.log(this.loja);
    if (this.loja) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._horariosServ.horarios(this.loja.loj_pk).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              // this.loja = this._lojasServ.fix(api.loja.data);
              this.horarios = api.data;
              [0, 1, 2, 3, 4, 5, 6].forEach(v => {
                // console.log(v);
                this.horarios.days[v].str = "";
                this.horarios.days[v].ints.forEach(
                  (s: string) => (this.horarios.days[v].str += s + "\n")
                );
              });
              // console.log(this.horarios);
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _buscaFormas() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this.formas = null;
      this._formasServ.formas(this.loja.loj_pk).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          loading.dismiss();
          if (api.ok) {
            this.formas = this._formasServ.fixes(api.data);
          } // if
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }
  //#endregion
}
