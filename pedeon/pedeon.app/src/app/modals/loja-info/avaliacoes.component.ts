//#region ng
import { Component, Input } from "@angular/core";
//#endregion

//#region app models
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-loja-info-avaliacoes",
  templateUrl: "./avaliacoes.component.html",
  styleUrls: ["./avaliacoes.component.scss"]
})
export class LojaInfoAvaliacoesModalComponent {
  //#region comm
  @Input() loja: ILoja;
  //#endregion
}
