//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
// import { ILoja } from "../../modules/_shared/_models/_interfaces";
// import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
// import { IonicService } from "../../modules/_mix/_core/_services";
import { HorariosService, StorageService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-horario-form",
  templateUrl: "./horario-form.page.html",
  styleUrls: ["./horario-form.page.scss"]
})
export class HorarioFormModalPage implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() diaSemana: number;
  //#endregion

  //#region publics
  horarioForm: FormGroup;
  horarios: any;
  intervalos: {
    inicio: number;
    final: number;
  } = {
    inicio: null,
    final: null
  };
  inverted: boolean = false;
  jwt: string;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _horariosServ: HorariosService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {
    this.horarioForm = formBuilder.group({
      iHoras: ["0", Validators.compose([Validators.required])],
      iMinutos: ["0", Validators.compose([Validators.required])],
      fHoras: ["23", Validators.compose([Validators.required])],
      fMinutos: ["59", Validators.compose([Validators.required])]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.onChanges();
    });
  }
  //#endregion

  //#region mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  async onSubmit() {
    // console.log(this.horarioForm.value);
    // console.log(this.idLoja);
    // console.log(this.diaSemana);
    // console.log(this.jwt);
    // console.log(this.intervalos);
    if (
      this.intervalos &&
      this.idLoja > 0 &&
      (this.diaSemana >= 0 && this.diaSemana < 7) &&
      this.jwt
    ) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._horariosServ
          .novo(
            this.idLoja,
            this.diaSemana,
            this.intervalos.inicio,
            this.intervalos.final,
            this.jwt
          )
          .subscribe(
            (api: any) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this.modalCtrl.dismiss();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // endif
  }

  onChanges() {
    // console.log(this.intervalos);
    if (this.intervalos) {
      this.intervalos.inicio = null;
      this.intervalos.final = null;
      let iHoras = parseInt(this.horarioForm.controls.iHoras.value);
      let iMinutos = parseInt(this.horarioForm.controls.iMinutos.value);
      let fHoras = parseInt(this.horarioForm.controls.fHoras.value);
      let fMinutos = parseInt(this.horarioForm.controls.fMinutos.value);
      if (isNaN(iHoras)) iHoras = -1;
      if (isNaN(iMinutos)) iMinutos = -1;
      if (isNaN(fHoras)) fHoras = -1;
      if (isNaN(fMinutos)) fMinutos = -1;
      if (iHoras >= 0 && iMinutos >= 0) {
        this.intervalos.inicio = iHoras * 60 + iMinutos;
      } // if
      if (fHoras >= 0 && fMinutos >= 0) {
        this.intervalos.final = fHoras * 60 + fMinutos;
      } // if
      // console.log(iHoras, iMinutos, fHoras, fMinutos);
      // console.log(this.intervalos);
      if (this.intervalos.inicio && this.intervalos.final) {
        this.inverted = this.intervalos.inicio > this.intervalos.final;
      } else {
        this.inverted = false;
      } // else
    } // if
  }
  //#endregion
}
