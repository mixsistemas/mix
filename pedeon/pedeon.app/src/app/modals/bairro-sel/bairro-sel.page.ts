//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { IBairro, ISetor } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
// import { IonicService } from "../../modules/_mix/_core/_services";
import { BairrosService, StorageService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-bairro-sel",
  templateUrl: "./bairro-sel.page.html",
  styleUrls: ["./bairro-sel.page.scss"]
})
export class BairroSelModalPage implements OnInit {
  //#region comm
  @Input() setor: ISetor;
  //#endregion

  //#region publics
  bairros: IBairro[];
  filter: string;
  jwt: string;
  //#endregion

  //#region constructor
  constructor(
    private _bairrosServ: BairrosService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.onRefresh();
    });
  }
  //#endregion

  //#region methods
  async onRefresh() {
    // console.log("onRefresh");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // console.log(this.setor);
    // console.log(this.jwt);

    loading.present().then(() => {
      this.bairros = null;
      this._bairrosServ
        .bairrosSetor(this.setor.set_pk, this.jwt, this.filter)
        .subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.bairros = api.data || [];
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }

  onLimpaProcura() {
    this.filter = "";
    this.onRefresh();
  }

  onModalClose() {
    this.modalCtrl.dismiss();
  }
  onBairroSel(bairro: IBairro) {
    // console.log(bairro);
    this.modalCtrl.dismiss(bairro);
  }
  //#endregion
}
