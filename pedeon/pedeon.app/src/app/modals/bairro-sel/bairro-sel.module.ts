//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modals
import { BairroSelModalPage } from "..";
//#endregion

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [
    // components
    // modals
    BairroSelModalPage
  ],
  entryComponents: [BairroSelModalPage]
})
export class BairroSelModalModule {}
