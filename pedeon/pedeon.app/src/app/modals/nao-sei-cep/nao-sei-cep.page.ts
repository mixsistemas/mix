//#region ng
import { Component, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app modals
import {
  IModalResponse,
  IViaCep
} from "../../modules/_mix/_models/_interfaces";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  CepService,
  IonicService,
  UtilsService
} from "../../modules/_mix/_core/_services";
import { StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import { LocalFormModalPage } from "../../modals/local-form/local-form.page";
//#endregion

@Component({
  selector: "po-nao-sei-cep",
  templateUrl: "./nao-sei-cep.page.html",
  styleUrls: ["./nao-sei-cep.page.scss"]
})
export class NaoSeiCepModalPage {
  //#region comm
  @Input() value: any;
  //#endregion

  //#region publics
  enderecoForm: FormGroup;
  logradouros: IViaCep[];
  //#endregion

  //#region constructor
  constructor(
    private _cepServ: CepService,
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _router: Router,
    private _storageServ: StorageService,
    private _utilsServ: UtilsService
  ) {
    this.enderecoForm = formBuilder.group({
      estado: ["MG", Validators.required],
      cidade: ["Uberaba", Validators.required],
      logradouro: ["", Validators.required]
    });
  }
  //#endregion

  //#region methods
  async onSubmit() {
    // console.log(this.enderecoForm.value);
    const {
      estado: ESTADO,
      cidade: CIDADE,
      logradouro: LOGRADOURO
    } = this.enderecoForm.value;

    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._cepServ.logradouros(ESTADO, CIDADE, LOGRADOURO).subscribe(
        (api: IViaCep[]) => {
          // console.log(api);
          this.logradouros = api;
          if (this.logradouros.length == 0) {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // if
          loading.dismiss();
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  onModalClose() {
    this.modalCtrl.dismiss();
  }

  async onLocalClick(logradouro: IViaCep) {
    this.modalCtrl.dismiss();

    const modal = await this.modalCtrl.create({
      component: LocalFormModalPage,
      componentProps: { cepInfo: logradouro }
    });

    modal.onDidDismiss().then((data: IModalResponse) => {
      if (data.data) {
        let local: IViaCep = data.data;
        local.key = this._utilsServ.guid();
        // console.log(local);
        this._storageServ.localSet(local, () => {
          this._storageServ.localAdd(local);
          this._router.navigate(["/sel-loja"]);
        });
      } // if
    });

    // this.toast("Local selecionado com sucesso.");
    return await modal.present();
  }
  //#endregion
}
