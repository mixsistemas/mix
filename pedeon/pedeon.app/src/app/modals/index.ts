export * from "./area-lojista/area-lojista.page";
export * from "./bairro-sel/bairro-sel.page";
export * from "./detalhe-produto-form/detalhe-produto-form.page";
export * from "./grupo-produtos-edit/grupo-produtos-edit.page";
export * from "./horario-form/horario-form.page";
export * from "./item-detalhe-produto-form/item-detalhe-produto-form.page";
export * from "./local-form/local-form.page";
export * from "./login-cadastro/login-cadastro.page";
export * from "./loja-info/loja-info.page";
export * from "./lojas-favoritas/lojas-favoritas.page";
export * from "./nao-sei-cep/nao-sei-cep.page";
export * from "./perguntas-frequentes/perguntas-frequentes.page";
export * from "./procura-geral/procura-geral.page";
export * from "./produto-form/produto-form.page";
export * from "./produto-ok/produto-ok.page";
export * from "./produtos-favoritos/produtos-favoritos.page";
export * from "./setor-form/setor-form.page";
export * from "./seus-enderecos/seus-enderecos.page";
export * from "./seu-pedido/seu-pedido.page";
export * from "./seus-pedidos/seus-pedidos.page";
export * from "./sua-conta/sua-conta.page";
export * from "./zoom-img/zoom-img.page";
