//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { IGrupoProdutos } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import { GruposProdutosService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-grupo-produtos-edit",
  templateUrl: "./grupo-produtos-edit.page.html",
  styleUrls: ["./grupo-produtos-edit.page.scss"]
})
export class GrupoProdutosEditModalPage implements OnInit {
  //#region comm
  @Input() grupo: IGrupoProdutos;
  //#endregion

  //#region private
  fv: FormValidation;
  grupoForm: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _gruposServ: GruposProdutosService,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.grupoForm = formBuilder.group({
      grupo: [
        "",
        Validators.compose([Validators.maxLength(20), Validators.required])
      ],
      ativo: [""]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      // console.log(this.grupo);
      this.grupoForm.patchValue({
        grupo: this.grupo.gpr_c_grupo,
        ativo: this.grupo.gpr_b_ativo
      });
    }, 0);
  }
  //#endregion

  //#region functions
  private async _gravaGrupo() {
    // console.log("_gravaGrupo");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.grupo.gpr_c_grupo = this.grupoForm.value.grupo;
      this.grupo.gpr_b_ativo = this.grupoForm.value.ativo;

      loading.present().then(() => {
        this._gruposServ.update(this.grupo, token).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              this.modalCtrl.dismiss();
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    });
  }

  /* private async _buscaLoja(id: number) {
    // console.log(id);
    this.loja = null;
    if (id > 0) {
      let loading = await this.loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._lojasServ
          .loja(id)
          .pipe(
            mergeMap((loja: IApiResponse) => {
              return this._intervalosLojasServ
                .intervalos(loja.data.loj_pk)
                .pipe(
                  map((intervalos: IApiResponse) => {
                    return {
                      loja: loja,
                      intervalos: intervalos
                    };
                  })
                );
            })
          )
          .subscribe(
            (api: any) => {
              console.log(api);
              loading.dismiss();
              if (api.intervalos.ok) {
                this.loja = this._lojasServ.fix(api.loja.data);
                this.intervalos = api.intervalos.data;
                [0, 1, 2, 3, 4, 5, 6].forEach(v => {
                  console.log(v);
                  this.intervalos.days[v].str = "";
                  this.intervalos.days[v].ints.forEach(
                    (s: string) => (this.intervalos.days[v].str += s + "\n")
                  );
                });
                console.log(this.intervalos);
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // if
  } */
  //#endregion

  //#region app mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    // console.log("onSubmit");
    this._gravaGrupo();
  }
  //#endregion
}
