//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import {
  ActionSheetController,
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region app models
import {
  IApiResponse,
  IModalResponse
} from "../../modules/_mix/_models/_interfaces";
import { ISetor } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { IonicService } from "../../modules/_mix/_core/_services";
import {
  // BairrosService,
  SetoresService,
  StorageService
} from "../../modules/_services";
//#endregion

//#region app modals
import { BairroSelModalPage, SetorFormModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-area-lojista-setores-entrega",
  templateUrl: "setores-entrega.component.html",
  styleUrls: ["setores-entrega.component.scss"]
})
export class AreaLojistaSetoresEntregaComponent implements OnInit {
  //#region comm
  @Input() idLoja: number;
  //#endregion

  //#region publics
  all: any;
  setores: any;
  jwt: string;
  //#endregion

  //#region constructor
  constructor(
    private _actionCtrl: ActionSheetController,
    // private _bairrosServ: BairrosService,
    private _ionicServ: IonicService,
    public _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _setoresServ: SetoresService,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._refresh();
    });
  }
  //#endregion

  //#region functions
  private async _refresh(onSuccess?: any) {
    // console.log(id);
    this.setores = null;
    if (this.idLoja > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._setoresServ.setores(this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              let setores = api.data.rows;
              this.all = api.data.all;
              // Gera accordion.
              if (setores) {
                setores.forEach(s => {
                  s.expanded = false;
                  s.bairros = s.bairros || [];
                  // console.log(s);
                });
              } // if
              this.setores = this._setoresServ.fixes(setores);
              // console.log(this.setores);
              if (onSuccess && typeof onSuccess === "function") {
                onSuccess();
              } // if */
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _setorDel(setor: ISetor) {
    // console.log(setor.set_pk, this.jwt);
    if (setor.set_pk > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this._setoresServ.del(setor.set_pk, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast("Setor eliminado com sucesso.", "danger");
                this._refresh();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  private async _adicionarBairro(bairro: string, setor: ISetor) {
    // console.log(setor.set_pk, this.jwt);
    if (bairro && setor && this.jwt) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      if (this.jwt) {
        loading.present().then(() => {
          this._setoresServ.novoBairro(bairro, setor, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                // this._ionicServ.toast("Setor eliminado com sucesso.", "danger");
                this._refresh(() => {
                  this._setorOpen(setor.set_pk);
                });
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  private _setorOpen(idSetor: number) {
    this.setores.forEach(e => (e.expanded = e.set_pk == idSetor));
  }

  private async _setorEdit(setor: ISetor) {
    // console.log(this.idLoja);
    const modal = await this.modalCtrl.create({
      component: SetorFormModalPage,
      componentProps: { idLoja: this.idLoja, setor: setor }
    });

    modal.onDidDismiss().then(() => {
      this._refresh();
    });

    return await modal.present();
  }

  private async _selBairro(setor: ISetor) {
    const modal = await this.modalCtrl.create({
      component: BairroSelModalPage,
      componentProps: { setor: setor }
    });

    modal.onDidDismiss().then((resp: IModalResponse) => {
      if (resp && resp.data) {
        this._adicionarBairro(resp.data.bai_c_bairro, setor);
      } // if
    });

    return await modal.present();
  }
  //#endregion

  //#region methods
  async onMenuClick(setor: ISetor) {
    // console.log(setor);
    if (setor) {
      // Inicia array de buttons.
      let buttons: any[] = [
        {
          text: "Modificar setor",
          // role: "destructive",
          icon: "/assets/icon/edit.svg",
          handler: () => {
            this._setorEdit(setor);
          }
        }
      ];

      // Só permite exclusão de setores vazios.
      if (setor.bairros.length == 0) {
        buttons.push({
          text: "Excluir setor",
          icon: "/assets/icon/trash.svg",
          handler: () => {
            if (setor) {
              this._setorDel(setor);
            } // if
          }
        });
      } // else

      // Completa array de buttons.
      buttons.push(
        {
          text: "Adicionar bairro",
          // role: "destructive",
          icon: "/assets/icon/plus.svg",
          handler: () => {
            this._selBairro(setor);
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            // console.log("Cancel clicked");
          }
        }
      );

      const actionSheet = await this._actionCtrl.create({
        header: setor.set_c_setor,
        buttons: buttons
      });
      await actionSheet.present();
    } // if
  }

  async onNovoSetorClick() {
    // console.log(this.idLoja);
    const modal = await this.modalCtrl.create({
      component: SetorFormModalPage,
      componentProps: { idLoja: this.idLoja, setor: null }
    });

    modal.onDidDismiss().then(() => {
      this._refresh();
    });

    return await modal.present();
  }

  async onAllToggle() {
    // console.log(this.idLoja, this.all);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt && this.idLoja > 0) {
      loading.present().then(() => {
        this._setoresServ
          .toggleAll(this.idLoja, this.all.status, this.all.value, this.jwt)
          .subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // if
  }

  async onAllValorBlur() {
    // console.log(this.idLoja, this.all);
    if (this.jwt && this.idLoja > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      loading.present().then(() => {
        this._setoresServ
          .toggleAll(this.idLoja, this.all.status, this.all.value, this.jwt)
          .subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              this._ionicServ.toast('Valor de taxa geral atualizado.');
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // if
  }

  async onBairroDelClick(idBairro: number) {
    // console.log(idBairro, this.jwt);

    if (idBairro > 0 && this.jwt) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      loading.present().then(() => {
        this._setoresServ.bairroDel(idBairro, this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            console.log(api);
            loading.dismiss();
            if (api.ok) {
              this._ionicServ.toast("Bairro eliminado com sucesso.", "danger");
              this._refresh();
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion
}
