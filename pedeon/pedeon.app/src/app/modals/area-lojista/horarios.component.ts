//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
// import { IHorario } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { IonicService } from "../../modules/_mix/_core/_services";
import { HorariosService, StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import { HorarioFormModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-area-lojista-horarios",
  templateUrl: "horarios.component.html",
  styleUrls: ["horarios.component.scss"]
})
export class AreaLojistaHorariosComponent implements OnInit {
  //#region comm
  @Input() idLoja: number;
  //#endregion

  //#region publics
  horarios: any;
  jwt: string;
  //#endregion

  //#region constructor
  constructor(
    private _horariosServ: HorariosService,
    private _ionicServ: IonicService,
    public _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._refresh();
    });
  }
  //#endregion

  //#region functions
  private async _refresh(onSuccess?: any) {
    // console.log(id);
    this.horarios = null;
    if (this.idLoja > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._horariosServ.horarios(this.idLoja).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              let horarios = api.data;
              // Gera accordion.
              [0, 1, 2, 3, 4, 5, 6].forEach(v => {
                // console.log(horarios.days[v].dlong);
                horarios.days[v].expanded = false;
              });
              this.horarios = horarios;
              // console.log(this.horarios);
              if (onSuccess && typeof onSuccess === "function") {
                onSuccess();
              } // if
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  onExpandClick(status: boolean = true) {
    // console.log(status);
    if (this.horarios) {
      [0, 1, 2, 3, 4, 5, 6].forEach(
        v => (this.horarios.days[v].expanded = status)
      );
    } // if
  }

  async onNovoClick(hor: number) {
    // console.log(hor);
    this.horarios.days[hor].expanded = true;

    const modal = await this.modalCtrl.create({
      component: HorarioFormModalPage,
      componentProps: { idLoja: this.idLoja, diaSemana: hor }
    });

    modal.onDidDismiss().then(() => {
      this._refresh(() => {
        this.horarios.days[hor].expanded = true;
      });
    });

    return await modal.present();
  }

  async onIntervaloDelClick(idIntervalo: number) {
    // console.log(idIntervalo, this.jwt);

    if (idIntervalo > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this._horariosServ.del(idIntervalo, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast(
                  "Horário eliminado com sucesso.",
                  "danger"
                );
                this._refresh();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }
  //#endregion
}
