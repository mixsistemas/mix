//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import {
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region app models
import { IProduto } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService, StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import { ProdutoFormModalPage, ZoomImgModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-area-lojista-produtos",
  templateUrl: "produtos.component.html",
  styleUrls: ["produtos.component.scss"]
})
export class AreaLojistaProdutosComponent implements OnInit {
  //#region comm
  @Input() idLoja: number = 0;
  //#endregion

  //#region publics
  filter: string;
  jwt: string;
  produtos: IProduto[];
  //#endregion

  //#region constructor
  constructor(
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.onRefresh();
    });
  }
  //#endregion

  //#region functions
  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }

  private async _produtoForm(idProduto: number) {
    // console.log("_grupoEdit");
    const modal = await this.modalCtrl.create({
      component: ProdutoFormModalPage,
      componentProps: { idProduto: idProduto, idLoja: this.idLoja }
    });

    modal.onDidDismiss().then(() => {
      this.onRefresh();
    });

    return await modal.present();
  }
  //#endregion

  //#region methods
  onLimpaProcura() {
    this.filter = "";
    this.onRefresh();
  }

  async onRefresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.produtos = null;
        this._produtosServ
          .produtosJwt(this.idLoja, this.jwt, this.filter)
          .subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this.produtos = this._produtosServ.fixes(api.data) || [];
                // console.log(this.produtos);
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // if
  }

  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }

  onProdutoEdit(produto: IProduto) {
    this._produtoForm(produto.pro_pk);
  }

  onNovoClick() {
    this._produtoForm(0);
  }
  //#endregion
}
