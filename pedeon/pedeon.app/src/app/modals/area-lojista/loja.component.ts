//#region ng
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-area-lojista-loja",
  templateUrl: "loja.component.html",
  styleUrls: ["loja.component.scss"]
})
export class AreaLojistaLojaComponent implements OnInit {
  //#region publics
  fv: FormValidation;
  lojaForm: FormGroup;
  //#endregion

  //#region constructor
  constructor(public formBuilder: FormBuilder) {
    this.fv = new FormValidation();

    this.lojaForm = formBuilder.group({
      loja: [
        "",
        Validators.compose([Validators.maxLength(40), Validators.required])
      ]
    });

    /* 
    loj_c_loja: string;
    loj_fk_tipo: number;
    loj_c_bairro: string;
    loj_c_cep: string;
    loj_c_complemento: string;
    loj_c_localidade: string;
    loj_c_logradouro: string;
    loj_c_tipo?: string;
    loj_c_uf: string;
    loj_f_media_avaliacoes: number;
    loj_f_media_despacho: number;
    loj_i_nro: number;
    taxa_entrega: number;
 */
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {}
  //#endregion

  //#region methods
  onSubmit() {}
  //#endregion
}
