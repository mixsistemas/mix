export * from "./formas-pgto.component";
export * from "./horarios.component";
export * from "./loja.component";
export * from "./grupos-produtos.component";
export * from "./produtos.component";
export * from "./sel-loja.component";
export * from "./setores-entrega.component";
