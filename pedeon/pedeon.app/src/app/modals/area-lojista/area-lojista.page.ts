//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
// import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
import { IAuthJwt, ILoja } from "../../modules/_shared/_models/_interfaces";
interface IAreaLojistaTab {
  id: string;
  caption: string;
}
//#endregion

//#region app services
import {
  AuthService,
  LojasService,
  StorageService
} from "../../modules/_services";
//#endregion

//#region app modals
// import { LoginCadastroModalPage } from "../../modals/login-cadastro/login-cadastro.page";
//#endregion

@Component({
  selector: "po-area-lojista",
  templateUrl: "./area-lojista.page.html",
  styleUrls: ["./area-lojista.page.scss"]
})
export class AreaLojistaModalPage implements OnInit {
  //#region comm
  // @Input() value: any;
  //#endregion

  //#region publics
  incluirLoja: boolean = false;
  conta: IAuthJwt;
  jwt: string;
  loja: ILoja;
  slideOpts = {
    slidesPerView: "4.5",
    breakpoints: {
      768: {
        slidesPerView: 2.5
      },
      992: {
        slidesPerView: 3.5
      },
      1200: {
        slidesPerView: 4.5
      }
    }
  };
  tabSel: string;
  tabs: IAreaLojistaTab[] = [
    // { id: "loj", caption: "Loja" },
    { id: "pro", caption: "Produtos" },
    { id: "gpr", caption: "Grupos produtos" },
    { id: "hor", caption: "Horários" },
    { id: "fpg", caption: "Formas pgto" },
    { id: "set", caption: "Setores entrega" }
  ];
  //#endregion

  //#region idLoja
  private _idLoja: number = 0;
  set idLoja(val: number) {
    // console.log(val);
    this._idLoja = val;
    this._buscaLoja(val);
  }
  get idLoja(): number {
    return this._idLoja;
  }
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.tabSel = this.tabs["0"].id;
    this._refresh();
  }
  //#endregion

  //#region functions
  private async _refresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.jwt = token;
      this.conta = null;

      if (this.jwt) {
        loading.present().then(() => {
          this._authServ.checkToken(token).subscribe(
            (api: any) => {
              // console.log(api);
              loading.dismiss();
              if (api) {
                this.conta = api;
                this.conta.lojas = this.conta.lojas || [];
                if (this.conta.lojas.length == 1) {
                  this.idLoja = this.conta.lojas[0];
                } // if
              } // if
              this._storageServ.onAuthChanged.next(this.conta);
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    });
  }

  private async _buscaLoja(idLoja: number) {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    this.loja = null;
    if (this.jwt && idLoja > 0) {
      loading.present().then(() => {
        this._lojasServ.loja(idLoja, "*", "[]", this.jwt).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.loja = this._lojasServ.fix(api.data);
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion

  //#region methods
  onSelTabClick(tab: IAreaLojistaTab) {
    // console.log(cat);
    this.tabSel = tab.id;
  }

  onModalClose() {
    this.modalCtrl.dismiss();
  }

  /* async onLoginCadastroClick() {
    // console.log("onLoginCadastroClick");
    // this.modalCtrl.dismiss();
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: LoginCadastroTab.login }
    });

    modal.onDidDismiss().then(() => {
      this._refresh();
    });

    return await modal.present();
  } */

  onLogin(local: IViaCep) {
    // console.log(local);
    if (local) {
      this._storageServ.localSet(local);
    } // if
    this._refresh();
  }

  onSelClick(idLoja: number) {
    // console.log(idLoja);
    if (idLoja) {
      this.idLoja = idLoja;
    } // if
  }
  //#endregion
}
