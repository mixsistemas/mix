//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region ionic
import { LoadingController } from "@ionic/angular";
//#endregion

//#region app models
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { LojasService } from "../../modules/_services";

@Component({
  selector: "po-area-lojista-sel-loja",
  templateUrl: "sel-loja.component.html",
  styleUrls: ["sel-loja.component.scss"]
})
export class AreaLojistaSelLojaComponent implements OnChanges, OnInit {
  //#region comm
  @Input() jwt: string;
  @Output() onSelected: EventEmitter<number> = new EventEmitter<number>();
  //#endregion

  //#region publics
  lojas: ILoja[];
  //#endregion

  //#region constructor
  constructor(
    private _lojasServ: LojasService,
    private _loadingCtrl: LoadingController
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    // console.log(this.jwt);
    this._refresh();
  }

  private async _refresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // this.conta = null;
    if (this.jwt) {
      loading.present().then(() => {
        this._lojasServ.lojasConta(this.jwt).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.lojas = this._lojasServ.fixes(api.data);
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  ngOnInit() {}
  //#endregion

  //#region methods
  onSelClick(loja: ILoja) {
    // console.log(loja);
    if (loja) {
      this.onSelected.emit(loja.loj_pk);
    } // if
  }
  //#endregion
}
