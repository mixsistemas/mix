//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import {
  ActionSheetController,
  AlertController,
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region app models
import { IGrupoProdutos } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { GruposProdutosService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { GrupoProdutosEditModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-area-lojista-grupos-produtos",
  templateUrl: "grupos-produtos.component.html",
  styleUrls: ["grupos-produtos.component.scss"]
})
export class AreaLojistaGruposProdutosComponent implements OnInit {
  //#region comm
  @Input() idLoja: number = 0;
  //#endregion

  //#region publics
  // conta: IAuthJwt;
  filter: string;
  jwt: string;
  grupos: IGrupoProdutos[];
  //#endregion

  //#region constructor
  constructor(
    private _actionCtrl: ActionSheetController,
    private _alertCtrl: AlertController,
    private _gruposServ: GruposProdutosService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService,
    private _ionicServ: IonicService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.onRefresh();
    });
  }
  //#endregion

  //#region functions
  private async _grupoDel(id: number) {
    if (id > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this._gruposServ.del(id, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast("Grupo eliminado com sucesso.", "danger");
                this.onRefresh();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  private async _grupoEdit(grupo: IGrupoProdutos) {
    // console.log("_grupoEdit");
    const modal = await this.modalCtrl.create({
      component: GrupoProdutosEditModalPage,
      componentProps: { grupo: grupo }
    });

    modal.onDidDismiss().then(() => {
      this.onRefresh();
    });

    return await modal.present();
  }
  //#endregion

  //#region methods
  async onRefresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.grupos = null;
        this._gruposServ
          .gruposJwt(this.idLoja, this.jwt, this.filter)
          .subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                let grupos: IGrupoProdutos[] =
                  this._gruposServ.fixes(api.data) || [];
                // console.log(grupos);
                const L: number = grupos.length;
                // Calcula posições anteriores.
                var v: number = 0;
                for (let i = 0; i < L; i++) {
                  // console.log(i, v);
                  grupos[i].prior = v;
                  v = grupos[i].gpr_pk;
                } // for
                // Calcula posições posteriores.
                v = 0;
                for (let i = L - 1; i >= 0; i--) {
                  // console.log(i, v);
                  grupos[i].next = v;
                  v = grupos[i].gpr_pk;
                } // for
                this.grupos = grupos;
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    } // if
  }

  onLimpaProcura() {
    this.filter = "";
    this.onRefresh();
  }

  async onSwapPosClick(grupo: IGrupoProdutos, up: boolean = true) {
    // console.log(up, grupo.gpr_pk, grupo.prior, grupo.next);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    if (up) {
      // console.log(grupo.gpr_pk, grupo.prior);
      if (grupo.gpr_pk > 0 && grupo.prior > 0) {
        loading.present().then(() => {
          this._gruposServ
            .togglePos(this.idLoja, grupo.gpr_pk, grupo.prior, this.jwt)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                if (api.ok) {
                  this.onRefresh();
                } // if
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      } // if;
    } else {
      // console.log(grupo.gpr_pk, grupo.next);
      if (grupo.gpr_pk > 0 && grupo.next > 0) {
        loading.present().then(() => {
          this._gruposServ
            .togglePos(this.idLoja, grupo.gpr_pk, grupo.next, this.jwt)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                if (api.ok) {
                  this.onRefresh();
                } // if
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      } // if;
    } // else
  }

  async onMenuClick(grupo: IGrupoProdutos) {
    // console.log(grupo);
    if (grupo) {
      // Inicia array de buttons.
      let buttons: any[] = [
        {
          text: "Modificar",
          // role: "destructive",
          icon: "/assets/icon/edit.svg",
          handler: () => {
            this._grupoEdit(grupo);
          }
        }
      ];

      // Só permite exclusão de grupos vazios.
      if (grupo.gpr_i_num_produtos == 0) {
        buttons.push({
          text: "Excluir",
          icon: "/assets/icon/trash.svg",
          handler: () => {
            this._grupoDel(grupo.gpr_pk);
          }
        });
      } // else

      // Completa array de buttons.
      buttons.push({
        text: "Cancelar",
        icon: "close",
        role: "cancel",
        handler: () => {
          // console.log("Cancel clicked");
        }
      });

      const actionSheet = await this._actionCtrl.create({
        header: grupo.gpr_c_grupo,
        buttons: buttons
      });
      await actionSheet.present();
    } // if
  }

  async onNovoClick() {
    const alert = await this._alertCtrl.create({
      header: "Novo grupo de produtos",
      subHeader: "Nome",
      inputs: [
        {
          name: "grupo",
          value: "",
          type: "text",
          placeholder: "Ex: Bebidas, pizzas, ..."
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            // console.log("Cancel");
          }
        },
        {
          text: "Ok",
          handler: (grupo: { grupo: string }) => {
            // console.log(grupo);
            if (grupo) {
              this._novo(grupo.grupo);
            } // if
          }
        }
      ]
    });

    await alert.present();
  }

  private async _novo(grupo: string) {
    // console.log(grupo);
    if (grupo) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      loading.present().then(() => {
        this._gruposServ.novo(this.idLoja, grupo, this.jwt).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.onRefresh();
            } else {
              if (api.errors.grupo) {
                this._ionicServ.alert(
                  "Erro",
                  "Nome de grupo inválido ou já utilizado."
                );
              } // if
            } //
          },
          err => {
            console.error(err);
            // loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion
}
