//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { BairroSelModalModule } from "../bairro-sel/bairro-sel.module";
import { ComponentsModule } from "../../components/components.module";
import { GrupoProdutosEditModalModule } from "../grupo-produtos-edit/grupo-produtos-edit.module";
import { HorarioFormModalModule } from "../horario-form/horario-form.module";
import { ProdutoFormModalModule } from "../produto-form/produto-form.module";
import { SetorFormModalModule } from "../setor-form/setor-form.module";
import { ZoomImgModalModule } from "../zoom-img/zoom-img.module";
//#endregion

//#region app modals
import { AreaLojistaModalPage } from "./area-lojista.page";
//#endregion

//#region app components
import {
  AreaLojistaFormasPgtoComponent,
  AreaLojistaGruposProdutosComponent,
  AreaLojistaHorariosComponent,
  AreaLojistaLojaComponent,
  AreaLojistaSelLojaComponent,
  AreaLojistaProdutosComponent,
  AreaLojistaSetoresEntregaComponent
} from ".";
//#endregion

@NgModule({
  imports: [
    BairroSelModalModule,
    CommonModule,
    ComponentsModule,
    FormsModule,
    GrupoProdutosEditModalModule,
    HorarioFormModalModule,
    ProdutoFormModalModule,
    SetorFormModalModule,
    IonicModule,
    ReactiveFormsModule,
    ZoomImgModalModule
  ],
  declarations: [
    // components
    AreaLojistaFormasPgtoComponent,
    AreaLojistaGruposProdutosComponent,
    AreaLojistaHorariosComponent,
    AreaLojistaLojaComponent,
    AreaLojistaSelLojaComponent,
    AreaLojistaProdutosComponent,
    AreaLojistaSetoresEntregaComponent,
    // modals
    AreaLojistaModalPage
  ],
  entryComponents: [AreaLojistaModalPage],
  exports: [
    AreaLojistaFormasPgtoComponent,
    AreaLojistaGruposProdutosComponent,
    AreaLojistaHorariosComponent,
    AreaLojistaLojaComponent,
    AreaLojistaSelLojaComponent,
    AreaLojistaProdutosComponent,
    AreaLojistaSetoresEntregaComponent
  ]
})
export class AreaLojistaModalModule {}
