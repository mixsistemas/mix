//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController } from "@ionic/angular";
//#endregion

//#region app models
type TTab = "D" | "C" | "O";
import { environment } from "../../../environments/environment";
import { IFormaPgto } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { FormasPgtoService, StorageService } from "../../modules/_services";
// import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-area-lojista-formas-pgto",
  templateUrl: "formas-pgto.component.html",
  styleUrls: ["formas-pgto.component.scss"]
})
export class AreaLojistaFormasPgtoComponent implements OnInit {
  //#region comm
  @Input() idLoja: number = 0;
  //#endregion

  //#region publics
  jwt: string;
  formas: IFormaPgto[];
  url_imgs = environment.url.img;
  //#endregion

  //#region tab
  private _tab: TTab;
  set tab(val: TTab) {
    this._tab = val;
    this._refresh();
  }
  get tab(): TTab {
    return this._tab;
  }
  //#endregion

  //#region constructor
  constructor(
    private _formasServ: FormasPgtoService,
    private _loadingCtrl: LoadingController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      // this._refresh();
      this.tab = "D";
    });
  }
  //#endregion

  //#region functions
  private async _refresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.formas = null;
        this._formasServ.formasConta(this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              // console.log(this.tab);
              // console.log(api.data[this.tab]);
              this.formas = this._formasServ.fixes(api.data[this.tab]);
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion

  //#region methods
  async onFormaClick(forma: IFormaPgto) {
    // console.log(forma);
    if (forma) {
      // console.log(forma.fpg_pk, forma.sel);
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt && this.idLoja > 0) {
        loading.present().then(() => {
          this._formasServ
            .toggle(forma.sel, forma.fpg_pk, this.idLoja, this.jwt)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                /* if (api.ok) {
              // console.log(this.tab);
              // console.log(api.data.all[this.tab]);
              this.formas = this._formasServ.fixes(api.data.all[this.tab]);
            } // if */
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      } // if
    } // if
  }
  //#endregion
}
