//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { ILoja } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { LojasService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-lojas-fav-armazenadas",
  templateUrl: "armazenadas.component.html",
  styleUrls: ["armazenadas.component.scss"]
})
export class LojasFavArmazenadasComponent implements OnInit {
  //#region publics
  lojas: ILoja[];
  //#endregion

  //#region contructor
  constructor(
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("LojasFavArmazenadasComponent.ngOnInit");
    this._refresh();
  }
  //#endregion

  //#region methods
  private _refresh() {
    this._storageServ.lojFavGet((favs: string) => {
      // console.log(favs);
      this._buscaLojas(favs);
    });
  }

  private async _buscaLojas(favs: string) {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this.lojas = null;
      this._lojasServ.favsArmazenadas(favs).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          if (api.ok) {
            loading.dismiss();
            this.lojas = this._lojasServ.fixes(api.data) || [];
          } // if
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  onSelClick(loja: ILoja) {
    // console.log(loja);
    if (loja) {
      this._storageServ.lojFavDel(loja.loj_pk, () => {
        this._ionicServ.toast(
          `Loja ${loja.loj_c_loja.toUpperCase()} não é mais favorita.`,
          "danger"
        );
        this._refresh();
      });
    } // if
  }
  //#endregion
}
