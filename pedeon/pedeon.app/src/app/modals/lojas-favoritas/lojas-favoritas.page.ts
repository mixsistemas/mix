//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region models
enum LojasFavTab {
  armazenadas,
  conta
}
//#endregion
@Component({
  selector: "po-lojas-favoritas",
  templateUrl: "./lojas-favoritas.page.html",
  styleUrls: ["./lojas-favoritas.page.scss"]
})
export class LojasFavoritasModalPage implements OnInit {
  //#region publics
  Tab = LojasFavTab;
  tab: LojasFavTab;
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.tab = LojasFavTab.armazenadas;
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }
  //#endregion
}
