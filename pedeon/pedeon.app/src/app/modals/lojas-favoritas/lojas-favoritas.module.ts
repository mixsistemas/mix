//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app modals
import { LojasFavoritasModalPage } from ".";
//#endregion

//#region app components
import { LojasFavArmazenadasComponent, LojasFavContaComponent } from ".";
//#endregion

@NgModule({
  imports: [CommonModule, ComponentsModule, FormsModule, IonicModule],
  declarations: [
    // components
    LojasFavArmazenadasComponent,
    LojasFavContaComponent,
    // modals
    LojasFavoritasModalPage
  ],
  entryComponents: [LojasFavoritasModalPage]
})
export class LojasFavoritasModalModule {}
