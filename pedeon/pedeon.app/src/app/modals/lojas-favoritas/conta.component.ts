//#region ng
import { Component, EventEmitter, Output } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import { IApiResponse, IViaCep } from "../../modules/_mix/_models/_interfaces";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { LojasService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { LoginCadastroModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-lojas-fav-conta",
  templateUrl: "conta.component.html",
  styleUrls: ["conta.component.scss"]
})
export class LojasFavContaComponent {
  //#region comm
  @Output() onSel: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  lojas: ILoja[];
  jwt: string;
  //#endregion

  //#region contructor
  constructor(
    private _ionicServ: IonicService,
    private _lojasServ: LojasService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.jwt = null;
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._buscaLojas();
    });
  }

  private async _buscaLojas() {
    // console.log(this.jwt);

    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.lojas = null;
        this._lojasServ.favsConta(this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok && api.data) {
              this.lojas = this._lojasServ.fixes(api.data.lojas) || [];
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion

  //#region methods
  async onLoginCadastroClick() {
    console.log("onLoginCadastroClick");
    // this.modalCtrl.dismiss();
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: LoginCadastroTab.login }
    });

    modal.onDidDismiss().then(() => {
      this._refresh();
    });

    return await modal.present();
  }

  async onSelClick(loja: ILoja) {
    // console.log(loja);
    if (loja) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this.lojas = null;
          this._lojasServ.favDel(loja.loj_pk, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast(
                  `Loja ${loja.loj_c_loja.toUpperCase()} não é mais favorita.`,
                  "danger"
                );
                this._buscaLojas();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  onLogin(local: IViaCep) {
    // console.log(local);
    if (local) {
      this._storageServ.localSet(local);
    } // if
    this._refresh();
  }
  //#endregion
}
