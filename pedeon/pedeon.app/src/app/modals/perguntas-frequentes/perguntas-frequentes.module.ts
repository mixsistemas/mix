//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modals
import { PerguntasFrequentesModalPage } from "..";
//#endregion

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [
    // components
    // modals
    PerguntasFrequentesModalPage
  ],
  entryComponents: [PerguntasFrequentesModalPage]
})
export class PerguntasFrequentesModalModule {}
