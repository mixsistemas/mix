//#region ng
import { Component, Input, OnChanges, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

@Component({
  selector: "po-perguntas-frequentes",
  templateUrl: "./perguntas-frequentes.page.html",
  styleUrls: ["./perguntas-frequentes.page.scss"]
})
export class PerguntasFrequentesModalPage implements OnChanges, OnInit {
  //#region comm
  @Input() value: any;
  //#endregion

  //#region publics
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    console.log(this.value);
  }
  ngOnInit() {}
  //#endregion

  //#region app mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }
  //#endregion
}
