//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { ISetor } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import { IonicService } from "../../modules/_mix/_core/_services";
import { SetoresService, StorageService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-setor-form",
  templateUrl: "./setor-form.page.html",
  styleUrls: ["./setor-form.page.scss"]
})
export class SetorFormModalPage implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() setor: ISetor;
  //#endregion

  //#region publics
  caption: string;
  fv: FormValidation;
  jwt: string;
  novo: boolean;
  setorForm: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _setoresServ: SetoresService,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.setorForm = formBuilder.group({
      setor: [
        "",
        Validators.compose([Validators.maxLength(20), Validators.required])
      ],
      taxaEntrega: ["0.00"],
      ativo: [true]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
    });

    setTimeout(() => {
      this.novo = this.setor == null;

      this.caption = this.novo
        ? "Novo setor"
        : `Modificando setor ${this.setor.set_c_setor}`;

      if (!this.novo) {
        this.setorForm.patchValue({
          setor: this.setor.set_c_setor,
          taxaEntrega: this.setor.set_f_taxa_entrega,
          ativo: this.setor.set_b_ativo
        });
      } // if
    }, 0);
  }
  //#endregion

  //#region functions
  private async _novoSetor() {
    // console.log("_gravaproduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // console.log(this.jwt);
    /* this.setor = {
      set_fk_loja: this.idLoja,
      set_b_ativo: this.setorForm.value.ativo,
      set_c_setor: this.setorForm.value.setor,
      set_f_taxa_entrega: parseFloat(this.setorForm.value.taxaEntrega)
    };
    console.log(this.setor); */
    // console.log(this.setorForm.value);

    loading.present().then(() => {
      this._setoresServ
        .novo(this.idLoja, this.jwt, this.setorForm.value)
        .subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              this.modalCtrl.dismiss();
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }

  private async _gravaSetor() {
    // console.log("_gravaproduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // console.log(this.jwt);
    /* this.setor = {
      set_fk_loja: this.idLoja,
      set_b_ativo: this.setorForm.value.ativo,
      set_c_setor: this.setorForm.value.setor,
      set_f_taxa_entrega: parseFloat(this.setorForm.value.taxaEntrega)
    };
    console.log(this.setor); */
    // console.log(this.setorForm.value);

    loading.present().then(() => {
      this._setoresServ
        .update(this.setor.set_pk, this.setorForm.value, this.jwt)
        .subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
             if (api.ok) {
              this.modalCtrl.dismiss();
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    if (this.novo) {
      this._novoSetor();
    } else {
      this._gravaSetor();
    } // else
  }
  //#endregion
}
