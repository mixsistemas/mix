//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app modals
import { SetorFormModalPage } from "..";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ],
  declarations: [
    // components
    // modals
    SetorFormModalPage
  ],
  entryComponents: [SetorFormModalPage]
})
export class SetorFormModalModule {}
