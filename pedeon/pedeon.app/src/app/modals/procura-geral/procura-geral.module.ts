//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
import { ProcuraGeralModalPage } from "./procura-geral.page";
import { ZoomImgModalModule } from "../../modals/zoom-img/zoom-img.module";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    ZoomImgModalModule
  ],
  declarations: [
    // components
    // modals
    ProcuraGeralModalPage
  ],
  entryComponents: [ProcuraGeralModalPage]
})
export class ProcuraGeralModalModule {}
