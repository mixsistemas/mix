//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region models
type TTabProcuraGeral = "loj" | "pro";
import { environment } from "../../../environments/environment";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { ILoja, IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  LojasService,
  ProcuraService,
  ProdutosService
} from "../../modules/_services";
//#endregion

//#region app modals
import { ZoomImgModalPage } from "../../modals/zoom-img/zoom-img.page";

@Component({
  selector: "po-procura-geral",
  templateUrl: "./procura-geral.page.html",
  styleUrls: ["./procura-geral.page.scss"]
})
export class ProcuraGeralModalPage implements OnInit {
  //#region comm
  @Input() filter: string;
  //#endregion

  //#region publics
  tab: TTabProcuraGeral;
  lojas: ILoja[];
  produtos: IProduto[];
  url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    public modalCtrl: ModalController,
    private _procuraServ: ProcuraService,
    private _produtosServ: ProdutosService,
    private _router: Router
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private async _refresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this.lojas = null;
      this.produtos = null;
      this._procuraServ.lojPro(this.filter).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          loading.dismiss();
          if (api.ok) {
            this.lojas = this._lojasServ.fixes(api.data.loj) || [];
            this.produtos = this._produtosServ.fixes(api.data.pro) || [];
            if (this.produtos.length > 0) {
              this.tab = "pro";
            } else {
              this.tab = this.lojas.length > 0 ? "loj" : "pro";
            } // else
          } // if
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }

  onLojaSel(idLoja: number) {
    // console.log(idLoja);
    if (idLoja > 0) {
      this.modalCtrl.dismiss();
      this._router.navigate(["/loja", idLoja]);
    } // if
  }
  //#endregion
}
