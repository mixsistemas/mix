//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ZoomImgModalModule } from "../../modals/zoom-img/zoom-img.module";
//#endregion

//#region app modals
import { ProdutoOkModalPage } from "..";
//#endregion

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ZoomImgModalModule],
  declarations: [
    // components
    // modals
    ProdutoOkModalPage
  ],
  entryComponents: [ProdutoOkModalPage]
})
export class ProdutoOkModalModule {}
