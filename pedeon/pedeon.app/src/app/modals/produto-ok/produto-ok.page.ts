//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILoja, IProduto } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { ZoomImgModalPage } from "../../modals/zoom-img/zoom-img.page";
//#endregion

@Component({
  selector: "po-produto-ok",
  templateUrl: "./produto-ok.page.html",
  styleUrls: ["./produto-ok.page.scss"]
})
export class ProdutoOkModalPage implements OnInit {
  //#region comm
  @Input() idProduto: number;
  @Input() loja: ILoja;
  //#endregion

  //#region publics
  jwt: string;
  produto: IProduto;
  url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.jwt = token;
      this._refresh();
    });
  }
  //#endregion

  //#region functions
  private async _refresh() {
    // console.log("_refresh()");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // console.log(this.jwt);
    loading.present().then(() => {
      this._storageServ.proFavGet((favs: string) => {
        // console.log(favs);
        this.produto = null;
        this._produtosServ.produto(this.idProduto, favs, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.produto = this._produtosServ.fix(api.data);
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    });
  }

  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }
  //#endregion

  //#region app methods
  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }

  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onToggleFavClick() {
    // console.log(this.produto);
    if (this.produto) {
      const { pro_pk: ID_PRODUTO, pro_b_favorito: FAV } = this.produto;
      // console.log(ID_PRODUTO, FAV, this.produto);
      // console.log(this.jwt);
      if (ID_PRODUTO > 0) {
        if (this.jwt) {
          if (FAV) {
            this._produtosServ.favDel(ID_PRODUTO, this.jwt).subscribe(
              (api: IApiResponse) => {
                console.log(api);
                if (api.ok) {
                  this._storageServ.proFavDel(ID_PRODUTO); // Apaga do armazenagem local junto da conta.
                  this._ionicServ.toast("Produto não é mais favorito.", "danger");
                  this._storageServ.onProFavChangedEvent.next(true);
                  this._refresh();
                } // if
              },
              err => {
                console.error(err);
              }
            );
          } else {
            this._produtosServ.favAdd(ID_PRODUTO, this.jwt).subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                if (api.ok) {
                  this._ionicServ.toast("Produto agora é favorito.");
                  this._storageServ.onProFavChangedEvent.next(true);
                  this._refresh();
                } // if
              },
              err => {
                console.error(err);
              }
            );
          } // else
        } else {
          if (FAV) {
            this._storageServ.proFavDel(ID_PRODUTO, () => {
              // console.log(ID_PRODUTO);
              this._ionicServ.toast("Produto não é mais favorito.", "danger");
              this._refresh();
            });
          } else {
            this._storageServ.proFavAdd(ID_PRODUTO, () => {
              // console.log(ID_PRODUTO);
              this._ionicServ.toast("Produto agora é favorito.");
              this._refresh();
            });
          } // else
        } // else
      } // if
    } // if
  }
  //#endregion
}
