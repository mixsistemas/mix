//#region ng
import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { IonContent, ModalController } from "@ionic/angular";
//#endregion

//#region models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region services
import { StorageService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-login-cadastro",
  templateUrl: "./login-cadastro.page.html",
  styleUrls: ["./login-cadastro.page.scss"]
})
export class LoginCadastroModalPage implements OnInit {
  //#region comm
  @Input() tab: LoginCadastroTab;
  @ViewChild("content") content: IonContent;
  //#endregion

  //#region publics
  Tab = LoginCadastroTab;
  //#endregion

  //#region constructor
  constructor(
    public modalCtrl: ModalController,
    private _storageServ: StorageService,
    private _router: Router
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {}
  //#endregion

  //#region app mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onCadastroPageChanged() {
    // console.log("onCadastroPageChanged");
    this.content.scrollToTop();
  }

  onLogin(local: IViaCep) {
    // console.log(local);
    if (local) {
      this._storageServ.localSet(local);
    } // if
    this.modalCtrl.dismiss();
    if (window.location.href.includes("sel-local")) {
      this._router.navigate(["/sel-loja"]);
    } // if
  }
  //#endregion
}
