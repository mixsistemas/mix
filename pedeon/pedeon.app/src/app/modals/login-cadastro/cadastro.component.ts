//#region ng
import { Component, EventEmitter, ViewChild, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region 3rd
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app services
import { CepService, IonicService } from "../../modules/_mix/_core/_services";
import {
  ContasService,
  StorageService,
  LojasService
} from "../../modules/_services";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
import { CadastroTab } from "../../modules/_shared/_models/_enums";
import { IApiResponse, IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

@Component({
  selector: "po-cadastro",
  templateUrl: "cadastro.component.html",
  styleUrls: ["cadastro.component.scss"]
})
export class CadastroComponent {
  //#region comm
  // @ViewChild("sliderCadastro") sliderCadastro: any;
  // @ViewChild(Slides) sliderCadastro: Slides;
  @ViewChild("sliderCadastro") sliderCadastro;
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter(false);
  @Output() onFalhaLogin: EventEmitter<boolean> = new EventEmitter(false);
  //#endregion

  //#region publics
  activeSlide: CadastroTab = CadastroTab.info;
  fv: FormValidation;
  infoForm: FormGroup;
  enderecoForm: FormGroup;
  Tab = CadastroTab;
  tab: CadastroTab;
  termosForm: FormGroup;
  //#endregion

  //#region methods
  // onPageChangedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _cepServ: CepService,
    private _contasServ: ContasService,
    private _ionicServ: IonicService,
    private _lojasServ: LojasService,
    public formBuilder: FormBuilder,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _router: Router,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();
    // this.submitted = false;

    this.infoForm = formBuilder.group({
      nome: [
        "james",
        Validators.compose([
          Validators.maxLength(20),
          Validators.pattern("[a-zA-Z ]*"),
          Validators.required
        ])
      ],
      sobrenome: [
        "bond",
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern("[a-zA-Z ]*")
        ])
      ],
      genero: ["M", Validators.compose([Validators.required])],
      nascimento: ["", Validators.compose([Validators.required])],
      fone: ["(34)99924-9134", Validators.compose([Validators.required])],
      cpf: ["863.626.906-72", Validators.compose([Validators.maxLength(14)])],
      email: [
        "teste@teste.com",
        Validators.compose([
          Validators.maxLength(60),
          Validators.required,
          Validators.email
        ])
      ],
      senha: [
        "teste",
        Validators.compose([Validators.minLength(5), Validators.required])
      ],
      confirmacao: [
        "teste",
        Validators.compose([Validators.minLength(5), Validators.required])
      ]
    });

    this.enderecoForm = formBuilder.group({
      cep: ["38060-100", Validators.compose([Validators.required])],
      uf: ["", Validators.required],
      localidade: ["", Validators.required],
      logradouro: ["", Validators.required],
      bairro: ["", Validators.required],
      nro: ["70", Validators.required],
      complemento: [""],
      referencia: [""]
    });

    this.termosForm = formBuilder.group({
      termos: [true, Validators.compose([Validators.required])]
    });
  }
  //#endregion

  //#region functions
  private async _gravaCadastro() {
    // console.log(_gravaCadastro);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._contasServ
        .add(this.infoForm.value, this.enderecoForm.value)
        .subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              if (api.errors["login"] || !api.data.jwt) {
                // alert("erro ao logar automaticamente.");
                this.onFalhaLogin.emit(true);
              } else {
                this.modalCtrl.dismiss();
                // console.log(this.enderecoForm.value);
                this._storageServ.localSet(this.enderecoForm.value, () => {
                  this._storageServ.onLocalChangedEvent.next(
                    this.enderecoForm.value
                  );
                  this._storageServ.authJwtSet(api.data.jwt, () => {
                    this._storageServ.onAuthChanged.next(api.data.payload);
                    this._router.navigate(["/sel-loja"]);
                  });
                });
              } // else
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
              this.sliderCadastro.slideTo(CadastroTab.info);
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }
  //#endregion

  //#region methods
  async onCepSel() {
    // alert(this.enderecoForm.value.cep);
    const CEP = this.enderecoForm.value.cep;
    // this._router.navigate(["/sel-loja"]);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._cepServ
        .cep(CEP)
        .pipe(
          mergeMap((cep: IViaCep) => {
            return this._lojasServ.numLojas(cep.localidade, cep.uf).pipe(
              map((numLojas: any) => {
                return {
                  cep: cep,
                  numLojas: numLojas
                };
              })
            );
          })
        )
        .subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.numLojas.ok) {
              if (api.numLojas.data > 0) {
                this.enderecoForm.controls.uf.setValue(api.cep.uf);
                this.enderecoForm.controls.localidade.setValue(
                  api.cep.localidade
                );
                this.enderecoForm.controls.logradouro.setValue(
                  api.cep.logradouro
                );
                this.enderecoForm.controls.bairro.setValue(api.cep.bairro);
              } else {
                this._ionicServ.alert(
                  "Localidade não atendida.",
                  `${api.cep.localidade} ${api.cep.uf}`
                );
                this.enderecoForm.reset();
              } // else
            } else {
              this._ionicServ.alert(
                "CEP não encontrado",
                this.enderecoForm.value.cep
              );
              this.enderecoForm.reset();
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );

      /*
      this._cepServ.cep(CEP).subscribe(
        (api: IViaCep) => {
          // console.log(api);
          loading.dismiss();
          if (api.erro) {
            this.presentAlert(
              "CEP não encontrado",
              this.enderecoForm.value.cep
            );
            this.enderecoForm.reset();
          } else {
            this.enderecoForm.controls.uf.setValue(api.uf);
            this.enderecoForm.controls.localidade.setValue(api.localidade);
            this.enderecoForm.controls.logradouro.setValue(api.logradouro);
            this.enderecoForm.controls.bairro.setValue(api.bairro);
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
        );
        */
    });
  }
  onNextSlide() {
    this.sliderCadastro.slideNext();
  }

  onPrevSlide() {
    this.sliderCadastro.slidePrev();
  }

  OnSlideChanged() {
    this.sliderCadastro.getActiveIndex().then(resp => {
      // console.log(resp);
      this.activeSlide = resp; // resp.toString();
      // console.log(this.activeSlide);
      this.onChanged.emit();
    });
  }

  onSubmit() {
    // this.submitted = true;
    /* console.log(
      this.infoForm.valid,
      this.enderecoForm.valid,
      this.termosForm.valid
    ); */

    /* if (!this.infoForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.info);
    } else if (!this.enderecoForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.endereco);
    } else if (!this.termosForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.termos);
    } else {
      // console.log(this.infoForm.value);
      // console.log(this.enderecoForm.value);
      // console.log(this.termosForm.value);
      this._gravaCadastro();
    } // else */

    // console.log(this.infoForm.value);
    // console.log(this.enderecoForm.value);
    // console.log(this.termosForm.value);
    this._gravaCadastro();
    if (!this.infoForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.info);
    } else if (!this.enderecoForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.endereco);
    } else if (!this.termosForm.valid) {
      this.sliderCadastro.slideTo(CadastroTab.termos);
    }
  }
  //#endregion
}
