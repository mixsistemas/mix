//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region 3rd
import { BrMaskerModule } from "br-mask";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app components
import { CadastroComponent } from ".";
//#endregion

//#region app modals
import { LoginCadastroModalPage } from "..";
//#endregion

@NgModule({
  imports: [
    BrMaskerModule,
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ],
  declarations: [
    // components
    CadastroComponent,
    // modals
    LoginCadastroModalPage
  ],
  entryComponents: [LoginCadastroModalPage]
})
export class LoginCadastroModalModule {}
