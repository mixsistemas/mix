//#region ng
import { Component, Input, OnInit, ViewChild, ElementRef } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region app models
// import { environment } from "../../../environments/environment";
//#endregion

@Component({
  selector: "po-zoom-img",
  templateUrl: "./zoom-img.page.html",
  styleUrls: ["./zoom-img.page.scss"]
})
export class ZoomImgModalPage implements OnInit {
  //#region comm
  @Input() img: string;
  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  //#endregion

  //#region publics
  // url_imgs = environment.url.img;
  sliderOpts = {
    zoom: {
      maxRatio: 2
    }
  };
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {}
  //#endregion

  //#region app mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onZoomClick(zoomIn: boolean = false) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    } // else
  }
  //#endregion
}
