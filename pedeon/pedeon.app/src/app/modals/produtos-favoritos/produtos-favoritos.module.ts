//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app modals
import { ProdutosFavoritosModalPage } from ".";
//#endregion

//#region app components
import { ProdutosFavArmazenadosComponent, ProdutosFavContaComponent } from ".";
//#endregion

@NgModule({
  imports: [CommonModule, ComponentsModule, FormsModule, IonicModule],
  declarations: [
    // components
    ProdutosFavArmazenadosComponent,
    ProdutosFavContaComponent,
    // modals
    ProdutosFavoritosModalPage
  ],
  entryComponents: [ProdutosFavoritosModalPage]
})
export class ProdutosFavoritosModalModule {}
