//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { IProduto } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { ZoomImgModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-produtos-fav-armazenados",
  templateUrl: "armazenados.component.html",
  styleUrls: ["armazenados.component.scss"]
})
export class ProdutosFavArmazenadosComponent implements OnInit {
  //#region publics
  produtos: IProduto[];
  //#endregion

  //#region contructor
  constructor(
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("ProdutosFavArmazenadosComponent.ngOnInit");
    this._refresh();
  }
  //#endregion

  //#region methods
  private _refresh() {
    this._storageServ.proFavGet((favs: string) => {
      // console.log(favs);
      this._buscaLojas(favs);
    });
  }

  private async _buscaLojas(favs: string) {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this.produtos = null;
      this._produtosServ.favsArmazenados(favs).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          if (api.ok) {
            loading.dismiss();
            this.produtos = this._produtosServ.fixes(api.data) || [];
          } // if
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  onSelClick(produto: IProduto) {
    // console.log(produto);
    if (produto) {
      this._storageServ.proFavDel(produto.pro_pk, () => {
        this._ionicServ.toast(
          `Produto ${produto.pro_c_produto.toUpperCase()} não é mais favorito.`,
          "danger"
        );
        this._refresh();
      });
    } // if
  }

  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }
  //#endregion
}
