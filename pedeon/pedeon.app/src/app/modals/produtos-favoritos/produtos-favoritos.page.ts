//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region models
enum ProdutosFavTab {
  armazenados,
  conta
}
//#endregion
@Component({
  selector: "po-produtos-favoritos",
  templateUrl: "./produtos-favoritos.page.html",
  styleUrls: ["./produtos-favoritos.page.scss"]
})
export class ProdutosFavoritosModalPage implements OnInit {
  //#region publics
  Tab = ProdutosFavTab;
  tab: ProdutosFavTab;
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.tab = ProdutosFavTab.armazenados;
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }
  //#endregion
}
