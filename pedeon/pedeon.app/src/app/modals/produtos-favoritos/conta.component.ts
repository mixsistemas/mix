//#region ng
import { Component, EventEmitter, Output } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import { IApiResponse, IViaCep } from "../../modules/_mix/_models/_interfaces";
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { LoginCadastroModalPage, ZoomImgModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-produtos-fav-conta",
  templateUrl: "conta.component.html",
  styleUrls: ["conta.component.scss"]
})
export class ProdutosFavContaComponent {
  //#region comm
  @Output() onSel: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  produtos: IProduto[];
  jwt: string;
  //#endregion

  //#region contructor
  constructor(
    private _ionicServ: IonicService,
    private _produtosServ: ProdutosService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.jwt = null;
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._buscaprodutos();
    });
  }

  private async _buscaprodutos() {
    // console.log(this.jwt);

    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.produtos = null;
        this._produtosServ.favsConta(this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok && api.data) {
              this.produtos = this._produtosServ.fixes(api.data.produtos) || [];
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion

  //#region methods
  async onLoginCadastroClick() {
    // console.log("onLoginCadastroClick");
    // this.modalCtrl.dismiss();
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: LoginCadastroTab.login }
    });

    modal.onDidDismiss().then(() => {
      this._refresh();
    });

    return await modal.present();
  }

  async onSelClick(produto: IProduto) {
    // console.log(produto);
    if (produto) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this.produtos = null;
          this._produtosServ.favDel(produto.pro_pk, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast(
                  `Produto ${produto.pro_c_produto.toUpperCase()} não é mais favorito.`,
                  "danger"
                );
                this._buscaprodutos();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  onLogin(local: IViaCep) {
    // console.log(local);
    if (local) {
      this._storageServ.localSet(local);
    } // if
    this._refresh();
  }

  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }
  //#endregion
}
