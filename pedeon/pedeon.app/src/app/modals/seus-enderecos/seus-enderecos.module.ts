//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region 3rd
import { Ng2BRPipesModule } from "ng2-brpipes";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app components
import { EnderecosContaComponent } from ".";
//#endregion

//#region app modals
import { LocalFormModalModule } from "../local-form/local-form.module";
import { SeusEnderecosModalPage } from ".";
//#endregion

@NgModule({
  imports: [
    LocalFormModalModule,
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    Ng2BRPipesModule,
    ReactiveFormsModule
  ],
  declarations: [
    // components
    EnderecosContaComponent,
    // modals
    SeusEnderecosModalPage
  ],
  entryComponents: [SeusEnderecosModalPage]
})
export class SeusEnderecosModalModule {}
