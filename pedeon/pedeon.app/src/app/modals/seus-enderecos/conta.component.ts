//#region ng
import { Component, EventEmitter, Output } from "@angular/core";
//#endregion

//#region ionic
import {
  ActionSheetController,
  AlertController,
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region 3rd
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import {
  IApiResponse,
  IEndereco,
  IModalResponse,
  IViaCep
} from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { CepService, IonicService } from "../../modules/_mix/_core/_services";
import {
  EnderecosService,
  LojasService,
  StorageService
} from "../../modules/_services";
//#endregion

//#region app modals
import { LocalFormModalPage, LoginCadastroModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-enderecos-conta",
  templateUrl: "conta.component.html",
  styleUrls: ["conta.component.scss"]
})
export class EnderecosContaComponent {
  //#region comm
  @Output() onSel: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  enderecos: IEndereco[];
  filter: string;
  jwt: string;
  //#endregion

  //#region contructor
  constructor(
    private _actionCtrl: ActionSheetController,
    private _alertCtrl: AlertController,
    private _cepServ: CepService,
    private _enderecosServ: EnderecosService,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private _refresh() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.onRefresh();
    });
  }

  private async _setFavorito(id: number) {
    if (id > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this._enderecosServ.favorito(id, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast(
                  "Endereço favorito modificado com sucesso."
                );
                this._refresh();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  private async _enderecoDel(id: number) {
    if (id > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      if (this.jwt) {
        loading.present().then(() => {
          this._enderecosServ.del(id, this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this._ionicServ.toast(
                  "Endereço eliminado com sucesso.",
                  "danger"
                );
                this._refresh();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } // if
  }

  private async _verificaNovo(cep: { cep: string }) {
    // console.log(cep.cep);

    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._cepServ
        .cep(cep.cep)
        .pipe(
          mergeMap((cep: IViaCep) => {
            return this._lojasServ.numLojas(cep.localidade, cep.uf).pipe(
              map((numLojas: any) => {
                return {
                  cep: cep,
                  numLojas: numLojas
                };
              })
            );
          })
        )
        .subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.numLojas.ok) {
              if (api.numLojas.data > 0) {
                this.onLocalFormClick(api.cep);
              } else {
                this._ionicServ.alert(
                  "Localidade não atendida.",
                  `${api.cep.localidade} ${api.cep.uf}`
                );
              } // else
            } else {
              this._ionicServ.alert("CEP não encontrado", cep.cep);
              // this.cepForm.reset();
            } // elsee
          },
          err => {
            this._ionicServ.alert("CEP inválido ou não encontrado", cep.cep);
            // this.cepForm.reset();
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }

  private async _novo(local: IViaCep) {
    // console.log(local);
    if (local) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      loading.present().then(() => {
        this._enderecosServ.novo(local, this.jwt).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this._refresh();
            } // if
          },
          err => {
            console.error(err);
            // loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion

  //#region methods
  onLimpaProcura() {
    this.filter = "";
    this._refresh();
  }

  async onRefresh() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    // console.log(this.jwt);
    if (this.jwt) {
      loading.present().then(() => {
        this.enderecos = null;
        this._enderecosServ.enderecos(this.jwt, this.filter).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.enderecos = this._enderecosServ.fixes(api.data) || [];
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  onEnderecoClick(endereco: IEndereco) {
    // console.log(endereco);
    if (endereco) {
      const LOCAL = this._enderecosServ.end2local(endereco);
      this._storageServ.localSet(LOCAL, () => this.onSel.emit(LOCAL));
    } // if
  }

  onFavoritoClick(endereco: IEndereco) {
    // console.log(endereco);
    if (endereco) {
      this._setFavorito(endereco.end_pk);
    } // if
  }

  onEnderecoDelClick(endereco: IEndereco) {
    // console.log(endereco);
    if (endereco) {
      this._enderecoDel(endereco.end_pk);
    } // if
  }

  async onLoginCadastroClick() {
    // console.log("onLoginCadastroClick");
    // this.modalCtrl.dismiss();
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: LoginCadastroTab.login }
    });

    modal.onDidDismiss().then(() => {
      this.onRefresh();
    });

    return await modal.present();
  }

  async onMenuClick(endereco: IEndereco) {
    if (endereco) {
      // Inicia array de buttons.
      let buttons: any[] = [
        {
          text: "Selecionar",
          // role: "destructive",
          icon: "/assets/icon/check.svg",
          handler: () => {
            this.onEnderecoClick(endereco);
          }
        }
      ];

      // Opção de tornar favorito apenas para não favoritos.
      if (!endereco.end_b_favorito) {
        buttons.push(
          {
            text: "Tornar favorito",
            icon: "/assets/icon/heart.svg",
            handler: () => {
              if (endereco) {
                this._setFavorito(endereco.end_pk);
              } // if
            }
          },
          {
            text: "Excluir",
            icon: "/assets/icon/trash.svg",
            handler: () => {
              if (endereco) {
                this._enderecoDel(endereco.end_pk);
              } // if
            }
          }
        );
      } // else

      // Completa array de buttons.
      buttons.push({
        text: "Cancelar",
        icon: "close",
        role: "cancel",
        handler: () => {
          // console.log("Cancel clicked");
        }
      });

      const actionSheet = await this._actionCtrl.create({
        header: `${endereco.end_c_logradouro}, ${endereco.end_i_nro} ${
          endereco.end_c_complemento
        }`,
        buttons: buttons
      });
      await actionSheet.present();
    } // if
  }

  onLogin(local: IViaCep) {
    // console.log(local);
    if (local) {
      this._storageServ.localSet(local);
    } // if
    this._refresh();
  }

  async onNovoClick() {
    const alert = await this._alertCtrl.create({
      header: "Novo endereço",
      subHeader: "Indique seu CEP",
      inputs: [
        {
          name: "cep",
          value: "",
          type: "tel",
          placeholder: "Ex: 99999 999"
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            // console.log("Cancel");
          }
        },
        {
          text: "Ok",
          handler: (cep: { cep: string }) => {
            // console.log(cep);
            this._verificaNovo(cep);
          }
        }
      ]
    });

    await alert.present();
  }

  async onLocalFormClick(cepInfo: IViaCep) {
    // console.log(cepInfo);

    const modal = await this.modalCtrl.create({
      component: LocalFormModalPage,
      componentProps: { cepInfo: cepInfo }
    });

    modal.onDidDismiss().then((data: IModalResponse) => {
      // console.log(data);
      if (data.data) {
        this._novo(data.data);
      } // if
    });

    return await modal.present();
  }
  //#endregion
}
