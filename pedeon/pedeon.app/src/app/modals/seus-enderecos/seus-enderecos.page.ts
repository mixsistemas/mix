//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region models
enum SeusEnderecosTab {
  armazenados,
  conta
}
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

@Component({
  selector: "po-seus-enderecos",
  templateUrl: "./seus-enderecos.page.html",
  styleUrls: ["./seus-enderecos.page.scss"]
})
export class SeusEnderecosModalPage implements OnInit {
  //#region publics
  Tab = SeusEnderecosTab;
  tab: SeusEnderecosTab;
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.tab = SeusEnderecosTab.armazenados;
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSelClick(local: IViaCep) {
    // console.log(local);
    this.modalCtrl.dismiss(local);
  }

  /* onNovoClick() {
    this.modalCtrl.dismiss();
    this._router.navigate(["/sel-local"]);
  } */
  //#endregion
}
