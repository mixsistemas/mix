//#region ng
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
//#endregion

//#region ionic
import {
  ActionSheetController,
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region app models
// import { environment } from "../../../environments/environment";
import {
  IDetalheProdutos,
  IItemDetalhesProdutos
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import {
  DetalhesProdutosService,
  StorageService
} from "../../modules/_services";
// import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import {
  DetalheProdutoFormModalPage,
  ItemDetalheProdutoFormModalPage
} from "../../modals";
//#endregion

@Component({
  selector: "po-detalhes-list",
  templateUrl: "detalhes-list.component.html",
  styleUrls: ["detalhes-list.component.scss"]
})
export class DetalhesListComponent implements OnInit {
  //#region comm
  @Input() idProduto: number;
  @Input() idLoja: number = 0;
  @Input() detalhes: IDetalheProdutos;
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  //#endregion

  //#region publics
  jwt: string;
  //#endregion

  //#region constructor
  constructor(
    private _actionCtrl: ActionSheetController,
    private _detalhesServ: DetalhesProdutosService,
    // private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
    });
  }
  //#endregion

  //#region functions
  private async _detalheForm(detalhe: IDetalheProdutos) {
    // console.log("_grupoEdit");
    const modal = await this.modalCtrl.create({
      component: DetalheProdutoFormModalPage,
      componentProps: {
        detalhe: detalhe,
        idLoja: this.idLoja,
        idProduto: this.idProduto
      }
    });

    modal.onDidDismiss().then(() => {
      // this._refresh();
      this.onChanged.emit(true);
    });

    return await modal.present();
  }
  //#endregion

  //#region methods
  async onMenuDetalheClick(detalhe: IDetalheProdutos) {
    // console.log(detalhe);
    if (detalhe) {
      let buttons: any[] = [
        {
          text: "Modificar detalhe",
          // role: "destructive",
          icon: "/assets/icon/edit.svg",
          handler: () => {
            this._detalheForm(detalhe);
          }
        },
        {
          text: "Remover detalhe",
          icon: "/assets/icon/minus.svg",
          handler: () => {
            // this._detalheDel(detalhe.gpr_pk);
          }
        },
        {
          text: "Novo item",
          icon: "/assets/icon/plus.svg",
          handler: () => {
            this._itemForm(detalhe, null);
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            // console.log("Cancel clicked");
          }
        }
      ];

      const actionSheet = await this._actionCtrl.create({
        header: detalhe.det_c_detalhe,
        buttons: buttons
      });
      await actionSheet.present();
    } // if
  }

  async onMenuItemClick(
    detalhe: IDetalheProdutos,
    item: IItemDetalhesProdutos
  ) {
    // console.log(item);
    if (item) {
      let buttons: any[] = [
        {
          text: "Modificar item",
          // role: "destructive",
          icon: "/assets/icon/edit.svg",
          handler: () => {
            this._itemForm(detalhe, item);
          }
        },
        {
          text: "Excluir item",
          icon: "/assets/icon/trash.svg",
          handler: () => {
            // this._itemDel(item.gpr_pk);
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            // console.log("Cancel clicked");
          }
        }
      ];

      const actionSheet = await this._actionCtrl.create({
        header: item.idp_c_item,
        buttons: buttons
      });
      await actionSheet.present();
    } // if
  }

  private async _itemForm(
    detalhe: IDetalheProdutos,
    item: IItemDetalhesProdutos
  ) {
    // console.log(detalhe);
    const modal = await this.modalCtrl.create({
      component: ItemDetalheProdutoFormModalPage,
      componentProps: { idLoja: this.idLoja, detalhe: detalhe, item: item }
    });

    modal.onDidDismiss().then(() => {
      // this._refresh();
      this.onChanged.emit(true);
    });

    return await modal.present();
  }

  async onSwapPosClick(detalhe: IDetalheProdutos, up: boolean = true) {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    if (up) {
      // console.log(detalhe.det_pk, detalhe.prior);
      if (detalhe.det_pk > 0 && detalhe.prior > 0) {
        loading.present().then(() => {
          this._detalhesServ
            .togglePos(this.idLoja, detalhe.det_pk, detalhe.prior, this.jwt)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                if (api.ok) {
                  // this._refresh();
                  this.onChanged.emit(true);
                } // if
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      } // if;
    } else {
      // console.log(detalhe.det_pk, detalhe.next);
      if (detalhe.det_pk > 0 && detalhe.next > 0) {
        loading.present().then(() => {
          this._detalhesServ
            .togglePos(this.idLoja, detalhe.det_pk, detalhe.next, this.jwt)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                if (api.ok) {
                  // this._refresh();
                  this.onChanged.emit(true);
                } // if
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      } // if;
    } // else
  }
  //#endregion
}
