//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { ProdutoFormTab } from "../../modules/_shared/_models/_enums";
import { IProduto } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService, StorageService } from "../../modules/_services";
// import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-produto-form",
  templateUrl: "./produto-form.page.html",
  styleUrls: ["./produto-form.page.scss"]
})
export class ProdutoFormModalPage implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() idProduto: number;
  //#endregion

  //#region publics
  caption: string;
  jwt: string;
  novo: boolean;
  produto: IProduto;
  Tab = ProdutoFormTab;
  tab: ProdutoFormTab;
  //#endregion

  //#region constructor
  constructor(
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _storageServ: StorageService
  ) {
    this.tab = ProdutoFormTab.produto;
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.jwt = token;
      this._refresh();
      this.tab = ProdutoFormTab.produto;
    });
  }
  //#endregion

  //#region functions
  private async _refresh() {
    // console.log(this.idProduto);
    this.novo = this.idProduto == 0;
    if (!this.novo) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      // console.log(this.jwt);
      this.produto = null;
      if (this.jwt) {
        // console.log(this.jwt);
        loading.present().then(() => {
          // console.log(favs);
          this._produtosServ.produto(this.idProduto, "[]", this.jwt).subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              loading.dismiss();
              if (api.ok) {
                this.produto = this._produtosServ.fix(api.data);
                this.caption = `Modificando ${this.produto.pro_c_produto}`;
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    } else {
      this.produto = {
        pro_pk: 0,
        pro_fk_grupo: 0,
        pro_fk_loja: this.idLoja,
        pro_b_ativo: true,
        pro_c_barcode: "",
        pro_c_descricao: "",
        pro_c_produto: "",
        pro_f_preco: 0.0,
        pro_f_preco_prom: 0.0
      };
      this.caption = "Novo produto";
    }
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onDetalhesChanged() {
    // console.log("ProdutoFormModalPage.onDetalhesChanged");
    this._refresh();
  }
  //#endregion
}
