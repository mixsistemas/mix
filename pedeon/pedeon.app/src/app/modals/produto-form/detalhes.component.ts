//#region ng
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
//#endregion

//#region ionic
import {
  // ActionSheetController,
  LoadingController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region app models
enum DetalhesProdutoTab {
  obrigatorios,
  opcionais
}
import { IDetalheProdutos } from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import {
  DetalhesProdutosService,
  StorageService
} from "../../modules/_services";
// import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { DetalheProdutoFormModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-produto-form-detalhes",
  templateUrl: "./detalhes.component.html",
  styleUrls: ["./detalhes.component.scss"]
})
export class ProdutoFormDetalhesComponent implements OnInit {
  //#region comm
  @Input() idProduto: number;
  @Input() idLoja: number = 0;
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  //#endregion

  //#region publics
  detalhes: {
    obrigatorios: IDetalheProdutos[];
    opcionais: IDetalheProdutos[];
  };
  jwt: string;
  Tab = DetalhesProdutoTab;
  tab: DetalhesProdutoTab;
  //#endregion

  //#region constructor
  constructor(
    // private _actionCtrl: ActionSheetController,
    private _detalhesServ: DetalhesProdutosService,
    // private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this.tab = DetalhesProdutoTab.obrigatorios;
      this._refresh();
    });
  }
  //#endregion

  //#region functions
  private async _refresh(onSuccess?: any) {
    // console.log(this.idProduto);
    if (this.idProduto > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this.detalhes = null;
        this._detalhesServ.detalhesJwt(this.idProduto, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              // obrigatórios
              // console.log(api.data.obrigatorios || []);
              let obrigatorios: IDetalheProdutos[] =
                this._detalhesServ.fixes(api.data.obrigatorios) || [];
              const LOB: number = obrigatorios.length;
              // console.log(LOB, obrigatorios);
              // Calcula posições anteriores.
              var v: number = 0;
              for (let i = 0; i < LOB; i++) {
                // console.log(i, v);
                obrigatorios[i].prior = v;
                v = obrigatorios[i].det_pk;
              } // for
              // Calcula posições posteriores.
              v = 0;
              for (let i = LOB - 1; i >= 0; i--) {
                // console.log(i, v);
                obrigatorios[i].next = v;
                v = obrigatorios[i].det_pk;
              } // for

              // opcionais
              let opcionais: IDetalheProdutos[] = this._detalhesServ.fixes(
                api.data.opcionais || []
              );
              const LOP: number = opcionais.length;
              // console.log(LOP, opcionais);
              // Calcula posições anteriores.
              v = 0;
              for (let i = 0; i < LOP; i++) {
                // console.log(i, v);
                opcionais[i].prior = v;
                v = opcionais[i].det_pk;
              } // for
              // Calcula posições posteriores.
              v = 0;
              for (let i = LOP - 1; i >= 0; i--) {
                // console.log(i, v);
                opcionais[i].next = v;
                v = opcionais[i].det_pk;
              } // for

              // console.log(obrigatorios);
              // console.log(opcionais);
              this.detalhes = {
                obrigatorios: obrigatorios,
                opcionais: opcionais
              };

              if (onSuccess && typeof onSuccess === "function") {
                onSuccess();
              } // if
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _detalheForm(detalhe: IDetalheProdutos) {
    // console.log("_grupoEdit");
    const modal = await this.modalCtrl.create({
      component: DetalheProdutoFormModalPage,
      componentProps: {
        detalhe: detalhe,
        idLoja: this.idLoja,
        idProduto: this.idProduto
      }
    });

    modal.onDidDismiss().then(() => {
      // this._refresh();
      this.onDetalhesChanged();
    });

    return await modal.present();
  }
  //#endregion

  //#region methods
  onNovoDetalheClick() {
    // console.log("onNovoDetalheClick");
    this._detalheForm(null);
  }

  onDetalhesChanged() {
    // console.log("ProdutoFormDetalhesComponent.onDetalhesChanged");
    this.onChanged.emit(true);
    this._refresh();
  }
  //#endregion
}
