//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
// import { environment } from "../../../environments/environment";
import { FormValidation } from "../../modules/_mix/_models/_classes";
import {
  IGrupoProdutos,
  IProduto
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  GruposProdutosService,
  ProdutosService,
  StorageService
} from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import { ZoomImgModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-produto-form-produto",
  templateUrl: "./produto.component.html",
  styleUrls: ["./produto.component.scss"]
})
export class ProdutoFormProdutoComponent implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() produto: IProduto;
  //#endregion

  //#region publics
  file: File;
  fv: FormValidation;
  grupos: IGrupoProdutos[];
  jwt: string;
  novo: boolean;
  produtoForm: FormGroup;
  // url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _gruposServ: GruposProdutosService,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.produtoForm = formBuilder.group({
      produto: [
        "",
        Validators.compose([Validators.maxLength(60), Validators.required])
      ],
      ativo: [true],
      barcode: ["", Validators.compose([Validators.maxLength(20)])],
      descricao: ["", Validators.compose([Validators.maxLength(255)])],
      idGrupo: ["", Validators.compose([Validators.required])],
      preco: ["0.00", Validators.compose([Validators.required])],
      precoProm: ["0.00"]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._buscaGrupos();
    });

    setTimeout(() => {
      // console.log(this.produto);
      if (this.produto) {
        this.novo = this.produto.pro_pk == 0;
      } else {
        this.novo = false;
      } // else
      if (!this.novo) {
        this.produtoForm.patchValue({
          produto: this.produto.pro_c_produto,
          ativo: this.produto.pro_b_ativo,
          barcode: this.produto.pro_c_barcode,
          descricao: this.produto.pro_c_descricao,
          idGrupo: this.produto.pro_fk_grupo,
          preco: this.produto.pro_f_preco,
          precoProm: this.produto.pro_f_preco_prom
        });
        // console.log(this.produtoForm.controls.idGrupo);
      } // if
    }, 0);
  }
  //#endregion

  //#region functions
  private async _buscaGrupos() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.grupos = null;
        this._gruposServ.gruposJwt(this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.grupos = this._gruposServ.fixes(api.data) || [];
              // console.log(grupos);
            } // endif
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _novoProduto() {
    // console.log("_novoProduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    let produto = {
      pro_fk_grupo: this.produtoForm.value.idGrupo,
      pro_b_ativo: this.produtoForm.value.ativo,
      pro_c_barcode: this.produtoForm.value.barcode,
      pro_c_descricao: this.produtoForm.value.descricao,
      pro_c_produto: this.produtoForm.value.produto,
      // pro_c_img: "",
      pro_f_preco: this.produtoForm.value.preco,
      pro_f_preco_prom: this.produtoForm.value.precoProm
    };
    // console.log(produto);
    // console.log(this.produtoForm.value);
    // console.log(this.jwt);

    loading.present().then(() => {
      this._produtosServ.novo(produto, this.jwt).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          loading.dismiss();
          this.fv.setApiErrors(api.errors);
          if (api.ok) {
            this.modalCtrl.dismiss();
          } else {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  private async _gravaProduto() {
    // console.log("_gravaproduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    let produto = {
      pro_pk: this.produto.pro_pk,
      pro_fk_grupo: this.produtoForm.value.idGrupo,
      pro_b_ativo: this.produtoForm.value.ativo,
      pro_c_barcode: this.produtoForm.value.barcode,
      pro_c_descricao: this.produtoForm.value.descricao,
      pro_c_produto: this.produtoForm.value.produto,
      // pro_c_img: "",
      pro_f_preco: this.produtoForm.value.preco,
      pro_f_preco_prom: this.produtoForm.value.precoProm
    };
    // console.log(this.produtoForm.value);
    // console.log(produto);
    // console.log(this.jwt);

    loading.present().then(() => {
      this._produtosServ.update(produto, this.jwt).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          loading.dismiss();
          this.fv.setApiErrors(api.errors);
          if (api.ok) {
            this.modalCtrl.dismiss();
          } else {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  pro_img(img: string): string {
    return this._produtosServ.img(img, true);
  }
  //#endregion

  //#region methods
  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }

  onSubmit() {
    console.log(this.novo);
    if (this.novo) {
      this._novoProduto();
    } else {
      this._gravaProduto();
    } // else
  }
  //#endregion

  /* changeListener($event): void {
    this.file = $event.target.files[0];
  }

  saveProfile_click() {
    console.log("saveProfile_click");
    // Add your code here
    this.afAuth.authState.take(1).subscribe(auth => {
      this.afDatabase
        .object(profile/${this.uid}
        .set(this.profile)
        .then(() => {
          this.uploadProfileImage();
          this.navCtrl.pop();
        });
    }); 
  }

  uploadProfileImage() {
    console.log("uploadProfileImage");
    let fileRef = firebase.storage().ref("profileImages/" + this.uid + ".jpg");
    fileRef.put(this.file).then(function(snapshot) {
      console.log("Uploaded a blob or file!");
    });
  } */
}
