//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
import { DetalheProdutoFormModalModule } from "../detalhe-produto-form/detalhe-produto-form.module";
import { ItemDetalheProdutoFormModalModule } from "../item-detalhe-produto-form/item-detalhe-produto-form.module";
//#endregion

//#region app modals
import { ProdutoFormModalPage } from "..";
//#endregion

//#region app components
import {
  DetalhesListComponent,
  ProdutoFormProdutoComponent,
  ProdutoFormDetalhesComponent
} from ".";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    DetalheProdutoFormModalModule,
    FormsModule,
    ItemDetalheProdutoFormModalModule,
    ReactiveFormsModule,
    IonicModule
  ],
  declarations: [
    // components
    DetalhesListComponent,
    ProdutoFormProdutoComponent,
    ProdutoFormDetalhesComponent,
    // modals
    ProdutoFormModalPage
  ],
  entryComponents: [ProdutoFormModalPage]
})
export class ProdutoFormModalModule {}
