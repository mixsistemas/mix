//#region ng
import { AfterViewInit, Component, EventEmitter, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region models
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region services
import { ContasService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-mudar-senha",
  templateUrl: "mudar-senha.component.html",
  styleUrls: ["mudar-senha.component.scss"]
})
export class MudarSenhaComponent implements AfterViewInit {
  //#region comm
  @Output() onSubmitted: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  fv: FormValidation;
  mudarSenhaForm: FormGroup;
  //#endregion

  //#region methods
  onMudarSenhaFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(
    true
  );
  //#endregion

  //#region constructor
  constructor(
    private _contasServ: ContasService,
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.mudarSenhaForm = formBuilder.group({
      senhaAtual: [
        "",
        Validators.compose([Validators.minLength(5), Validators.required])
      ],
      novaSenha: [
        "",
        Validators.compose([Validators.minLength(5), Validators.required])
      ],
      confirmacao: [
        "",
        Validators.compose([Validators.minLength(5), Validators.required])
      ]
    });
  }
  //#endregion

  //#region lifecycles
  ngAfterViewInit() {
    // console.log("ngAfterViewInit");
    setTimeout(() => {
      this.onMudarSenhaFocusEvent.emit();
    }, 0);
  }
  //#endregion

  //#region functions
  private async _mudaSenha() {
    // console.log(_gravaConta);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      loading.present().then(() => {
        loading.present().then(() => {
          this._contasServ
            .mudarSenha(this.mudarSenhaForm.value, token)
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                loading.dismiss();
                this.fv.setApiErrors(api.errors);
                if (api.ok) {
                  this._ionicServ.toast("Senha modificada com sucesso.");
                  this.modalCtrl.dismiss();
                } else {
                  this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
                } // else
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      });
    });
  }
  //#endregion

  //#region methods
  onSubmit() {
    this._mudaSenha();
  }
  //#endregion
}
