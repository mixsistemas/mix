//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region 3rd
import { CpfPipe, TelefonePipe } from "ng2-brpipes";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { IConta } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ContasService, StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-editar-perfil",
  templateUrl: "editar-perfil.component.html",
  styleUrls: ["editar-perfil.component.scss"]
})
export class EditarPerfilComponent implements OnInit, AfterViewInit {
  //#region comm
  @Output() onSubmitted: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  conta: IConta;
  fv: FormValidation;
  perfilForm: FormGroup;
  //#endregion

  //#region methods
  onPerfilFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  //#endregion

  //#region constructor
  constructor(
    private _contasServ: ContasService,
    private _cpfPipe: CpfPipe,
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService,
    private _telPipe: TelefonePipe
  ) {
    this.fv = new FormValidation();

    this.perfilForm = formBuilder.group({
      nome: [
        "",
        Validators.compose([Validators.maxLength(20), Validators.required])
      ],
      sobrenome: [
        "",
        Validators.compose([Validators.maxLength(20), Validators.required])
      ],
      fone: ["", Validators.compose([Validators.required])],
      cpf: [""],
      email: [
        "",
        Validators.compose([
          Validators.maxLength(60),
          Validators.required,
          Validators.email
        ])
      ]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._buscaConta();
  }

  ngAfterViewInit() {
    // console.log("ngAfterViewInit");
    setTimeout(() => {
      this.onPerfilFocusEvent.emit();
    }, 0);
  }
  //#endregion

  //#region functions
  private async _buscaConta() {
    this.conta = null;
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      loading.present().then(() => {
        this._contasServ.conta(token).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.conta = this._contasServ.fix(api.data);
              console.log();
              this.perfilForm.patchValue({
                nome: this.conta.con_c_nome,
                sobrenome: this.conta.con_c_sobrenome,
                fone: this._telPipe.transform(this.conta.con_c_fone),
                cpf: this.conta.con_c_cpf
                  ? this._cpfPipe.transform(this.conta.con_c_cpf)
                  : null,
                email: this.conta.con_c_email
              });
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    });
  }

  private async _gravaConta() {
    // console.log(_gravaConta);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      loading.present().then(() => {
        this._contasServ.update(this.perfilForm.value, token).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              this._storageServ.authJwtSet(api.data.jwt, () => {
                this._storageServ.onAuthChanged.next(api.data.payload);
                this._ionicServ.toast("Perfil modificado com sucesso.");
                this.modalCtrl.dismiss();
              });
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    });
  }
  //#endregion

  //#region methods
  /*
  async onEsqueciSenhaClick() {
    const alert = await this._alertCtrl.create({
      header: "Recuperação de senha",
      subHeader: "Indique seu E-mail",
      inputs: [
        {
          name: "email",
          value: "",
          type: "email",
          placeholder: "mix****dev@gmail.com"
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel");
          }
        },
        {
          text: "Ok",
          handler: data => {
            console.log(data);
          }
        }
      ]
    });

    await alert.present();
  }
  */

  onSubmit() {
    this._gravaConta();
  }
  //#endregion
}
