//#region ng
import { Component, Input, OnChanges, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region models
import { EditarPerfilTab } from "../../modules/_shared/_models/_enums";
//#endregion

@Component({
  selector: "po-sua-conta",
  templateUrl: "./sua-conta.page.html",
  styleUrls: ["./sua-conta.page.scss"]
})
export class SuaContaModalPage implements OnChanges, OnInit {
  //#region comm
  @Input() value: any;
  //#endregion

  //#region publics
  Tab = EditarPerfilTab;
  tab: EditarPerfilTab;
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) {
    this.tab = EditarPerfilTab.perfil;
  }
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    console.log(this.value);
  }
  ngOnInit() {}
  //#endregion

  //#region app mehtods
  onModalClose() {
    this.modalCtrl.dismiss();
  }
  //#endregion
}
