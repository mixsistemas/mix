//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region 3rd
import { BrMaskerModule } from "br-mask";
// import { TelefonePipe } from "ng2-brpipes";
//#endregion

//#region app modules
import { MixSharedModule } from "../../modules/_mix/_shared/mix-shared.module";
import { CpfPipe, Ng2BRPipesModule, TelefonePipe } from "ng2-brpipes";
//#endregion

//#region app modals
import { SuaContaModalPage } from "..";
//#endregion

//#region app components
import { EditarPerfilComponent, MudarSenhaComponent } from ".";
//#endregion

@NgModule({
  imports: [
    BrMaskerModule,
    CommonModule,
    FormsModule,
    IonicModule,
    MixSharedModule,
    Ng2BRPipesModule,
    ReactiveFormsModule
  ],
  declarations: [
    // components
    MudarSenhaComponent,
    EditarPerfilComponent,
    // modals
    SuaContaModalPage
  ],
  entryComponents: [SuaContaModalPage],
  providers: [CpfPipe, TelefonePipe]
})
export class SuaContaModalModule {}
