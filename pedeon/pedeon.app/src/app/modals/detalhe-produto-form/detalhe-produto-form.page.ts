//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import {
  IDetalheProdutos,
  IGrupoProdutos
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  DetalhesProdutosService,
  GruposProdutosService,
  StorageService
} from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-detalhe-produto-form",
  templateUrl: "./detalhe-produto-form.page.html",
  styleUrls: ["./detalhe-produto-form.page.scss"]
})
export class DetalheProdutoFormModalPage implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() idProduto: number;
  @Input() detalhe: IDetalheProdutos;
  //#endregion

  //#region publics
  caption: string;
  detalheForm: FormGroup;
  fv: FormValidation;
  grupos: IGrupoProdutos[];
  jwt: string;
  novo: boolean;
  //#endregion

  //#region constructor
  constructor(
    private _detalhesServ: DetalhesProdutosService,
    public formBuilder: FormBuilder,
    private _gruposServ: GruposProdutosService,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.detalheForm = formBuilder.group({
      detalhe: [
        "",
        Validators.compose([Validators.maxLength(60), Validators.required])
      ],
      valor: ["0.00", Validators.compose([Validators.required])],
      ativo: [true],
      min: ["0", Validators.compose([Validators.required])],
      max: ["0", Validators.compose([Validators.required])],
      idGrupo: [""]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      this._buscaGrupos();
    });

    setTimeout(() => {
      this.novo = this.detalhe == null;
      this.caption = this.novo
        ? "Novo detalhe"
        : `Modificando detalhe ${this.detalhe.det_c_detalhe}`;
      if (!this.novo) {
        this.detalheForm.patchValue({
          detalhe: this.detalhe.det_c_detalhe,
          valor: this.detalhe.det_f_valor,
          ativo: this.detalhe.det_b_ativo,
          min: this.detalhe.det_i_min,
          max: this.detalhe.det_i_max,
          idGrupo: this.detalhe.det_fk_grupo_itens
        });
      } // if
    }, 0);
  }
  //#endregion

  //#region functions
  private async _buscaGrupos() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.grupos = null;
        this._gruposServ.gruposJwt(this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.grupos = this._gruposServ.fixes(api.data) || [];
              // console.log(grupos);
            } // endif
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _novoDetalhe() {
    console.log("_novoDetalhe");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    let detalhe = {
      det_fk_grupo_itens: this.detalheForm.value.idGrupo,
      det_b_ativo: this.detalheForm.value.ativo,
      det_c_detalhe: this.detalheForm.value.detalhe,
      det_i_max: this.detalheForm.value.max,
      det_i_min: this.detalheForm.value.min,
      det_f_valor: this.detalheForm.value.valor
    };
    // console.log(detalhe);
    // console.log(this.detalheForm.value);
    // console.log(this.jwt);
    loading.present().then(() => {
      this._detalhesServ.novo(this.idProduto, detalhe, this.jwt).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          loading.dismiss();
          this.fv.setApiErrors(api.errors);
          if (api.ok) {
            this.modalCtrl.dismiss();
          } else {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  private async _gravaDetalhe() {
    // console.log("_gravaproduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    let detalhe = {
      det_pk: this.detalhe.det_pk,
      det_fk_grupo_itens: this.detalheForm.value.idGrupo,
      det_b_ativo: this.detalheForm.value.ativo,
      det_c_detalhe: this.detalheForm.value.detalhe,
      det_i_max: this.detalheForm.value.max,
      det_i_min: this.detalheForm.value.min,
      det_f_valor: this.detalheForm.value.valor
    };
    // console.log(this.detalheForm.value);
    // console.log(detalhe);
    // console.log(this.idLoja);
    // console.log(this.idProduto);
    // console.log(this.jwt);

    loading.present().then(() => {
      this._detalhesServ.update(this.idProduto, detalhe, this.jwt).subscribe(
        (api: IApiResponse) => {
          console.log(api);
          loading.dismiss();
          this.fv.setApiErrors(api.errors);
          if (api.ok) {
            this.modalCtrl.dismiss();
          } else {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    // console.log("onSubmit");
    if (this.novo) {
      this._novoDetalhe();
    } else {
      this._gravaDetalhe();
    } // else
  }
  //#endregion
}
