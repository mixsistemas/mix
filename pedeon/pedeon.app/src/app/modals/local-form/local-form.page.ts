//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-local-form",
  templateUrl: "./local-form.page.html",
  styleUrls: ["./local-form.page.scss"]
})
export class LocalFormModalPage implements OnInit {
  //#region comm
  @Input() cepInfo: IViaCep;
  //#endregion

  //#region publics
  enderecoForm: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    public modalCtrl: ModalController
  ) {
    this.enderecoForm = formBuilder.group({
      cep: ["", Validators.required],
      uf: ["", Validators.required],
      localidade: ["", Validators.required],
      logradouro: ["", Validators.required],
      bairro: ["", Validators.required],
      nro: ["", Validators.required],
      complemento: [""],
      referencia: [""]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      // console.log(this.cepInfo);
      this.enderecoForm.controls.cep.setValue(this.cepInfo.cep);
      this.enderecoForm.controls.uf.setValue(this.cepInfo.uf);
      this.enderecoForm.controls.localidade.setValue(this.cepInfo.localidade);
      this.enderecoForm.controls.logradouro.setValue(this.cepInfo.logradouro);
      this.enderecoForm.controls.bairro.setValue(this.cepInfo.bairro);
    }, 0);
  }
  //#endregion

  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    // console.log(this.enderecoForm.valid);
    if (this.enderecoForm.valid) {
      this.modalCtrl.dismiss(this.enderecoForm.value);
    } else {
      this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
    } // else
  }
  //#endregion
}
