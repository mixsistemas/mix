//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modals
import { LocalFormModalPage } from "..";
//#endregion

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ReactiveFormsModule],
  declarations: [
    // components
    // modals
    LocalFormModalPage
  ],
  entryComponents: [LocalFormModalPage]
})
export class LocalFormModalModule {}
