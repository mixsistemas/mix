//#region ng
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region app models
import {
  IDetalheProdutos,
  IItemDetalhesProdutos
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse } from "../../modules/_mix/_models/_interfaces";
import { FormValidation } from "../../modules/_mix/_models/_classes";
import { FORM_CORRIJA_CAMPOS } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  ItensDetalhesProdutosService,
  // GruposProdutosService,
  StorageService
} from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-item-detalhe-produto-form",
  templateUrl: "./item-detalhe-produto-form.page.html",
  styleUrls: ["./item-detalhe-produto-form.page.scss"]
})
export class ItemDetalheProdutoFormModalPage implements OnInit {
  //#region comm
  @Input() idLoja: number;
  @Input() detalhe: IDetalheProdutos;
  @Input() item: IItemDetalhesProdutos;
  //#endregion

  //#region publics
  caption: string;
  fv: FormValidation;
  itemForm: FormGroup;
  jwt: string;
  novo: boolean;
  //#endregion

  //#region constructor
  constructor(
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _itensServ: ItensDetalhesProdutosService,
    private _loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {
    this.fv = new FormValidation();

    this.itemForm = formBuilder.group({
      item: [
        "",
        Validators.compose([Validators.maxLength(60), Validators.required])
      ],
      valor: ["0.00", Validators.compose([Validators.required])],
      ativo: [true]
    });
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._storageServ.authJwtGet((jwt: string) => {
      // console.log(jwt);
      this.jwt = jwt;
      // this._buscaGrupos();
    });

    setTimeout(() => {
      this.novo = this.item == null;
      this.caption = this.novo
        ? "Novo item de detalhe"
        : `Modificando ${this.item.idp_c_item}`;
      if (!this.novo) {
        this.itemForm.patchValue({
          item: this.item.idp_c_item,
          valor: this.item.idp_f_valor,
          ativo: this.item.idp_b_ativo
        });
      } else {
        this.itemForm.patchValue({
          item: "",
          valor: 0,
          ativo: true
        });
      }
    }, 0);
  }
  //#endregion

  //#region functions
  private async _novoItem() {
    // console.log("_novoItem");
    if (this.detalhe.det_pk > 0 && this.jwt) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      let item: IItemDetalhesProdutos = {
        idp_fk_detalhe: this.detalhe.det_pk,
        idp_b_ativo: this.itemForm.value.ativo,
        idp_c_item: this.itemForm.value.item,
        idp_f_valor: this.itemForm.value.valor
      };
      // console.log(item);
      // console.log(this.itemForm.value);
      // console.log(this.jwt);
      loading.present().then(() => {
        this._itensServ.novo(this.idLoja, item, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              this.modalCtrl.dismiss();
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _gravaItem() {
    // console.log("_gravaItem");
    if (this.detalhe.det_pk > 0 && this.jwt) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });

      let item: IItemDetalhesProdutos = {
        idp_pk: this.item.idp_pk,
        idp_fk_detalhe: this.detalhe.det_pk,
        idp_b_ativo: this.itemForm.value.ativo,
        idp_c_item: this.itemForm.value.item,
        idp_f_valor: this.itemForm.value.valor
      };
      // console.log(item);

      // console.log(this.itemForm.value);
      // console.log(this.jwt);
      loading.present().then(() => {
        this._itensServ.update(this.idLoja, item, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            this.fv.setApiErrors(api.errors);
            if (api.ok) {
              this.modalCtrl.dismiss();
            } else {
              this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
            } // else
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }
  //#endregion
  /*
  private async _buscaGrupos() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    if (this.jwt) {
      loading.present().then(() => {
        this.grupos = null;
        this._gruposServ.gruposJwt(this.idLoja, this.jwt).subscribe(
          (api: IApiResponse) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.grupos = this._gruposServ.fixes(api.data) || [];
              // console.log(grupos);
            } // endif
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  private async _gravaItem() {
    // console.log("_gravaproduto");
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    let detalhe = {
      idp_pk: this.item.idp_pk,
      idp_fk_grupo_itens: this.itemForm.value.idGrupo,
      idp_b_ativo: this.itemForm.value.ativo,
      idp_c_item: this.itemForm.value.detalhe,
      idp_i_max: this.itemForm.value.max,
      idp_i_min: this.itemForm.value.min,
      idp_f_valor: this.itemForm.value.valor
    };
    // console.log(this.itemForm.value);
    // console.log(detalhe);
    // console.log(this.idLoja);
    // console.log(this.idDetalhe);
    // console.log(this.jwt);

    loading.present().then(() => {
      this._itensServ.update(this.idDetalhe, detalhe, this.jwt).subscribe(
        (api: IApiResponse) => {
          console.log(api);
          loading.dismiss();
          this.fv.setApiErrors(api.errors);
          if (api.ok) {
            this.modalCtrl.dismiss();
          } else {
            this._ionicServ.toast(FORM_CORRIJA_CAMPOS, "danger");
          } // else
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }
  //#endregion
  */
  //#region methods
  onModalClose() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    // console.log("onSubmit");
    if (this.novo) {
      this._novoItem();
    } else {
      this._gravaItem();
    } // else
  }
  //#endregion
}
