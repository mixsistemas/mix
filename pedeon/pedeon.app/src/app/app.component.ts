//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { Platform, ModalController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { LoginCadastroTab } from "./modules/_shared/_models/_enums";
import { IModalResponse, IViaCep } from "./modules/_mix/_models/_interfaces";
import { IAuthJwt } from "./modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { UtilsService } from "./modules/_mix/_core/_services";
import { AuthService, StorageService } from "./modules/_services";
//#endregion

//#region app modals
import {
  AreaLojistaModalPage,
  LojasFavoritasModalPage,
  LoginCadastroModalPage,
  ProdutosFavoritosModalPage,
  SeusEnderecosModalPage,
  SuaContaModalPage
} from "./modals";
//#endregion

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy {
  //#region publics
  conta: IAuthJwt;
  Tab = LoginCadastroTab;
  local: IViaCep;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    public modalCtrl: ModalController,
    private _platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _storageServ: StorageService,
    private _router: Router,
    private _utilsServ: UtilsService
  ) {
    /* this._router.events.forEach(event => {
      if (event instanceof NavigationStart) {
        console.log(event.url);
      } // if
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    }); */
    this.initializeApp();
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._subs.push(
      this._storageServ.onAuthChanged.subscribe((payload: IAuthJwt) => {
        // console.log(payload);
        this.conta = payload;
      })
    );

    this._subs.push(
      this._storageServ.onLocalChangedEvent.subscribe((local: IViaCep) => {
        // console.log(local);
        this.local = local;
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  initializeApp() {
    console.log("initializeApp");
    this._platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  //#endregion

  //#region methods
  async onLoginCadastroClick(tab: LoginCadastroTab) {
    // console.log("onLoginCadastroClick", tab);
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: tab }
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  onEditarPerfilClick() {
    /* // console.log("onEditarPerfilClick");

    const modal = await this.modalCtrl.create({
      component: SuaContaModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); 

    return await modal.present(); */
  }

  async onSuaContaClick() {
    // console.log("onEditarPerfilClick");

    const modal = await this.modalCtrl.create({
      component: SuaContaModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  async onLojasFavoritasClick() {
    // console.log("onEditarPerfilClick");

    const modal = await this.modalCtrl.create({
      component: LojasFavoritasModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  async onProdutosFavoritosClick() {
    // console.log("onEditarPerfilClick");

    const modal = await this.modalCtrl.create({
      component: ProdutosFavoritosModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  async onSeusEnderecosClick() {
    // console.log("onSeusEnderecosClick");
    const modal = await this.modalCtrl.create({
      component: SeusEnderecosModalPage,
      componentProps: {}
    });

    modal.onDidDismiss().then((data: IModalResponse) => {
      // console.log(data);
      if (data.data) {
        let local: IViaCep = data.data;
        local.key = this._utilsServ.guid();
        this._storageServ.localSet(local, () => {
          this._storageServ.localAdd(local);
          // if (window.location.href.includes("sel-local"))
          this._router.navigate(["/sel-loja"]);
        });
      } // if
    });

    return await modal.present();
  }

  async onAreaLojistaClick() {
    // console.log("onAreaLojistaClick");
    // console.log(this.conta);
    const modal = await this.modalCtrl.create({
      component: AreaLojistaModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  onLogoutClick() {
    this._authServ.logout();
  }

  onIntroClick() {
    this._storageServ.splashRemove(() => {
      setTimeout(() => {
        this._router.navigate(["/splash"], { replaceUrl: true });
      }, 100);
    });

    /* this._storageServ.introGet(status => {
      console.log(status);
    }); */
  }
  //#endregion
}
