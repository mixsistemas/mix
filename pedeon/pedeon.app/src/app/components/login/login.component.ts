//#region ng
import { Component, EventEmitter, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
//#endregion

//#region ionic
import { AlertController, LoadingController } from "@ionic/angular";
//#endregion

//#region models
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region services
import {
  AuthService,
  StorageService,
  EnderecosService
} from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-login",
  templateUrl: "login.component.html",
  styleUrls: ["login.component.scss"]
})
export class LoginComponent {
  //#region comm
  @Output() onLogin: EventEmitter<IViaCep> = new EventEmitter<IViaCep>();
  //#endregion

  //#region publics
  loginForm: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    private _alertCtrl: AlertController,
    private _authServ: AuthService,
    private _enderecosServ: EnderecosService,
    public formBuilder: FormBuilder,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    private _storageServ: StorageService
  ) {
    this.loginForm = formBuilder.group({
      email: [
        "teste@teste.com",
        Validators.compose([
          Validators.maxLength(60),
          Validators.required,
          Validators.email
        ])
      ],
      senha: [
        "",
        Validators.compose([Validators.minLength(5), Validators.required])
      ]
    });
  }
  //#endregion

  //#region methods
  async onEsqueciSenhaClick() {
    const alert = await this._alertCtrl.create({
      header: "Recuperação de senha",
      subHeader: "Indique seu E-mail",
      inputs: [
        {
          name: "email",
          value: "",
          type: "email",
          placeholder: "mix****dev@gmail.com"
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel");
          }
        },
        {
          text: "Ok",
          handler: data => {
            console.log(data);
          }
        }
      ]
    });

    await alert.present();
  }

  async onSubmit() {
    // console.log(this.loginForm.value);
    // this._router.navigate(["/sel-loja"]);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._authServ.login(this.loginForm.value).subscribe(
        (api: any) => {
          // console.log(api);
          loading.dismiss();
          if (api.ok) {
            if (api.data.jwt) {
              this._storageServ.authJwtSet(api.data.jwt, () => {
                this._storageServ.onAuthChanged.next(api.data.payload);
                const LOCAL = this._enderecosServ.end2local(
                  api.data.payload.endereco
                );
                // console.log(LOCAL);
                this.onLogin.emit(LOCAL);
              });
            } else {
              this.loginForm.reset();
              this._ionicServ.alert("Conta não encontrada.", "Login");
            } // else
          } // if
        },
        err => {
          this.loginForm.reset();
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }
  //#endregion
}
