//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region ionic
import { ModalController } from "@ionic/angular";
//#endregion

//#region app models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import { IConta } from "../../modules/_shared/_models/_interfaces";
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { AuthService, StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import { LoginCadastroModalPage, SuaContaModalPage } from "../../modals";
//#endregion

@Component({
  selector: "po-conta-info",
  templateUrl: "conta-info.component.html",
  styleUrls: ["conta-info.component.scss"]
})
export class ContaInfoComponent implements OnInit {
  //#region comm
  @Input() conta: IConta;
  //#endregion

  //#region publics
  local: IViaCep;
  Tab = LoginCadastroTab;
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    public modalCtrl: ModalController,
    private _storageServ: StorageService
  ) {}

  //#region lifecycles
  ngOnInit() {
    this.local = null;
    // let loading = await this.loadingCtrl.create({
    //   // message: "Aguarde...",
    //   translucent: true
    // });
    // loading.present().then(() => {

    this._storageServ.localGet((local: string) => {
      // console.log(local);
      this.local = JSON.parse(local);
    });
  }
  //#endregion

  //#region methods
  onLogoutClick() {
    this._authServ.logout();
  }

  async onSuaContaClick() {
    // console.log("onEditarPerfilClick");

    const modal = await this.modalCtrl.create({
      component: SuaContaModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  async onEntrarClick(tab: LoginCadastroTab) {
    // console.log("onLoginCadastroClick", tab);
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: tab }
    });

    /* modal.onDidDismiss().then((data) => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  //#endregion
}
