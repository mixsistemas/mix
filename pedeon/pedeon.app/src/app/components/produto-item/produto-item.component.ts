//#region ng
import { Component, EventEmitter, Input, Output } from "@angular/core";
//#endregion

//#region app models
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-produto-item",
  templateUrl: "produto-item.component.html",
  styleUrls: ["produto-item.component.scss"]
})
export class ProdutoItemComponent {
  //#region comm
  @Input() produto: IProduto;
  @Input() lojaFechada: boolean = true;
  @Input() border: boolean = true;
  @Output() onImgClick: EventEmitter<string> = new EventEmitter<string>();
  @Output() onSelect: EventEmitter<IProduto> = new EventEmitter<IProduto>();
  //#endregion

  //#region constructor
  constructor(private _produtosServ: ProdutosService) {}
  //#endregion

  //#region functions
  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }
  //#endregion

  //#region methods
  onZoomImgClick(img: string) {
    // console.log(img);
    this.onImgClick.emit(img);
  }

  onSelectClick(produto) {
    this.onSelect.emit(produto);
  }
  //#endregion
}
