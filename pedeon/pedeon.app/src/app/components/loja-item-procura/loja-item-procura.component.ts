//#region ng
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-loja-item-procura",
  templateUrl: "loja-item-procura.component.html",
  styleUrls: ["loja-item-procura.component.scss"]
})
export class LojaItemProcuraComponent implements OnInit {
  //#region comm
  @Input() loja: ILoja;
  @Output("onSel") onSel: EventEmitter<number> = new EventEmitter();
  //#endregion

  //#region publics
  msgAbertura: string = "";
  url_imgs = environment.url.img;
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      this.msgAbertura = this.loja.loj_c_horario_abertura
        ? `Abre às ${this.loja.loj_c_horario_abertura}`
        : "Não abre mais hoje";
    }, 0);
  }
  //#endregion

  //#region methods
  onSelClick() {
    this.onSel.emit(this.loja.loj_pk);
  }
  //#endregion
}
