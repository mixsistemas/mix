//#region ng
import { Component, EventEmitter, Input, Output } from "@angular/core";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-produto-item-sel",
  templateUrl: "produto-item-sel.component.html",
  styleUrls: ["produto-item-sel.component.scss"]
})
export class ProdutoItemSelComponent {
  //#region comm
  @Input() produto: IProduto;
  @Input() delBtn: boolean = false;
  @Output() onSelected: EventEmitter<IProduto> = new EventEmitter<IProduto>();
  @Output() onImgClick: EventEmitter<string> = new EventEmitter<string>();
  //#endregion

  //#region publics
  url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(private _produtosServ: ProdutosService) {}
  //#endregion

  //#region functions
  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }
  //#endregion

  //#region methods
  onZoomImgClick(img: string) {
    // console.log(img);
    this.onImgClick.emit(img);
  }

  onSelClick(produto: IProduto) {
    // console.log(produto);
    this.onSelected.emit(produto);
  }
  //#endregion
}
