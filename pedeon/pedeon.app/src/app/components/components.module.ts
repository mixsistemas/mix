//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app components
import {
  ContaInfoComponent,
  LocalArmazenadosComponent,
  LoginComponent,
  LojaItemComponent,
  LojaItemProcuraComponent,
  LojaItemSelComponent,
  PrecoProdutoComponent,
  ProdutoItemComponent,
  ProdutoItemProcuraComponent,
  ProdutoItemSelComponent
} from ".";
//#endregion

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ReactiveFormsModule],
  declarations: [
    ContaInfoComponent,
    LocalArmazenadosComponent,
    LoginComponent,
    LojaItemComponent,
    LojaItemProcuraComponent,
    LojaItemSelComponent,
    PrecoProdutoComponent,
    ProdutoItemComponent,
    ProdutoItemProcuraComponent,
    ProdutoItemSelComponent
  ],
  exports: [
    ContaInfoComponent,
    LocalArmazenadosComponent,
    LoginComponent,
    LojaItemComponent,
    LojaItemProcuraComponent,
    LojaItemSelComponent,
    PrecoProdutoComponent,
    ProdutoItemComponent,
    ProdutoItemProcuraComponent,
    ProdutoItemSelComponent
  ]
})
export class ComponentsModule {}
