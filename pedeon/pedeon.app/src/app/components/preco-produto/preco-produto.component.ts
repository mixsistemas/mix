//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region app models
enum TiposPrecos {
  inicial = 0,
  promocional,
  normal
}
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-preco-produto",
  templateUrl: "preco-produto.component.html",
  styleUrls: ["preco-produto.component.scss"]
})
export class PrecoProdutoComponent implements OnInit {
  //#region comm
  @Input() produto: IProduto;
  @Input() lojaFechada: boolean = false;
  //#endregion

  //#region publics
  tipo: TiposPrecos;
  Tipo = TiposPrecos;
  //#endregion

  //#region lifecycles
  ngOnInit() {
    if (this.produto.num_detalhes > 0) {
      this.tipo = TiposPrecos.inicial;
    } else if (this.produto.pro_f_preco_prom > 0) {
      this.tipo = TiposPrecos.promocional;
    } else {
      this.tipo = TiposPrecos.normal;
    } // else
    // console.log(this.tipo, this.produto);
  }
  //#endregion
}
