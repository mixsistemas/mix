//#region ng
import { Component, EventEmitter, Input, Output } from "@angular/core";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-loja-item-sel",
  templateUrl: "loja-item-sel.component.html",
  styleUrls: ["loja-item-sel.component.scss"]
})
export class LojaItemSelComponent {
  //#region comm
  @Input() loja: ILoja;
  @Input() delBtn: boolean = false;
  @Output() onSelected: EventEmitter<ILoja> = new EventEmitter<ILoja>();
  //#endregion

  //#region publics
  url_imgs = environment.url.img;
  //#endregion

  //#region methods
  onSelClick(loja: ILoja) {
    // console.log(loja);
    this.onSelected.emit(loja);
  }
  //#endregion
}
