//#region ng
import { Component, EventEmitter, Input, Output } from "@angular/core";
//#endregion

//#region app models
// import { environment } from "../../../environments/environment";
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ProdutosService } from "../../modules/_services";
//#endregion

@Component({
  selector: "po-produto-item-procura",
  templateUrl: "produto-item-procura.component.html",
  styleUrls: ["produto-item-procura.component.scss"]
})
export class ProdutoItemProcuraComponent {
  //#region comm
  @Input() produto: IProduto;
  @Output("onImgClick") onImgClick: EventEmitter<string> = new EventEmitter();
  //#endregion

  //#region publics
  // url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(private _produtosServ: ProdutosService) {}
  //#endregion

  //#region functions
  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }
  //#endregion

  //#region methods
  onZoomImgClick(img: string) {
    // console.log(img);
    this.onImgClick.emit(img);
  }
  //#endregion
}
