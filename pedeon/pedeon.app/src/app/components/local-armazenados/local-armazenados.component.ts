//#region ng
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
//#endregion

//#region ionic
import { ActionSheetController } from "@ionic/angular";
//#endregion

//#region app models
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { StorageService } from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

@Component({
  selector: "po-local-armazenados",
  templateUrl: "local-armazenados.component.html",
  styleUrls: ["local-armazenados.component.scss"]
})
export class LocalArmazenadosComponent implements OnInit {
  //#region publics
  @Output() onSel: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region publics
  armazenados: IViaCep[];
  //#endregion

  //#region constructor
  constructor(
    private _actionCtrl: ActionSheetController,
    private _ionicServ: IonicService,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.armazenados = [];
    this._storageServ.enderecosGet(
      (enderecos: string) => (this.armazenados = JSON.parse(enderecos) || [])
    );
  }
  //#endregion

  //#region methods
  onLocalClick(local: IViaCep) {
    // console.log(local);
    this._storageServ.localSet(local, () => this.onSel.emit(local));
  }

  async onMenuClick(local: IViaCep) {
    if (local) {
      const actionSheet = await this._actionCtrl.create({
        header: `${local.logradouro}, ${local.nro} ${local.complemento}`,
        buttons: [
          {
            text: "Selecionar",
            // role: "destructive",
            icon: "/assets/icon/check.svg",
            handler: () => {
              this.onLocalClick(local);
            }
          },
          /* {
          text: "Gravar na conta",
          icon: "/assets/icon/save.svg",
          handler: () => {}
        }, */
          {
            text: "Excluir",
            icon: "/assets/icon/trash.svg",
            handler: () => {
              // console.log(local);
              this._storageServ.enderecoDel(local.key);
              this._ionicServ.toast("Local excluído com sucesso.", "danger");
              setTimeout(() => {
                this._refresh();
              }, 100);
            }
          },
          {
            text: "Cancelar",
            icon: "close",
            role: "cancel",
            handler: () => {
              // console.log("Cancel clicked");
            }
          }
        ]
      });
      await actionSheet.present();
    } // if
  }
  //#endregion
}
