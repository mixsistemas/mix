//#region ng
import { Component, Input, OnInit } from "@angular/core";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILoja } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "po-loja-item",
  templateUrl: "loja-item.component.html",
  styleUrls: ["loja-item.component.scss"]
})
export class LojaItemComponent implements OnInit {
  //#region comm
  @Input() loja: ILoja;
  @Input() border: boolean = true;
  //#endregion

  //#region publics
  msgAbertura: string = "";
  url_imgs = environment.url.img;
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      this.msgAbertura = this.loja.loj_c_horario_abertura
        ? `Abre às ${this.loja.loj_c_horario_abertura}`
        : "Não abre mais hoje";
    }, 0);
  }
  //#endregion
}
