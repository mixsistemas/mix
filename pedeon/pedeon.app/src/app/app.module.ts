//#region ng
import { registerLocaleData } from "@angular/common";
import localePt from "@angular/common/locales/pt";
import { NgModule, LOCALE_ID } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
registerLocaleData(localePt);
//#endregion

//#region ionic
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { IonicStorageModule } from "@ionic/storage";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
//#endregion

//#region app modules
import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { MixCoreModule } from "./modules/_mix/_core/mix-core.module";
import { ServicesModule } from "./modules/_services/services.module";
//#endregion

//#region app components/pages
import { AppComponent } from "./app.component";
//#endregion

//#region app modals
import { AreaLojistaModalModule } from "./modals/area-lojista/area-lojista.module";
import { LoginCadastroModalModule } from "./modals/login-cadastro/login-cadastro.module";
import { LojasFavoritasModalModule } from "./modals/lojas-favoritas/lojas-favoritas.module";
import { ProdutosFavoritosModalModule } from "./modals/produtos-favoritos/produtos-favoritos.module";
import { SeusEnderecosModalModule } from "./modals/seus-enderecos/seus-enderecos.module";
import { SuaContaModalModule } from "./modals/sua-conta/sua-conta.module";
//#endregion

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    AppRoutingModule,
    AreaLojistaModalModule,
    BrowserModule,
    ComponentsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    LoginCadastroModalModule,
    LojasFavoritasModalModule,
    MixCoreModule,
    ProdutosFavoritosModalModule,
    ServicesModule,
    SeusEnderecosModalModule,
    SuaContaModalModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: "pt-Br" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
