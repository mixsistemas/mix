//#region ng
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region routes
const ROUTES: Routes = [
  {
    path: "",
    redirectTo: "splash",
    pathMatch: "full"
  },
  {
    path: "splash",
    loadChildren: "./pages/splash/splash.module#SplashPageModule"
  },
  {
    path: "sel-local",
    loadChildren: "./pages/sel-local/sel-local.module#SelLocalPageModule"
  },
  {
    path: "sel-loja",
    loadChildren: "./pages/sel-loja/sel-loja.module#SelLojaPageModule"
  },
  {
    path: "loja/:id",
    loadChildren: "./pages/loja/loja.module#LojaPageModule"
  }
];
//#endregion

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
