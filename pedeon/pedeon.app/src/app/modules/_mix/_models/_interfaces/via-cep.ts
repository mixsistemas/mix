export interface IViaCep {
  key?: string;
  id?: number;
  cep?: string; // "01001-000",
  logradouro?: string; // "Praça da Sé",
  complemento?: string; // "lado ímpar",
  bairro?: string; // "Sé",
  localidade?: string; // "São Paulo",
  uf?: string; // "SP",
  unidade?: string; // "",
  ibge?: number; // "3550308",
  gia?: number; // "1004"
  erro?: boolean;
  nro?: number;
  referencia?: string;
}
