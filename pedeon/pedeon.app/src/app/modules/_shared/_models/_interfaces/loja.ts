export interface ILoja {
  loj_pk: number;
  loj_fk_tipo: number;
  loj_c_horario_abertura?: string;
  loj_c_bairro: string;
  loj_c_cep: string;
  loj_c_complemento: string;
  loj_c_localidade: string;
  loj_c_logradouro: string;
  loj_c_loja: string;
  loj_c_tipo?: string;
  loj_c_uf: string;
  loj_b_favorita?: boolean;
  loj_b_fechada?: boolean;
  loj_b_nova?: boolean;
  loj_f_media_avaliacoes: number;
  loj_f_media_despacho: number;
  loj_i_nro: number;
  taxa_entrega?: number;
}
