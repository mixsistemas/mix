export interface IItemDetalhesProdutos {
  idp_pk?: number;
  idp_fk_detalhe?: number;
  idp_b_ativo: boolean;
  idp_c_item: string;
  idp_f_valor: number;
  qtde?: number;
}
