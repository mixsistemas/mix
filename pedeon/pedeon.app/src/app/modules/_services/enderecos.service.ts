//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region app models
import { IEndereco, IViaCep } from "../_mix/_models/_interfaces";
import { environment } from "../../../environments/environment";
//#endregion

@Injectable({
  providedIn: "root"
})
export class EnderecosService {
  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IEndereco {
    row = row || {};
    let R: IEndereco = row;
    R.end_pk = parseInt(row.end_pk) || 0;
    R.end_b_favorito = row.end_b_favorito > 0;
    R.end_i_nro = parseInt(row.end_i_nro) || 0;
    return R;
  }

  fixes(rows: IEndereco[]): IEndereco[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion

  //#region functions
  end2local(endereco: IEndereco) {
    if (endereco) {
      return {
        id: endereco.end_pk,
        cep: endereco.end_c_cep,
        logradouro: endereco.end_c_logradouro,
        complemento: endereco.end_c_complemento,
        bairro: endereco.end_c_bairro,
        localidade: endereco.end_c_localidade,
        uf: endereco.end_c_uf,
        nro: endereco.end_i_nro,
        referencia: endereco.end_c_referencia
      };
    } else {
      return null;
    } // else
  }
  //#endregion

  //#region methods C
  novo(endereco: IViaCep, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/enderecos/novo/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, endereco);
  }
  //#endregion

  //#region methods R
  enderecos(token: string, filter: string = "*") {
    filter = filter.trim() || "*";
    const URL = encodeURI(
      `${environment.url.api.remote}/enderecos/token/${token}/filter/${filter}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  favorito(id: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/enderecos/favorito/${id}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods D
  del(id: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/enderecos/${id}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
