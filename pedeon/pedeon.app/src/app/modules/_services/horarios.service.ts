//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
// import { ICategoriaLoja } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class HorariosService {
  //#region publics
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region methods C
  novo(
    idLoja: number,
    diaSemana: number,
    inicio: number,
    final: number,
    token: string
  ) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/horarios/novo/loja/${idLoja}/dia/${diaSemana}/inicio/${inicio}/final/${final}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods R
  horarios(idLoja: number): Observable<any> {
    const NOW = new Date();
    const DW = NOW.getDay();
    // http://localhost/ws/pedeon/v1/index.php/horarios/loja/1/dw/4
    const URL = encodeURI(
      `${environment.url.api.remote}/horarios/loja/${idLoja}/dw/${DW}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods D
  del(id: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/horarios/${id}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
