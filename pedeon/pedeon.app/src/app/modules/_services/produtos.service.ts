//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IProduto } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class ProdutosService {
  //#region publics
  // categoriasProdutos: IProduto[];
  // categoriasProdutosFiltered: IProduto[];
  url_imgs = environment.url.img;
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IProduto {
    row = row || {};
    let R: IProduto = row;
    R.pro_pk = parseInt(row.pro_pk) || 0;
    R.pro_fk_grupo = parseInt(row.pro_fk_grupo) || 0;
    // R.pro_fk_loja = parseInt(row.pro_fk_loja) || 0;
    R.pro_b_ativo = row.pro_b_ativo > 0;
    R.pro_b_favorito = row.pro_b_favorito > 0;
    R.pro_f_preco = parseFloat(row.pro_f_preco) || 0.0;
    R.pro_f_preco_prom = parseFloat(row.pro_f_preco_prom) || 0.0;
    R.menor_preco = parseFloat(row.menor_preco) || 0.0;
    R.num_detalhes = parseInt(row.num_detalhes) || 0;
    return R;
  }

  fixes(rows: IProduto[]): IProduto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }

  img(img: string, big: boolean = false): string {
    let pos = "";
    let size = big ? "big/" : "";

    if (img) {
      if (img.startsWith("__")) {
        pos = "__/";
      } else if (img.startsWith("_")) {
        pos = "_/";
      } // if

      // console.log(pos, size);
      return `${this.url_imgs}/produtos/${pos}${size}/${img}.jpg`;
    } else {
      return "";
    } // else
  }
  //#endregion

  //#region methods C
  novo(produto: IProduto, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/produtos/novo/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, produto);
  }

  favAdd(idProduto: number, jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/produtos/favs/add/${idProduto}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods R
  produtosGrupo(
    idGrupo: number,
    filter: string = "*",
    favsArmazenados: string = "[]",
    jwt: string = ""
  ): Observable<any> {
    filter = filter.trim() || "*";
    // console.log(idGrupo, filter);
    favsArmazenados = favsArmazenados || "[]";
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/produtos/grupo/${idGrupo}/${filter}/${favsArmazenados}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  produto(
    idProduto: number,
    favsArmazenados: string = "[]",
    jwt: string = ""
  ): Observable<any> {
    favsArmazenados = favsArmazenados || "[]";
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/produtos/id/${idProduto}/${favsArmazenados}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  produtosJwt(
    idLoja: number,
    jwt: string,
    filter: string = "*"
  ): Observable<any> {
    filter = filter.trim() || "*";
    // console.log(idLoja, filter);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/produtos/loja/${idLoja}/filter/${filter}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  favsArmazenados(favs: string = "[]"): Observable<any> {
    favs = favs || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/produtos/favs/armazenados/${favs}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  favsConta(jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/produtos/favs/conta/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  update(produto: IProduto, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/produtos/update/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, produto);
  }

  toggleFav(id: number): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/produtos/toggle-fav/id/${id}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods D
  favDel(idProduto: number, jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/produtos/favs/del/${idProduto}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
