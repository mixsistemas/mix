//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ISetor } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class BairrosService {
  //#region publics
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region methods R
  bairrosSetor(
    idSetor: number,
    token: string,
    filter: string = "*"
  ): Observable<any> {
    filter = filter.trim() || "*";
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/bairros/setor/${idSetor}/filter/${filter}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
