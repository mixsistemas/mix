//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
//#endregion

//#region app modules
import { LocalGuard } from ".";
//#endregion

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [LocalGuard]
})
export class GuardsModule {}
