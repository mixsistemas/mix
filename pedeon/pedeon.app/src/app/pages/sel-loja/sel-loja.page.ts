//#region ng
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import {
  ActionSheetController,
  IonInfiniteScroll,
  LoadingController,
  MenuController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
type TOrderByLoja = "ava" | "tax" | "tem";
import {
  IAuthJwt,
  ICategoriaLojas,
  ILoja
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse, IViaCep } from "../../modules/_mix/_models/_interfaces";
/* import {
  KEY_AUTH_JWT,
  KEY_FAV_LOJAS,
  KEY_LOCAL
} from "../../modules/_shared/_models/consts"; */
//#endregion

//#region app services
import {
  AuthService,
  CategoriasLojasService,
  LojasService,
  StorageService
} from "../../modules/_services";
//#endregion

//#region app modals
import {
  ProcuraGeralModalPage,
  SeuPedidoModalPage
} from "../../modals";
//#endregion

@Component({
  selector: "po-sel-loja",
  templateUrl: "./sel-loja.page.html",
  styleUrls: ["./sel-loja.page.scss"]
})
export class SelLojaPage implements OnInit, OnDestroy {
  //#region comm
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  //#endregion

  //#region publics
  buff: string = "";
  categoriasLojas: ICategoriaLojas[];
  categoriasOn: boolean;
  conta: IAuthJwt;
  jwt: string;
  local: IViaCep;
  lojas: {
    total: number;
    page: number;
    all: ILoja[];
    rows: {
      abertas: {
        favoritas: ILoja[];
        novas: ILoja[];
        outras: ILoja[];
      };
      fechadas: {
        favoritas: ILoja[];
        novas: ILoja[];
        outras: ILoja[];
      };
    };
  };
  orderBy: {
    tag: TOrderByLoja;
    caption: string;
  };
  scroll = {
    pos: 0,
    prior: 0,
    up: 0,
    down: 0
  };
  scrollEvents: boolean = true;
  slideOpts = {
    slidesPerView: "2.5",
    breakpoints: {
      768: {
        slidesPerView: 2.5
      },
      992: {
        slidesPerView: 3.5
      },
      1200: {
        slidesPerView: 4.5
      }
    }
  };
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region catSel
  private _catSel: ICategoriaLojas;
  get catSel(): ICategoriaLojas {
    return this._catSel;
  }
  set catSel(val: ICategoriaLojas) {
    // console.log(val);
    this._catSel = val;
    this._resetLojas();
    this._buscaLojas(val.ctl_pk);
  }
  //#endregion

  //#region contructor
  constructor(
    private _actionCtrl: ActionSheetController,
    private _authServ: AuthService,
    private _categoriasLojasServ: CategoriasLojasService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    private _menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private _router: Router,
    private _storageServ: StorageService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("SelLojaPage.ngOnInit");
    this.orderBy = {
      tag: "ava",
      caption: "Melhor avaliada"
    };
    this.categoriasOn = true;
    this._menuCtrl.enable(false);

    this._subs.push(
      this._storageServ.onAuthChanged.subscribe((payload: IAuthJwt) => {
        // console.log(payload);
        this.conta = payload;
      })
    );

    this._subs.push(
      this._storageServ.onLojFavChangedEvent.subscribe((val: boolean) => {
        this._init();
      })
    );
    // this._init();
  }

  ngOnDestroy(): void {
    // console.log("SelLojaPage.ngOnDestroy");
    this._menuCtrl.enable(true);
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _init() {
    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.jwt = token;
      this._resetLojas();
      this._buscaCategorias();
      this._verificaConta();
    });
  }

  private async _buscaCategorias() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this.categoriasLojas = null;
      this._categoriasLojasServ.categorias().subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          if (api.ok) {
            this.categoriasLojas = api.data;
            loading.dismiss();
            if (this.categoriasLojas.length > 0) {
              this.catSel = this.categoriasLojas[0];
            } // if
          } // if
        },
        err => {
          console.error(err);
          loading.dismiss();
        }
      );
    });
  }

  private async _verificaConta() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.conta = null;
      if (token) {
        loading.present().then(() => {
          this._authServ.checkToken(token).subscribe(
            (api: any) => {
              // console.log(api);
              loading.dismiss();
              if (api) {
                this.conta = api;
              } // if
              this._storageServ.onAuthChanged.next(this.conta);
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    });
  }

  private _resetLojas() {
    // console.log("resetLojas");
    if (this.infiniteScroll) {
      this.infiniteScroll.disabled = false;
    } // if
    this.lojas = {
      total: 0,
      page: 1,
      all: null,
      rows: {
        abertas: {
          favoritas: [],
          novas: [],
          outras: []
        },
        fechadas: {
          favoritas: [],
          novas: [],
          outras: []
        }
      }
    };
  }

  private async _buscaLojas(idCategoria: number, infiniteScroll?) {
    this.local = null;
    // let loading = await this.loadingCtrl.create({
    //   // message: "Aguarde...",
    //   translucent: true
    // });
    // loading.present().then(() => {

    this._storageServ.localGet((local: string) => {
      // console.log(local);
      if (local) {
        this._storageServ.lojFavGet((favs: string) => {
          // console.log(favs);
          this.local = JSON.parse(local);
          this._storageServ.onLocalChangedEvent.next(this.local);
          const { localidade: LOCALIDADE, uf: UF, bairro: BAIRRO } = this.local;

          // console.log(idCategoria, LOCALIDADE, UF, BAIRRO, this.lojas.page);
          this._lojasServ
            .lojas(
              idCategoria,
              LOCALIDADE,
              UF,
              BAIRRO,
              this.lojas.page,
              12,
              favs,
              this.jwt
            )
            .subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                // loading.dismiss();
                if (api.ok) {
                  this.lojas.total = api.data.total;

                  this.lojas.rows.abertas.favoritas = [
                    ...this.lojas.rows.abertas.favoritas,
                    ...this._lojasServ.fixes(api.data.abertas.favoritas)
                  ];

                  this.lojas.rows.abertas.novas = [
                    ...this.lojas.rows.abertas.novas,
                    ...this._lojasServ.fixes(api.data.abertas.novas)
                  ];

                  this.lojas.rows.abertas.outras = [
                    ...this.lojas.rows.abertas.outras,
                    ...this._lojasServ.fixes(api.data.abertas.outras)
                  ];

                  this.lojas.rows.fechadas.favoritas = [
                    ...this.lojas.rows.fechadas.favoritas,
                    ...this._lojasServ.fixes(api.data.fechadas.favoritas)
                  ];

                  this.lojas.rows.fechadas.novas = [
                    ...this.lojas.rows.fechadas.novas,
                    ...this._lojasServ.fixes(api.data.fechadas.novas)
                  ];

                  this.lojas.rows.fechadas.outras = [
                    ...this.lojas.rows.fechadas.outras,
                    ...this._lojasServ.fixes(api.data.fechadas.outras)
                  ];

                  this.lojas.page++;
                  this._mergeLojas();
                  if (this.lojas.all) {
                    console.log(`${this.lojas.all.length}/${api.data.total}`);
                  } // if
                  if (infiniteScroll) {
                    infiniteScroll.target.complete();
                    if (this.lojas.all) {
                      // console.log(this.lojas.all.length);
                      if (this.lojas.all.length >= api.data.total) {
                        infiniteScroll.target.disabled = true;
                      } // if
                    } // if
                  } // if
                } // if
              },
              err => {
                console.error(err);
                // loading.dismiss();
              }
            );
        });
      } else {
        this.local = null;
        this._storageServ.onLocalChangedEvent.next(this.local);
        this._router.navigate(["/sel-local"]);
      } // else
    });
    //});
  }

  private _mergeLojas() {
    /* console.log("_mergeLojas");
    console.log(this.lojas.rows);
    console.log(this.lojas.rows.abertas.favoritas);
    console.log(this.lojas.rows.abertas.novas);
    console.log(this.lojas.rows.abertas.outras);
    console.log(this.lojas.rows.fechadas.favoritas);
    console.log(this.lojas.rows.fechadas.novas);
    console.log(this.lojas.rows.fechadas.outras); */
    this.lojas.all = [
      ...this.lojas.rows.abertas.favoritas,
      ...this.lojas.rows.abertas.novas,
      ...this.lojas.rows.abertas.outras,
      ...this.lojas.rows.fechadas.favoritas,
      ...this.lojas.rows.fechadas.novas,
      ...this.lojas.rows.fechadas.outras
    ];
    // console.log(this.lojas.all);
  }
  //#endregion

  //#region methods
  onSelCatClick(cat: ICategoriaLojas) {
    // console.log(cat);
    this.catSel = cat;
  }

  async onOrderByClick() {
    const actionSheet = await this._actionCtrl.create({
      header: "Ordenar por",
      buttons: [
        {
          text: "Menor tempo",
          // role: "destructive",
          icon: "/assets/icon/time.svg",
          handler: () => {
            console.log("Menor tempo");
            this.orderBy = {
              tag: "tem",
              caption: "Menor tempo"
            };
            this._mergeLojas();
          }
        },
        {
          text: "Menor taxa",
          icon: "/assets/icon/money.svg",
          handler: () => {
            console.log("Menor taxa");
            this.orderBy = {
              tag: "tax",
              caption: "Menor taxa"
            };
            this._mergeLojas();
          }
        },
        {
          text: "Melhor avaliada",
          icon: "/assets/icon/star.svg",
          handler: () => {
            console.log("Melhor avaliada");
            this.orderBy = {
              tag: "ava",
              caption: "Melhor avaliada"
            };
            this._mergeLojas();
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            // console.log("Cancel clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }

  onLoadData(e) {
    // console.log(e);
    this._buscaLojas(this._catSel.ctl_pk, e);
  }

  async onProcuraSubmit() {
    // console.log("onProcuraSubmit()");
    if (this.buff && this.buff.trim()) {
      const modal = await this.modalCtrl.create({
        component: ProcuraGeralModalPage,
        componentProps: { filter: this.buff }
      });

      modal.onDidDismiss().then(() => {
        this.buff = "";
        // console.log("Returned from modal", data);
        // const user = data["data"]; // Here's your selected user!
      });

      return await modal.present();
    } // if
  }

  async onCarrinhoClick() {
    // console.log("onCarrinhoClick");
    const modal = await this.modalCtrl.create({
      component: SeuPedidoModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  /* async onLoginCadastroClick(tab: LoginCadastroTab) {
    // console.log("onLoginCadastroClick", tab);
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: tab }
    });

    modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    });

    return await modal.present();
  } */
  //#endregion

  //#region scroll methods
  onScroll(e) {
    this.scroll.pos = e.detail.scrollTop;
    const UP: boolean = this.scroll.pos > this.scroll.prior;
    if (this.scroll.prior > 0) {
      if (!UP) {
        this.scroll.up += this.scroll.pos;
        this.scroll.down = 0;
      } else {
        this.scroll.down += this.scroll.pos;
        this.scroll.up = 0;
      } // else
    } // if
    this.scroll.prior = this.scroll.pos;
    // console.log(this.scroll.pos, UP);
  }

  onScrollEnd(e) {
    this.scrollEvents = false;
    // if (this.scroll.up) this.categoriasOn = true;
    // if (this.scroll.down) this.categoriasOn = true;
    // console.log(this.categoriasOn);
    if (this.scroll.up) {
      // console.log("scroll UP");
      this.categoriasOn = true;
    } else {
      // console.log("scroll DOWN");
      this.categoriasOn = false;
    } // else
    // this.categoriasOn = this.scroll.pos < 10;
    setTimeout(() => {
      this.scrollEvents = true;
    }, 100);
  }
  //#endregion
}
