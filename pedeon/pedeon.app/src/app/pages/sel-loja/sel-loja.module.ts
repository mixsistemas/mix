//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app components
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app modals
import { ProcuraGeralModalModule } from "../../modals/procura-geral/procura-geral.module";
import { SeuPedidoModalModule } from "../../modals/seu-pedido/seu-pedido.module";
//#endregion

//#region app pages
import { SelLojaPage } from "./sel-loja.page";
//#endregion

//#region routes
const ROUTES: Routes = [
  {
    path: "",
    component: SelLojaPage
  }
];
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    ProcuraGeralModalModule,
    RouterModule.forChild(ROUTES),
    SeuPedidoModalModule
  ],
  declarations: [SelLojaPage]
})
export class SelLojaPageModule {}
