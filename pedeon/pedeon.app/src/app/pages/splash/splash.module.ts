//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app pages
import { SplashPage } from "..";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: SplashPage
      }
    ])
  ],
  declarations: [SplashPage]
})
export class SplashPageModule {}
