//#region ng
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
// import { SwiperOptions } from "swiper/swiper-interface";
//#endregion

//#region 3rd
import { timer } from "rxjs";
//#endregion

//#region app services
import { StorageService } from "../../modules/_services";
//#endregion

@Component({
  selector: "app-splash",
  templateUrl: "splash.page.html",
  styleUrls: ["splash.page.scss", "./slides.scss"]
})
export class SplashPage implements OnInit {
  //#region comm
  @ViewChild("slides") slider: any;
  //#endregion

  //#region publics
  splash: boolean = true;
  atBeginning: boolean;
  atEnd: boolean;
  slideOpts = {
    pagination: {
      el: ".swiper-pagination",
      type: "bullets"
    }
  };

  //#endregion

  //#region constructor
  constructor(
    // private _platform: Platform,
    private _router: Router,
    private _storageServ: StorageService
  ) {
    this.atBeginning = false;
    this.atEnd = false;
    this._storageServ.localRemove(() => {


      /* this._storageServ.enderecosGet((enderecos: string) => {
        // console.log(enderecos);
        const ENDERECOS = JSON.parse(enderecos) || [];
        // console.log(val);
        this.splash = ENDERECOS.length > 0;
        // console.log(this.splash);
        if (this.splash) {
          timer(3800).subscribe(() =>
            this._router.navigate(["/sel-local"], { replaceUrl: true })
          );
        } else {
          this.onSlideChanged(null);
        } // else
      }); */

      this._storageServ.splashGet((splash: boolean) => {
        this.splash = splash;
        if (this.splash) {
          timer(3800).subscribe(() =>
            this._router.navigate(["/sel-local"], { replaceUrl: true })
          );
        } else {
          this.onSlideChanged(null);
        } // else
      });


    }); 
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // Reset session
    this._storageServ.authJwtRemove();
    this._storageServ.localSet(null);
    // this.onSlideChanged(null);
  }
  //#endregion

  //#region methods
  onSlideChanged(e: any) {
    // e.then(v => console.log(v));
    // console.log(e);
    if (this.slider) {
      this.slider.isBeginning().then(v => (this.atBeginning = v));
      this.slider.isEnd().then(v => (this.atEnd = v));
    } // if
  }

  onConcluirClick() {
    this._storageServ.splashSet(true, () => {
      this._router.navigate(["/sel-local"], { replaceUrl: true });
    });
  }

  onNextClick() {
    this.slider.slideNext().then(v => console.log(v));
  }

  onPrevClick() {
    this.slider.slidePrev().then(v => console.log(v));
  }
  //#endregion
}
