//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region 3rd
import { BrMaskerModule } from "br-mask";
//#endregion

//#region app components
import { ComponentsModule } from "../../components/components.module";
//#endregion

//#region app pages
import { SelLocalCepComponent, SelLocalPage } from ".";
//#endregion

//#region app modals
import { LocalFormModalModule } from "../../modals/local-form/local-form.module";
import { LoginCadastroModalModule } from "../../modals/login-cadastro/login-cadastro.module";
import { NaoSeiCepModalModule } from "../../modals/nao-sei-cep/nao-sei-cep.module";
import { SeuPedidoModalModule } from "../../modals/seu-pedido/seu-pedido.module";
//#endregion

//#region routes
const ROUTES: Routes = [
  {
    path: "",
    component: SelLocalPage
  }
];
//#endregion

@NgModule({
  imports: [
    BrMaskerModule,
    LocalFormModalModule,
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    LoginCadastroModalModule,
    NaoSeiCepModalModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
    SeuPedidoModalModule
  ],
  declarations: [SelLocalPage, SelLocalCepComponent]
})
export class SelLocalPageModule {}
