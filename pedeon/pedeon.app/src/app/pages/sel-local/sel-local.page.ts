//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import {
  LoadingController,
  MenuController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { SelLocalTab } from "../../modules/_shared/_models/_enums";
import { IAuthJwt } from "../../modules/_shared/_models/_interfaces";
import { IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import { AuthService, StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import { SeuPedidoModalPage } from "../../modals";
//#endregion

@Component({
  selector: "app-sel-local",
  templateUrl: "./sel-local.page.html",
  styleUrls: ["./sel-local.page.scss"]
})
export class SelLocalPage implements OnInit, OnDestroy {
  //#region publics
  Tab = SelLocalTab;
  tab: SelLocalTab;
  conta: IAuthJwt;
  local: IViaCep;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _loadingCtrl: LoadingController,
    private _menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private _storageServ: StorageService,
    private _router: Router
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("SelLocalPage.ngOnInit");
    this._menuCtrl.enable(true);
    this._storageServ.localSet(null);

    this._subs.push(
      this._storageServ.onAuthChanged.subscribe((payload: IAuthJwt) => {
        // console.log(payload);
        this.conta = payload;
      })
    );

    // this._subs.push(
    /* let sub: Subscription = this._storageServ.onLocalChangedEvent.subscribe(
      (local: IViaCep) => {
        console.log(local);
        // this.local = local;
        if (local) {
          if (sub) {
            sub.unsubscribe();
          } // if
          this._router.navigate(["/sel-loja"]);
        } // if
      }
    ); */
    // );

    // console.log('sel-local.page.ngOnInit()');
    this.tab = SelLocalTab.cep;
    this._verificaConta();
    this._buscaLocal();
  }

  ngOnDestroy(): void {
    // console.log("SelLocalPage.ngOnDestroy");
    this._menuCtrl.enable(false);
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private async _verificaConta() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.conta = null;
      if (token) {
        loading.present().then(() => {
          this._authServ.checkToken(token).subscribe(
            (api: any) => {
              // console.log(api);
              loading.dismiss();
              if (api) {
                this.conta = api;
              } // if
              this._storageServ.onAuthChanged.next(this.conta);
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    });
  }

  private async _buscaLocal() {
    this.local = null;
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    loading.present().then(() => {
      this._storageServ.localGet((local: string) => {
        // console.log(local);
        loading.dismiss();
        this.local = local ? JSON.parse(local) : null;
        // console.log(this.local);
        this._storageServ.onLocalChangedEvent.next(this.local);
      });
    });
  }

  onSelClick(e: IViaCep) {
    if (e) {
      this._router.navigate(["/sel-loja"]);
    }
  }
  //#endregion

  async onCarrinhoClick() {
    // console.log("onCarrinhoClick");
    const modal = await this.modalCtrl.create({
      component: SeuPedidoModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }
}
