//#region ng
import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
//#endregion

//#region ionic
import { LoadingController, ModalController } from "@ionic/angular";
//#endregion

//#region 3rd
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { LoginCadastroTab } from "../../modules/_shared/_models/_enums";
import {
  IModalResponse,
  IViaCep
} from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import {
  CepService,
  IonicService,
  UtilsService
} from "../../modules/_mix/_core/_services";
import { LojasService, StorageService } from "../../modules/_services";
//#endregion

//#region app modals
import {
  LocalFormModalPage,
  LoginCadastroModalPage,
  NaoSeiCepModalPage
} from "../../modals";
//#endregion

@Component({
  selector: "po-sel-local-cep",
  templateUrl: "local-cep.component.html",
  styleUrls: ["local-cep.component.scss"]
})
export class SelLocalCepComponent {
  //#region publics
  cepForm: FormGroup;
  Tab = LoginCadastroTab;
  geo: any;
  //#endregion

  //#region constructor
  constructor(
    private _cepServ: CepService,
    public formBuilder: FormBuilder,
    // private geolocation: Geolocation,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    public modalCtrl: ModalController,
    private _router: Router,
    private _storageServ: StorageService,
    private _utilsServ: UtilsService
  ) {
    this.cepForm = formBuilder.group({
      cep: [
        "",
        Validators.compose([
          Validators.required
          // Validators.pattern("^\\d{5}[-]\\d{3}$")
        ])
      ]
    });
  }
  //#endregion

  //#region methods
  async onSubmit() {
    const CEP = this.cepForm.value.cep;
    // this._router.navigate(["/sel-loja"]);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._cepServ
        .cep(CEP)
        .pipe(
          mergeMap((cep: IViaCep) => {
            return this._lojasServ.numLojas(cep.localidade, cep.uf).pipe(
              map((numLojas: any) => {
                return {
                  cep: cep,
                  numLojas: numLojas
                };
              })
            );
          })
        )
        .subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.numLojas.ok) {
              if (api.numLojas.data > 0) {
                this.onLocalFormClick(api.cep);
              } else {
                this._ionicServ.alert(
                  "Localidade não atendida.",
                  `${api.cep.localidade} ${api.cep.uf}`
                );
              } // else
            } else {
              this._ionicServ.alert(
                "CEP não encontrado",
                this.cepForm.value.cep
              );
              this.cepForm.reset();
            } // else
          },
          err => {
            this._ionicServ.alert(
              "CEP inválido ou não encontrado",
              this.cepForm.value.cep
            );
            this.cepForm.reset();
            console.error(err);
            loading.dismiss();
          }
        );
    });
  }

  async onLocalFormClick(cepInfo: IViaCep) {
    const modal = await this.modalCtrl.create({
      component: LocalFormModalPage,
      componentProps: { cepInfo: cepInfo }
    });

    modal.onDidDismiss().then((data: IModalResponse) => {
      // console.log(data);
      if (data.data) {
        let local: IViaCep = data.data;
        local.key = this._utilsServ.guid();
        // console.log(local);
        this._storageServ.localSet(local, () => {
          this.cepForm.reset();
          this._storageServ.localAdd(local);
          this._router.navigate(["/sel-loja"]);
        });
      } // if
    });

    return await modal.present();
  }

  async onLoginCadastroClick(tab: LoginCadastroTab) {
    // console.log("onLoginCadastroClick", tab);
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: tab }
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  async onNaoSeiCepClick() {
    // console.log("onLoginCadastroClick", tab);

    const modal = await this.modalCtrl.create({
      component: NaoSeiCepModalPage
      // componentProps: { tab: tab }
    });

    // modal.onDidDismiss().then(() => {});

    return await modal.present();
  }

  onUsarLocalizacaoClick() {
    /* this.geolocation
      .getCurrentPosition()
      .then(resp => {
        this.geo = resp;
        // resp.coords.latitude
        // resp.coords.longitude
      })
      .catch(error => {
        console.error("Erro buscando localização", error);
      }); */
    /* let watch = this.geolocation.watchPosition();
    watch.subscribe(data => {
      console.log(data);
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    }); */
  }
  //#region methods
}
