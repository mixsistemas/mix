//#region ng
import {
  Component,
  AfterViewInit,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region ionic
import {
  LoadingController,
  MenuController,
  ModalController
} from "@ionic/angular";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
type TExibicaoProdutos = "lis" | "sli";
import { LojaInfoTab } from "../../modules/_shared/_models/_enums";
import { environment } from "../../../environments/environment";
import {
  IAuthJwt,
  IGrupoProdutos,
  ILoja,
  IProduto
} from "../../modules/_shared/_models/_interfaces";
import { IApiResponse, IViaCep } from "../../modules/_mix/_models/_interfaces";
//#endregion

//#region app services
import {
  AuthService,
  GruposProdutosService,
  HorariosService,
  LojasService,
  ProdutosService,
  StorageService
} from "../../modules/_services";
import { IonicService } from "../../modules/_mix/_core/_services";
//#endregion

//#region app modals
import {
  LojaInfoModalPage,
  ProdutoOkModalPage,
  SeuPedidoModalPage,
  ZoomImgModalPage
} from "../../modals";
//#endregion

@Component({
  selector: "po-loja",
  templateUrl: "./loja.page.html",
  styleUrls: ["./loja.page.scss"]
})
export class LojaPage implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild("slides") slider: any;
  @HostListener("window:resize") onHostResize() {
    this.onResize();
  }
  //#endregion

  //#region publics
  conta: IAuthJwt;
  idLoja: number;
  jwt: string;
  footerOn: boolean = true;
  searchOn: boolean;
  grupos: IGrupoProdutos[];
  gruSel: IGrupoProdutos;
  devWidth: number;
  exibicao: TExibicaoProdutos = "lis";
  filter: string = "";
  intervalos: any;
  lisSlideOpts = {
    slidesPerView: 3.5,
    breakpoints: {
      540: {
        slidesPerView: 1.8
      },
      768: {
        slidesPerView: 2.5
      },
      992: {
        slidesPerView: 3.5
      }
    },
    centeredSlides: true,
    spaceBetween: 0,
    centerInsufficientSlides: true,
    grabCursor: false,
    loop: false,
    height: 48,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  };
  local: IViaCep;
  loja: ILoja;
  produtos: IProduto[];
  scroll = {
    pos: 0,
    prior: 0,
    up: 0,
    down: 0
  };
  scrollEvents: boolean = true;
  sliSlideOpts = {
    slidesPerView: 4.5,
    breakpoints: {
      540: {
        slidesPerView: 1.5
      },
      768: {
        slidesPerView: 2.5
      },
      992: {
        slidesPerView: 3.5
      },
      1200: {
        slidesPerView: 4.5
      }
    },
    centeredSlides: true,
    spaceBetween: 0,
    centerInsufficientSlides: true,
    grabCursor: true,
    loop: false,
    height: 48,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  };
  Tab = LojaInfoTab;
  url_imgs = environment.url.img;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  /* //#region idLoja
  private _idLoja: number;
  get idLoja(): number {
    return this._idLoja;
  }
  set idLoja(val: number) {
    this._idLoja = val;
    this._refresh();
  }
  //#endregion */

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _gruposServ: GruposProdutosService,
    private _horariosServ: HorariosService,
    private _ionicServ: IonicService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
    private _menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private _produtosServ: ProdutosService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _storageServ: StorageService
  ) {
    this.idLoja = this._route.snapshot.params["id"];
    /* if (this._platform.ready) {
      this.devWidth = this._platform.width();
    } // if */
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("LojaPage.ngOnInit");
    this._menuCtrl.enable(false);
    this._subs.push(
      this._storageServ.onAuthChanged.subscribe((payload: IAuthJwt) => {
        // console.log(payload);
        this.conta = payload;
      })
    );

    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.jwt = token;
      this.filter = "";
      setTimeout(() => {
        this._refresh();
        this.onBuscaGrupos();
      }, 0);
      this.searchOn = true;
    });
    this._verificaConta();
  }

  ngAfterViewInit() {
    // console.log("LojaPage.ngAfterViewInit");
    this.onResize();
  }

  ngOnDestroy(): void {
    // console.log("LojaPage.ngOnDestroy");
    this._menuCtrl.enable(false);
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  /* private async _buscaLoja() {
    this.loja = null;
    if (this.idLoja > 0 && this.local) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._storageServ.lojFavGet((favs: string) => {
          // console.log(favs);
          this._lojasServ
            .loja(this.idLoja, this.local.bairro, favs, this.jwt)
            .pipe(
              mergeMap((loja: IApiResponse) => {
                return this._horariosServ.horarios(loja.data.loj_pk).pipe(
                  map((intervalos: IApiResponse) => {
                    return {
                      loja: loja,
                      intervalos: intervalos
                    };
                  })
                );
              })
            )
            .subscribe(
              (api: any) => {
                // console.log(api);
                if (api.intervalos.ok) {
                  this.loja = this._lojasServ.fix(api.loja.data);
                  this.intervalos = api.intervalos.data;
                  loading.dismiss();
                } // if
              },
              err => {
                console.error(err);
                loading.dismiss();
              }
            );
        });
      });
    } // if
  } */

  private async _refresh() {
    // console.log(this.idLoja);
    this.local = null;
    this.loja = null;
    if (this.idLoja > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._storageServ.localGet((local: string) => {
          // console.log(local);
          // loading.dismiss();
          if (local) {
            this.local = JSON.parse(local);
            this._storageServ.onLocalChangedEvent.next(this.local);
            this._storageServ.lojFavGet((favs: string) => {
              // console.log(favs);
              // console.log(this.idLoja, this.local.bairro, favs, this.jwt);
              this._lojasServ
                .loja(this.idLoja, this.local.bairro, favs, this.jwt)
                .pipe(
                  mergeMap((loja: IApiResponse) => {
                    return this._horariosServ.horarios(loja.data.loj_pk).pipe(
                      map((intervalos: IApiResponse) => {
                        return {
                          loja: loja,
                          intervalos: intervalos
                        };
                      })
                    );
                  })
                )
                .subscribe(
                  (api: any) => {
                    // console.log(api);
                    if (api.intervalos.ok) {
                      this.loja = this._lojasServ.fix(api.loja.data);
                      this.intervalos = api.intervalos.data;
                      loading.dismiss();
                    } // if
                  },
                  err => {
                    console.error(err);
                    loading.dismiss();
                  }
                );
            });
          } else {
            this.local = null;
            this._storageServ.onLocalChangedEvent.next(this.local);
            this._router.navigate(["/sel-local"]);
          } // else
        });
      });
    } // if
  }

  private async _verificaConta() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    this._storageServ.authJwtGet((token: string) => {
      // console.log(token);
      this.conta = null;
      if (token) {
        loading.present().then(() => {
          this._authServ.checkToken(token).subscribe(
            (api: any) => {
              // console.log(api);
              loading.dismiss();
              if (api) {
                this.conta = api;
              } // if
              this._storageServ.onAuthChanged.next(this.conta);
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
      } // if
    });
  }

  pro_img(img: string): string {
    return this._produtosServ.img(img);
  }
  //#endregion

  //#region methods
  onLimpaProcura() {
    this.filter = "";
    this.onBuscaGrupos();
  }

  async onBuscaGrupos() {
    this.grupos = null;
    this.produtos = null;
    if (this.idLoja > 0) {
      let loading = await this._loadingCtrl.create({
        // message: "Aguarde...",
        translucent: true
      });
      loading.present().then(() => {
        this._gruposServ.grupos(this.idLoja, this.filter).subscribe(
          (api: any) => {
            // console.log(api);
            loading.dismiss();
            if (api.ok) {
              this.grupos = this._gruposServ.fixes(api.data);
              if (this.grupos.length > 0) {
                this.onSelCatClick(this.grupos[0]);
              } // if
            } // if
          },
          err => {
            console.error(err);
            loading.dismiss();
          }
        );
      });
    } // if
  }

  async onLojaInfoClick(tab: LojaInfoTab) {
    // console.log("onLojaInfoClick");
    const modal = await this.modalCtrl.create({
      component: LojaInfoModalPage,
      componentProps: { loja: this.loja, tab: tab }
    });

    /* modal.onDidDismiss().then(() => {
      // console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  onExibicaoToggleClick() {
    this.exibicao = this.exibicao == "lis" ? "sli" : "lis";
    /* if (this.exibicao == "sli") {
      this.slider.slideTo(0);
    } // if */
  }

  async onSelCatClick(cat: IGrupoProdutos) {
    // console.log(this.filter, cat);
    this.gruSel = cat;

    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      this._storageServ.proFavGet((favs: string) => {
        // console.log(favs);
        this.produtos = null;
        this._produtosServ
          .produtosGrupo(cat.gpr_pk, this.filter, favs, this.jwt)
          .subscribe(
            (api: IApiResponse) => {
              // console.log(api);
              if (api.ok) {
                this.produtos = this._produtosServ.fixes(api.data) || [];
                loading.dismiss();
              } // if
            },
            err => {
              console.error(err);
              loading.dismiss();
            }
          );
      });
    });

    // this.onViewportResize();
  }

  onZoomImgClick(img: string) {
    // console.log(img);
    this.modalCtrl
      .create({
        component: ZoomImgModalPage,
        componentProps: {
          img: this._produtosServ.img(img, true)
        }
      })
      .then(modal => modal.present());
  }

  onResize() {
    this.devWidth = window.innerWidth;
  }

  onToggleFavClick() {
    // console.log("onToggleFavClick()");
    if (this.loja) {
      const { loj_pk: ID_LOJA, loj_b_favorita: FAV } = this.loja;
      // console.log(ID_LOJA, !FAV, this.conta);
      if (ID_LOJA > 0) {
        if (this.jwt) {
          if (FAV) {
            this._lojasServ.favDel(ID_LOJA, this.jwt).subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                if (api.ok) {
                  this._storageServ.lojFavDel(ID_LOJA); // Apaga do armazenagem local junto da conta.
                  this._ionicServ.toast("Loja não é mais favorita.", "danger");
                  this._storageServ.onLojFavChangedEvent.next(true);
                  this._refresh();
                } // if
              },
              err => {
                console.error(err);
              }
            );
          } else {
            this._lojasServ.favAdd(ID_LOJA, this.jwt).subscribe(
              (api: IApiResponse) => {
                // console.log(api);
                if (api.ok) {
                  this._ionicServ.toast("Loja agora é favorita.");
                  this._storageServ.onLojFavChangedEvent.next(true);
                  this._refresh();
                } // if
              },
              err => {
                console.error(err);
              }
            );
          } // else
        } else {
          if (FAV) {
            this._storageServ.lojFavDel(ID_LOJA, () => {
              this._ionicServ.toast("Loja não é mais favorita.", "danger");
              this._refresh();
            });
          } else {
            this._storageServ.lojFavAdd(ID_LOJA, () => {
              this._ionicServ.toast("Loja agora é favorita.");
              this._refresh();
            });
          } // else
        } // else
      } // if
    } // if
  }

  async onProdutoSelClick(produto: IProduto) {
    // console.log(produto, this.loja);
    /* this.modalCtrl
      .create({
        component: ProdutoOkModalPage,
        componentProps: {
          idProduto: produto.pro_pk,
          loja: this.loja
        }
      })
      .then(modal => modal.present()); */

    // console.log("onLojaInfoClick");
    const modal = await this.modalCtrl.create({
      component: ProdutoOkModalPage,
      componentProps: { idProduto: produto.pro_pk, loja: this.loja }
    });

    modal.onDidDismiss().then(() => {
      // this._refresh();
      if (this.grupos.length > 0) {
        this.onSelCatClick(this.grupos[0]);
      } // if
    });

    return await modal.present();
  }

  async onCarrinhoClick() {
    // console.log("onCarrinhoClick");
    const modal = await this.modalCtrl.create({
      component: SeuPedidoModalPage,
      componentProps: {}
    });

    /* modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    }); */

    return await modal.present();
  }

  /* async onLoginCadastroClick(tab: LoginCadastroTab) {
    // console.log("onLoginCadastroClick", tab);
    const modal = await this.modalCtrl.create({
      component: LoginCadastroModalPage,
      componentProps: { tab: tab }
    });

    modal.onDidDismiss().then(() => {
      console.log("Returned from modal", data);
      // const user = data["data"]; // Here's your selected user!
    });

    return await modal.present();
  } */
  //#endregion

  //#region scroll methods
  onScroll(e) {
    this.scroll.pos = e.detail.scrollTop;
    const UP: boolean = this.scroll.pos > this.scroll.prior;
    if (this.scroll.prior > 0) {
      if (!UP) {
        this.scroll.up += this.scroll.pos;
        this.scroll.down = 0;
      } else {
        this.scroll.down += this.scroll.pos;
        this.scroll.up = 0;
      } // else
    } // if
    this.scroll.prior = this.scroll.pos;
    // console.log(this.scroll.pos, UP);
  }

  onScrollEnd(e) {
    this.scrollEvents = false;
    // if (this.scroll.up) this.searchOn = true;
    // if (this.scroll.down) this.searchOn = true;
    // console.log(this.searchOn);
    if (this.scroll.up) {
      // console.log("scroll UP");
      this.footerOn = true;
    } else {
      // console.log("scroll DOWN");
      this.footerOn = false;
    } // else
    this.searchOn = this.scroll.pos < 30;
    setTimeout(() => {
      this.scrollEvents = true;
    }, 100);
  }
  //#endregion
}
