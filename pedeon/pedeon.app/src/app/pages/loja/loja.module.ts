//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region ionic
import { IonicModule } from "@ionic/angular";
//#endregion

//#region app modules
import { ComponentsModule } from "../../components/components.module";
import { LojaInfoModalModule } from "../../modals/loja-info/loja-info.module";
import { ProdutoOkModalModule } from "../../modals/produto-ok/produto-ok.module";
import { SeuPedidoModalModule } from "../../modals/seu-pedido/seu-pedido.module";
import { ZoomImgModalModule } from "../../modals/zoom-img/zoom-img.module";
//#endregion

//#region app pages
import { LojaPage } from "./loja.page";
//#endregion

//#region routes
const ROUTES: Routes = [
  {
    path: "",
    component: LojaPage
  }
];
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    LojaInfoModalModule,
    ProdutoOkModalModule,
    RouterModule.forChild(ROUTES),
    SeuPedidoModalModule,
    ZoomImgModalModule
  ],
  declarations: [LojaPage]
})
export class LojaPageModule {}
