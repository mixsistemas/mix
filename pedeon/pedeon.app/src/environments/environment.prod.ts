export const environment = {
  production: true,
  url: {
    img: "https://www.pedeon.com.br/assets/img",
    api: {
      remote: "https://www.pedeon.com.br/ws/pedeon/v1/index.php"
    }
  }
};