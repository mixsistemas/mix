//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILogin } from "../_shared/_models/_interfaces";
//#endregion

//#region app services
import { StorageService } from "../_services/storage.service";
//#endregion

@Injectable({
  providedIn: "root"
})
export class AuthService {
  //#region constructor
  constructor(private _http: HttpClient, private _storageServ: StorageService) {
    this._storageServ.authJwtGet((token: string) => {
      this.checkToken(token);
    });
  }
  //#endregion

  //#region methods
  login(credenciais: ILogin): Observable<any> {
    const URL = encodeURI(`${environment.url.api.remote}/contas/login`);
    // console.log(`url: ${URL}`);
    return this._http.post(URL, credenciais);
  }

  logout() {
    this._storageServ.authJwtRemove();
  }

  isAuthenticated(): boolean {
    return this._storageServ.onAuthChanged.value != null;
  }

  checkToken(token: string): Observable<any> {
    // console.log(token);
    const URL =
      encodeURI(`${environment.url.api.remote}/check-token`) + "/" + token;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
