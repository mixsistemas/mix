//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { IConta } from "../_shared/_models/_interfaces";
import { environment } from "../../../environments/environment";
// import { KEY_AUTH_JWT } from "../_shared/_models/consts";
//#endregion

@Injectable({
  providedIn: "root"
})
export class ContasService {
  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IConta {
    row = row || {};
    let R: IConta = row;
    R.con_pk = parseInt(row.con_pk) || 0;
    R.con_d_nascimento = row.con_d_nascimento
      ? new Date(row.con_d_nascimento)
      : null;

    return R;
  }

  fixes(rows: IConta[]): IConta[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion

  //#region methods R
  conta(token: string): Observable<any> {
    const URL =
      encodeURI(`${environment.url.api.remote}/contas/conta/`) + token;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods C
  add(info: any, endereco: any) {
    // console.log(info, endereco);
    const URL = encodeURI(`${environment.url.api.remote}/contas/add`);
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { info: info, endereco: endereco });
  }
  //#endregion

  //#region methods U
  mudarSenha(info: any, token: string) {
    // console.log(info);
    const URL = encodeURI(`${environment.url.api.remote}/contas/mudar-senha`);
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { info: info, token: token });
  }

  update(info: any, token: string) {
    // console.log(info);
    const URL = encodeURI(`${environment.url.api.remote}/contas/update`);
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { perfil: info, token: token });
  }
  //#endregion
}
