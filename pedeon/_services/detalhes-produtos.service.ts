//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IDetalheProdutos } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class DetalhesProdutosService {
  //#region publics
  // categoriasProdutos: IDetalheProdutos[];
  // categoriasProdutosFiltered: IDetalheProdutos[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IDetalheProdutos {
    row = row || {};
    let R: IDetalheProdutos = row;
    R.det_pk = parseInt(row.det_pk) || 0;
    R.det_fk_grupo_itens = parseInt(row.det_fk_grupo_itens) || 0;
    R.det_b_ativo = row.det_b_ativo > 0;
    R.det_i_min = parseInt(row.det_i_min) || 0;
    R.det_i_max = parseInt(row.det_i_max) || 0;
    R.det_i_num_itens = parseInt(row.det_i_num_itens) || 0;
    R.det_f_valor = parseFloat(row.det_f_valor) || 0.0;
    R.expanded = false;
    R.itens = row.itens || [];
    return R;
  }

  fixes(rows: IDetalheProdutos[]): IDetalheProdutos[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region methods C
  novo(idProduto: number, detalhe: IDetalheProdutos, token: string) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/detalhes-produtos/novo/produto/${idProduto}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, detalhe);
  }
  //#endregion

  //#region methods R
  detalhes(idProduto: number): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/detalhes-produtos/produto/${idProduto}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  detalhesJwt(idProduto: number, jwt: string = ""): Observable<any> {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/detalhes-produtos/produto/${idProduto}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  update(idProduto: number, produto: IDetalheProdutos, token: string) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/detalhes-produtos/update/produto/${idProduto}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, produto);
  }

  togglePos(
    idLoja: number,
    p1: number,
    p2: number,
    jwt: string
  ): Observable<any> {
    // console.log(idLoja);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/detalhes-produtos/toggle-pos/loja/${idLoja}/${p1}/${p2}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
