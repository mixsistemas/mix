//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ILoja } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class LojasService {
  //#region publics
  // lojas: ILoja[];
  // lojasFiltered: ILoja[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ILoja {
    row = row || {};
    let R: ILoja = row;
    R.loj_pk = parseInt(row.loj_pk) || 0;
    R.loj_fk_tipo = parseInt(row.loj_fk_tipo) || 0;
    R.loj_b_favorita = row.loj_b_favorita > 0;
    R.loj_b_fechada = row.loj_b_fechada > 0;
    R.loj_b_nova = row.loj_b_nova > 0;
    R.loj_f_media_avaliacoes = parseFloat(row.loj_f_media_avaliacoes) || 0.0;
    R.loj_f_media_despacho = parseFloat(row.loj_f_media_despacho) || 0.0;
    R.taxa_entrega = parseFloat(row.taxa_entrega) || 0.0;
    R.loj_i_nro = parseInt(row.loj_i_nro) || 0;
    return R;
  }

  fixes(rows: ILoja[]): ILoja[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion

  //#region methods C
  favAdd(idLoja: number, jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/favs/add/${idLoja}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods R
  lojas(
    idCategoria: number,
    localidade: string,
    uf: string,
    bairro: string,
    page: number = 0,
    count: number = 0,
    favsArmazenados: string = "[]",
    jwt: string = ""
  ): Observable<any> {
    const NOW = new Date();
    const DW = NOW.getDay();
    const INTERVALO = NOW.getHours() * 60 + NOW.getMinutes();
    favsArmazenados = favsArmazenados || "[]";
    jwt = jwt || null;
    let url = `${
      environment.url.api.remote
    }/lojas/categoria/${idCategoria}/${localidade}/${uf}/${bairro}/${DW}/${INTERVALO}/${favsArmazenados}/token/${jwt}`;
    // page = page || 1;
    // console.log(page, count);s
    if (page > 0 && count > 0) {
      url += `?page=${page}&count=${count}`;
    } // if
    url = encodeURI(url);
    // console.log(`url: ${url}`);
    return this._http.get(url);
  }

  numLojas(localidade: string, uf: string): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/num/${localidade}/${uf}`
    );
    // console.log(`url: ${URL}`;
    return this._http.get(URL);
  }

  loja(
    id: number,
    bairro: string,
    favsArmazenados: string = "[]",
    jwt: string = ""
  ): Observable<any> {
    const NOW = new Date();
    const DW = NOW.getDay();
    const INTERVALO = NOW.getHours() * 60 + NOW.getMinutes();
    favsArmazenados = favsArmazenados || "[]";
    jwt = jwt || null;
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/lojas/id/${id}/bairro/${bairro}/${DW}/${INTERVALO}/${favsArmazenados}/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  favsArmazenadas(favs: string = "[]"): Observable<any> {
    favs = favs || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/favs/armazenadas/${favs}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  favsConta(jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/favs/conta/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  lojasConta(jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(`${environment.url.api.remote}/lojas/conta/${jwt}`);
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  // http://localhost/ws/pedeon/v1/index.php/lojas/toggle-fav/id/1
  toggleFav(id: number): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/toggle-fav/id/${id}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods D
  favDel(idLoja: number, jwt: string): Observable<any> {
    jwt = jwt || "[]";
    // console.log(favs);
    const URL = encodeURI(
      `${environment.url.api.remote}/lojas/favs/del/${idLoja}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
