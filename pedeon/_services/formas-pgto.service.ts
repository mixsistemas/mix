//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IFormaPgto } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class FormasPgtoService {
  //#region publics
  // categorias: ICategoriaLoja[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IFormaPgto {
    row = row || {};
    let R: IFormaPgto = row;
    R.fpg_pk = parseInt(row.fpg_pk) || 0;
    R.sel = row.sel > 0;
    // console.log(row.sel, R.sel);
    return R;
  }

  fixes(rows: IFormaPgto[]): IFormaPgto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion

  //#region methods R
  formasConta(idLoja: number, token: string): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/formas-pgto/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  formas(idLoja: number): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/formas-pgto/loja/${idLoja}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  toggle(status: boolean, idForma: number, idLoja: number, token: string): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/formas-pgto/toggle/status/${status ? '1' : '0'}/forma/${idForma}/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
