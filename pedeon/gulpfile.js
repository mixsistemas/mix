// vars
var gulp = require("gulp");

var src = {
  assets: "_assets/**/*.*",
  shared: "_shared/**/*.*",
  services: "_services/**/*.*",
  guards: "_guards/**/*.*"
};

var dest = {
  assets: {
    app: "./pedeon.www/src/assets/",
    www: "./pedeon.app/src/assets"
  },
  shared: {
    app: "./pedeon.www/src/app/modules/_shared",
    www: "./pedeon.app/src/app/modules/_shared"
  },
  services: {
    app: "./pedeon.www/src/app/modules/_services",
    www: "./pedeon.app/src/app/modules/_services"
  },
  guards: {
    app: "./pedeon.www/src/app/modules/_guards",
    www: "./pedeon.app/src/app/modules/_guards"
  }
};

// tasks
gulp.task("assets", function() {
  gulp
    .src(src.assets)
    .pipe(gulp.dest(dest.assets.app))
    .pipe(gulp.dest(dest.assets.www));
});

gulp.task("shared", function() {
  gulp
    .src(src.shared)
    .pipe(gulp.dest(dest.shared.app))
    .pipe(gulp.dest(dest.shared.www));
});

gulp.task("services", function() {
  gulp
    .src(src.services)
    .pipe(gulp.dest(dest.services.app))
    .pipe(gulp.dest(dest.services.www));
});

gulp.task("guards", function() {
  gulp
    .src(src.guards)
    .pipe(gulp.dest(dest.guards.app))
    .pipe(gulp.dest(dest.guards.www));
});

// watch task
gulp.task("default", function() {
  gulp.watch(src.assets, ["assets"]);
  gulp.watch(src.shared, ["shared"]);
  gulp.watch(src.services, ["services"]);
  gulp.watch(src.guards, ["guards"]);
});
