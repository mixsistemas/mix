//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
//#endregion

@Injectable({
  providedIn: "root"
})
export class ProcuraService {
  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region methods
  lojPro(filter: string): Observable<any> {
    const NOW = new Date();
    const DW = NOW.getDay();
    const INTERVALO = NOW.getHours() * 60 + NOW.getMinutes();
    // http://localhost/ws/pedeon/v1/index.php/procura/cerv
    const URL = encodeURI(`${environment.url.api.remote}/procura/${filter}/${DW}/${INTERVALO}`);
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
