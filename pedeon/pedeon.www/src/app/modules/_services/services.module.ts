//#region ng
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
//#endregion

//#region app modules
import {
  BairrosService,
  CategoriasLojasService,
  ContasService,
  DetalhesProdutosService,
  EnderecosService,
  FormasPgtoService,
  GruposProdutosService,
  HorariosService,
  ItensDetalhesProdutosService,
  LojasService,
  ProcuraService,
  ProdutosService,
  SetoresService,
  StorageService
} from ".";
//#endregion

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    BairrosService,
    CategoriasLojasService,
    ContasService,
    DetalhesProdutosService,
    EnderecosService,
    FormasPgtoService,
    GruposProdutosService,
    HorariosService,
    ItensDetalhesProdutosService,
    LojasService,
    ProcuraService,
    ProdutosService,
    SetoresService,
    StorageService
  ]
})
export class ServicesModule {}
