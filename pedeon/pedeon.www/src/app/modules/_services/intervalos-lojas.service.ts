//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
// import { ICategoriaLoja } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class IntervalosLojasService {
  //#region publics
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region methods
  intervalos(idLoja: number): Observable<any> {
    const NOW = new Date();
    const DW = NOW.getDay();
    // http://localhost/ws/pedeon/v1/index.php/intervalos-lojas/loja/1/dw/4
    const URL = encodeURI(
      `${environment.url.api.remote}/intervalos-lojas/loja/${idLoja}/dw/${DW}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
