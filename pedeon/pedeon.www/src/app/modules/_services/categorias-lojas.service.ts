//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
//#endregion

@Injectable({
  providedIn: "root"
})
export class CategoriasLojasService {
  //#region publics
  // categorias: ICategoriaLoja[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region methods
  categorias(): Observable<any> {
    const URL = encodeURI(`${environment.url.api.remote}/categorias-lojas`);
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
