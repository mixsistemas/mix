//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IGrupoProdutos } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class GruposProdutosService {
  //#region publics
  // categorias: ICategoriaLoja[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IGrupoProdutos {
    row = row || {};
    let R: IGrupoProdutos = row;
    R.gpr_pk = parseInt(row.gpr_pk) || 0;
    R.gpr_fk_loja = parseInt(row.gpr_fk_loja) || 0;
    R.gpr_b_ativo = row.gpr_b_ativo > 0;
    R.gpr_i_pos = parseInt(row.gpr_i_pos) || 0;
    R.prior = 0;
    R.next = 0;
    // R.expanded = row.expanded > 0;
    return R;
  }

  fixes(rows: IGrupoProdutos[]): IGrupoProdutos[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region methods C
  novo(idLoja: number, grupo: string, token: string) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/grupos-produtos/novo/loja/${idLoja}/${grupo}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods R
  grupos(idLoja: number, filter: string = "*"): Observable<any> {
    filter = filter.trim() || "*";
    // console.log(idLoja, filter);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/grupos-produtos/loja/${idLoja}/filter/${filter}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  gruposJwt(
    idLoja: number,
    jwt: string,
    filter: string = "*"
  ): Observable<any> {
    filter = filter.trim() || "*";
    // console.log(idLoja, filter);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/grupos-produtos/loja/${idLoja}/token/${jwt}/filter/${filter}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  togglePos(
    idLoja: number,
    p1: number,
    p2: number,
    jwt: string
  ): Observable<any> {
    // console.log(idLoja);
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/grupos-produtos/toggle-pos/loja/${idLoja}/${p1}/${p2}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  update(info: any, token: string) {
    // console.log(info);
    const URL = encodeURI(
      `${environment.url.api.remote}/grupos-produtos/update`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { info: info, jwt: token });
  }
  //#endregion

  //#region methods D
  del(id: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/grupos-produtos/${id}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
