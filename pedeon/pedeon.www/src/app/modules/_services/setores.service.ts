//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { ISetor } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class SetoresService {
  //#region publics
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ISetor {
    row = row || {};
    let R: ISetor = row;
    R.set_pk = parseInt(row.set_pk) || 0;
    R.set_fk_loja = parseInt(row.set_fk_loja) || 0;
    R.set_b_ativo = row.set_b_ativo > 0;
    R.set_f_taxa_entrega = parseFloat(row.set_f_taxa_entrega) || 0.0;
    return R;
  }

  fixes(rows: ISetor[]): ISetor[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion


  //#region methods C
  novoBairro(bairro: string, setor: ISetor, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/novo/bairro/${bairro}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, setor);
  }

  novo(idLoja: number, token: string, setor: ISetor) {
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/novo/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, setor);
  }
  //#endregion

  //#region methods R
  setores(idLoja: number, token: string): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion

  //#region methods U
  update(idSetor: number, info: any, token: string) {
    // console.log(info);
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/update/id/${idSetor}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { info: info, jwt: token });
  }
  //#endregion

  //#region methods U
  toggleAll(
    idLoja: number,
    status: boolean,
    taxaEntrega: number,
    token: string
  ) {
    // console.log(status);
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/all/loja/${idLoja}/status/${
        status ? "1" : "0"
      }/taxa/${taxaEntrega}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
 

  //#region methods D
  del(id: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/id/${id}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }

  bairroDel(idBairro: number, idLoja: number, token: string) {
    const URL = encodeURI(
      `${environment.url.api.remote}/setores/bairro/${idBairro}/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
  }
  //#endregion
}
