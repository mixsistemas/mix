//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region ionic
import { Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
//#endregion

//#region 3rd
import { BehaviorSubject } from "rxjs";
//#endregion

//#region app models
import { IViaCep } from "../_mix/_models/_interfaces";
import { IAuthJwt } from "../_shared/_models/_interfaces";
import {
  KEY_ARMAZENADOS,
  KEY_AUTH_JWT,
  KEY_FAV_LOJAS,
  KEY_FAV_PRODUTOS,
  KEY_SPLASH,
  KEY_LOCAL
} from "../_shared/_models/consts";
//#endregion

@Injectable({
  providedIn: "root"
})
export class StorageService {
  //#region methods
  onAuthChanged: BehaviorSubject<IAuthJwt> = new BehaviorSubject<IAuthJwt>(
    null
  );
  onSplashChangedEvent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  onLocalChangedEvent: BehaviorSubject<IViaCep> = new BehaviorSubject<IViaCep>(
    null
  );
  onLojFavChangedEvent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  onProFavChangedEvent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  //#endregion

  //#region constructor
  constructor(private _platform: Platform, private _storage: Storage) {}
  //#endregion

  //#region functions
  private _endId(end: IViaCep): string {
    return `${end.cep}${end.nro}${end.complemento}`.toLowerCase();
  }
  //#endregion

  //#region methods KEY_AUTH_JWT
  authJwtGet(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .get(KEY_AUTH_JWT)
        .then((jwt: string) => {
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(jwt);
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  authJwtSet(jwt: string, onSuccess?: any) {
    if (this._platform.ready()) {
      if (jwt) {
        // const JWT = `Bearer ${JSON.stringify(jwt)}`;
        this._storage
          .set(KEY_AUTH_JWT, jwt)
          .then(() => {
            if (onSuccess && typeof onSuccess === "function") {
              onSuccess();
            } // if
          })
          .catch(err => {
            console.error(err);
          });
      } else {
        this.authJwtRemove(onSuccess);
      } // else
    } // if
  }

  authJwtRemove(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .remove(KEY_AUTH_JWT)
        .then(() => {
          this.onAuthChanged.next(null);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }
  //#endregion

  //#region methods KEY_SPLASH
  splashGet(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .get(KEY_SPLASH)
        .then((splash: boolean) => {
          if (splash == null) splash = false;
          // console.log(splash);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(splash);
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  splashSet(status: boolean, onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .set(KEY_SPLASH, status)
        .then(() => {
          console.log(status);
          this.onSplashChangedEvent.next(status);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  splashRemove(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage.remove(KEY_SPLASH).then(() => {
        if (onSuccess && typeof onSuccess === "function") {
          onSuccess();
        } // if
      });
    } // if
  }
  //#endregion

  //#region methods KEY_LOCAL
  localGet(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .get(KEY_LOCAL)
        .then((local: string) => {
          // console.log(local);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(local);
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  localSet(local: IViaCep, onSuccess?: any) {
    if (this._platform.ready()) {
      if (local) {
        this._storage
          .set(KEY_LOCAL, JSON.stringify(local))
          .then(() => {
            // this.onLocalChangedEvent.emit(end);
            this.onLocalChangedEvent.next(local);
            if (onSuccess && typeof onSuccess === "function") {
              onSuccess();
            } // if
          })
          .catch(err => {
            console.error(err);
          });
      } else {
        this.localRemove(onSuccess);
      } // else
    } // if
  }

  localAdd(local: IViaCep, onSuccess?: any) {
    // console.log(local);
    if (this._platform.ready()) {
      let armazenados: IViaCep[] = [];
      this._storage
        .get(KEY_ARMAZENADOS)
        .then((enderecos: string) => {
          // console.log(enderecos);
          if (enderecos) {
            if (local) {
              let found: boolean = false;
              armazenados = JSON.parse(enderecos);
              // console.log(armazenados);
              const END_ID = this._endId(local);
              // console.log(END_ID);
              armazenados.forEach(v => {
                if (!found) {
                  if (this._endId(v) == END_ID) {
                    found = true;
                  } // if
                } // if
              });

              if (!found) {
                // console.log("NOT found.");
                armazenados.push(local);
                this._storage
                  .set(KEY_ARMAZENADOS, JSON.stringify(armazenados))
                  .then(() => {
                    if (onSuccess && typeof onSuccess === "function") {
                      onSuccess();
                    } // if
                  });
              } // if
            } // if
          } else {
            if (local) {
              // console.log(JSON.stringify([local]));
              this._storage
                .set(KEY_ARMAZENADOS, JSON.stringify([local]))
                .then(() => {
                  if (onSuccess && typeof onSuccess === "function") {
                    onSuccess();
                  } // if
                });
            } else {
              this._storage.remove(KEY_ARMAZENADOS).then(() => {
                if (onSuccess && typeof onSuccess === "function") {
                  onSuccess();
                } // if
              });
            } // else
          } // else
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  localRemove(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage.remove(KEY_LOCAL).then(() => {
        if (onSuccess && typeof onSuccess === "function") {
          onSuccess();
        } // if
      });
    } // if
  }
  //#endregion

  //#region methods KEY_ARMAZENADOS
  enderecosGet(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .get(KEY_ARMAZENADOS)
        .then((enderecos: string) => {
          // console.log(enderecos);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(enderecos);
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  enderecoDel(key: string, onSuccess?: any) {
    if (this._platform.ready()) {
      // console.log(id);
      let armazenados: IViaCep[] = [];
      this._storage
        .get(KEY_ARMAZENADOS)
        .then((enderecos: string) => {
          // console.log(key);
          // console.log(enderecos);
          if (enderecos && key) {
            const ENDERECOS = JSON.parse(enderecos);
            // console.log(enderecos);
            armazenados = ENDERECOS.filter((e: IViaCep) => e.key != key);
            // console.log(armazenados);
            this._storage
              .set(KEY_ARMAZENADOS, JSON.stringify(armazenados))
              .then(() => {
                if (onSuccess && typeof onSuccess === "function") {
                  onSuccess();
                } // if
              });
          } else {
            this._storage.remove(KEY_ARMAZENADOS).then(() => {
              this.onLocalChangedEvent.next(null);
              if (onSuccess && typeof onSuccess === "function") {
                onSuccess();
              } // if
            });
          } // else
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }
  //#endregion

  //#region methods KEY_FAV_LOJAS
  lojFavGet(onSuccess?: any) {
    // console.log("lojFavGet");
    this._platform
      .ready()
      .then(() => {
        this._storage.get(KEY_FAV_LOJAS).then((favs: string) => {
          // console.log(favs);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(favs);
          } // if
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  lojFavSet(favs: number[], onSuccess?: any) {
    if (this._platform.ready()) {
      if (favs) {
        this._storage
          .set(KEY_FAV_LOJAS, JSON.stringify(favs))
          .then(() => {
            this.onLojFavChangedEvent.next(true);
            if (onSuccess && typeof onSuccess === "function") {
              onSuccess();
            } // if
          })
          .catch(err => {
            console.error(err);
          });
      } else {
        this.lojFavRemove(onSuccess);
      } // else
    } // if
  }

  lojFavRemove(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .remove(KEY_FAV_LOJAS)
        .then(() => {
          this.onLojFavChangedEvent.next(true);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  lojFavAdd(idLoja: number, onSuccess?: any) {
    // console.log("lojfavAdd");
    if (this._platform.ready()) {
      this._storage
        .get(KEY_FAV_LOJAS)
        .then((lojasFavs: string) => {
          let favs = JSON.parse(lojasFavs || "[]");
          // console.log(favs);
          if (!favs.includes(idLoja)) {
            favs.push(idLoja);
          } // if
          // console.log(favs);
          this.lojFavSet(favs, onSuccess);
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  lojFavDel(idLoja: number, onSuccess?: any) {
    // console.log("lojfavAdd");
    if (this._platform.ready()) {
      this._storage
        .get(KEY_FAV_LOJAS)
        .then((lojasFavs: string) => {
          let favs = JSON.parse(lojasFavs || "[]");
          // console.log(favs);
          const FAVS = favs.filter(v => v !== idLoja);
          // console.log(FAVS);
          this.lojFavSet(FAVS, onSuccess);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }
  //#endregion

  //#region methods KEY_FAV_PRODUTOS
  proFavGet(onSuccess?: any) {
    // console.log("proFavGet");
    this._platform
      .ready()
      .then(() => {
        this._storage.get(KEY_FAV_PRODUTOS).then((favs: string) => {
          // console.log(favs);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess(favs);
          } // if
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  proFavSet(favs: number[], onSuccess?: any) {
    if (this._platform.ready()) {
      if (favs) {
        this._storage
          .set(KEY_FAV_PRODUTOS, JSON.stringify(favs))
          .then(() => {
            this.onProFavChangedEvent.next(true);
            if (onSuccess && typeof onSuccess === "function") {
              onSuccess();
            } // if
          })
          .catch(err => {
            console.error(err);
          });
      } else {
        this.proFavRemove(onSuccess);
      } // else
    } // if
  }

  proFavRemove(onSuccess?: any) {
    if (this._platform.ready()) {
      this._storage
        .remove(KEY_FAV_PRODUTOS)
        .then(() => {
          this.onProFavChangedEvent.next(true);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  proFavAdd(idProduto: number, onSuccess?: any) {
    // console.log("proFavAdd");
    if (this._platform.ready()) {
      this._storage
        .get(KEY_FAV_PRODUTOS)
        .then((produtosFavs: string) => {
          let favs = JSON.parse(produtosFavs || "[]");
          // console.log(favs);
          if (!favs.includes(idProduto)) {
            favs.push(idProduto);
          } // if
          // console.log(favs);
          this.proFavSet(favs, onSuccess);
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }

  proFavDel(idProduto: number, onSuccess?: any) {
    // console.log("proFavDel");
    if (this._platform.ready()) {
      this._storage
        .get(KEY_FAV_PRODUTOS)
        .then((produtosFavs: string) => {
          let favs = JSON.parse(produtosFavs || "[]");
          // console.log(favs);
          const FAVS = favs.filter(v => v !== idProduto);
          // console.log(FAVS);
          this.proFavSet(FAVS, onSuccess);
          if (onSuccess && typeof onSuccess === "function") {
            onSuccess();
          } // if
        })
        .catch(err => {
          console.error(err);
        });
    } // if
  }
  //#endregion
}
