//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { environment } from "../../../environments/environment";
import { IItemDetalhesProdutos } from "../_shared/_models/_interfaces";
//#endregion

@Injectable({
  providedIn: "root"
})
export class ItensDetalhesProdutosService {
  //#region publics
  // categoriasProdutos: IItemDetalhesProdutos[];
  // categoriasProdutosFiltered: IItemDetalhesProdutos[];
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IItemDetalhesProdutos {
    row = row || {};
    let R: IItemDetalhesProdutos = row;
    R.idp_pk = parseInt(row.idp_pk) || 0;
    R.idp_fk_detalhe = parseInt(row.idp_fk_detalhe) || 0;
    R.idp_b_ativo = row.idp_b_ativo > 0;
    R.idp_f_valor = parseFloat(row.idp_f_valor) || 0.0;
    R.qtde = 0;
    return R;
  }

  fixes(rows: IItemDetalhesProdutos[]): IItemDetalhesProdutos[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region methods C
  novo(idLoja: number, item: IItemDetalhesProdutos, token: string) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/itens-detalhes-produtos/novo/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, item);
  }
  //#endregion

  //#region methods R
  /* detalhes(idProduto: number): Observable<any> {
    const URL = encodeURI(
      `${environment.url.api.remote}/detalhes-produtos/produto/${idProduto}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  } */

  /* detalhesJwt(idProduto: number, jwt: string = ""): Observable<any> {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/detalhes-produtos/produto/${idProduto}/token/${jwt}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  } */
  //#endregion

  //#region methods U
  update(idLoja: number, item: IItemDetalhesProdutos, token: string) {
    const URL = encodeURI(
      `${
        environment.url.api.remote
      }/itens-detalhes-produtos/update/loja/${idLoja}/token/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.post(URL, item);
  }
  //#endregion
}
