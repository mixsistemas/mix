export interface IFormaPgto {
  fpg_pk: number;
  fpg_c_forma: string;
  fpg_e_tipo: 'C' | 'D' | 'O';
  fpg_c_legenda: string;
  fpg_c_img: string;
}
