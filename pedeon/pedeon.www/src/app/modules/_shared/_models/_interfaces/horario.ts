export interface IHorario {
  hor_pk: number;
  hor_fk_loja: number;
  hor_i_dia_semana: number;
  hor_i_inicio: number;
  hor_i_final: number;
}
