export interface IConta {
  con_pk: number;
  con_fk_conta: number;
  con_c_cpf: string;
  con_c_email: string;
  con_c_fone: string;
  con_c_nome: string;
  con_c_senha?: string;
  con_c_sobrenome: string;
  con_d_nascimento: Date;
  con_e_genero: "M" | "F";
}
