import { IItemDetalhesProdutos } from "./item-detalhes-produtos";

export interface IDetalheProdutos {
  det_pk?: number;
  det_fk_grupo_itens: number;
  det_b_ativo: boolean;
  det_c_detalhe: string;
  det_i_min: number;
  det_i_max: number;
  det_i_num_itens?: number;
  det_f_valor: number;
  expanded?: boolean;
  itens?: IItemDetalhesProdutos[];
  prior?: number;
  next?: number;
}
