/* import { IProduto } from "./produto"; */
export interface IGrupoProduto {
  gpr_pk: number;
  gpr_fk_loja: number;
  grp_b_ativo: boolean;
  gpr_c_grupo: string;
  gpr_i_num_produtos?: number;
  gpr_i_pos: number;
  gpr_i_prior?: number;
  gpr_i_next?: number;
}
