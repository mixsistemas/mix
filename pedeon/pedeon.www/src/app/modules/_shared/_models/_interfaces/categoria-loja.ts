export interface ICategoriaLoja {
  ctl_pk: number;
  ctl_c_categoria: string;
  ctl_c_icone: string;
}
