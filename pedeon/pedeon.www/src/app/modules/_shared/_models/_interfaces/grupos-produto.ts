/* import { IProduto } from "./produto"; */
export interface IGrupoProduto {
  gpr_pk: number;
  gpr_fk_loja: number;
  gpr_c_grupo: string;
  gpr_i_num_produtos?: number;
  gpr_i_pos: number;
}
