import { IEndereco } from "../../../_mix/_models/_interfaces";

export interface IAuthJwt {
  id: number;
  email: string;
  nome: string;
  sobrenome: string;
  enderecos?: IEndereco[];
  lojas?: number[];
}
