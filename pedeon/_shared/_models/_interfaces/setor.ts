export interface ISetor {
  set_pk?: number;
  set_fk_loja: number;
  set_b_ativo: boolean;
  set_c_setor: string;
  set_f_taxa_entrega: number;
  bairros?: any;
  expanded?: boolean;
}
