export interface IProduto {
  pro_pk?: number;
  pro_fk_grupo: number;
  pro_fk_loja?: number;
  pro_b_ativo: boolean;
  pro_b_favorito?: boolean;
  pro_b_loja_favorita?: boolean;
  pro_b_loja_fechada?: boolean;
  pro_c_barcode: string;
  pro_c_descricao: string;
  pro_c_grupo?: string;
  pro_c_img?: string;
  pro_c_loja?: string;
  pro_c_produto: string;
  pro_f_preco: number;
  pro_f_preco_prom: number;
  num_detalhes?: number;
  menor_preco?: number;
}
