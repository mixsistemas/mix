/* import { IProduto } from "./produto"; */
export interface IGrupoProdutos {
  gpr_pk: number;
  gpr_fk_loja: number;
  gpr_b_ativo: boolean;
  gpr_c_grupo: string;
  gpr_i_num_produtos?: number;
  gpr_i_pos: number;
  prior?: number;
  next?: number;
}
