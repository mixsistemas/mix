export interface IBairro {
  bai_pk: number;
  bai_c_estado: string;
  bai_c_cidade: string;
  bai_c_bairro: string;
}
