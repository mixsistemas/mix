// Storage
export const KEY_ARMAZENADOS: string = "KEY_ARMAZENADOS";
export const KEY_AUTH_JWT: string = "KEY_AUTH_JWT";
export const KEY_FAV_LOJAS: string = "KEY_FAV_LOJAS";
export const KEY_FAV_PRODUTOS: string = "KEY_FAV_PRODUTOS";
export const KEY_SPLASH: string = "KEY_SPLASH";
export const KEY_LOCAL: string = "KEY_LOCAL";

// Strings
export const FORM_CORRIJA_CAMPOS: string = "Corrija valores indicados.";
