//#region ng
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
//#endregion

//#region app components
import { HomeComponent } from "./components";
//#endregion

//#region routes
const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home",
    component: HomeComponent
  },
  { path: "**", pathMatch: "full", redirectTo: "/home" }
];
//#endregion

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
