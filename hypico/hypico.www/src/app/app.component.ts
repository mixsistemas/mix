//#region ng
import { Component } from "@angular/core";
//#endregion

//#region 3rd
import * as $ from "jquery";
import { Subscription } from "rxjs";
//#endregion

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  //#region publics
  sidemenu: boolean;
  //#endregion

  //#region methods
  onCloseSidemenuClick() {
    $(".sidemenu").removeClass("sidemenu--is-visible");
    this.sidemenu = $(".sidemenu").hasClass("sidemenu--is-visible");
    // this._checkActive();
  }

  onToggleSidemenuClick() {
    $(".sidemenu").toggleClass("sidemenu--is-visible");
    this.sidemenu = $(".sidemenu").hasClass("sidemenu--is-visible");
    // this._checkActive();
  }
  //#endregion
}
