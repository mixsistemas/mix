//#region ng
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
import { AppRoutingModule } from "./app-routing.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import { HomeComponent } from "./components";
//#endregion

@NgModule({
  imports: [BrowserModule, AppRoutingModule],
  declarations: [AppComponent, HomeComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
