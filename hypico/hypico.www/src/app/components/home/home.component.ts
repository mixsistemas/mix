//#region ng
import { AfterViewInit, Component } from "@angular/core";
//#endregion

//#region 3rd
import * as $ from "jquery";
//#endregion

//#region app models
import { IProjetoBase } from "../../modules/_mix/_models/_interfaces";
//#endregion

@Component({
  selector: "adm-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements AfterViewInit {
  //#region publics
  // corTema;
  projetos: IProjetoBase[];
  //#endregion

  constructor() {
    // this.corTema = "999";
    this.projetos = [
      {
        prj_pk: 1,
        prj_c_bg_color: "3985d1",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Plataforma de descontos.",
        prj_c_icone: "icon_descontante.png",
        prj_c_logo: "logo_descontante.png",
        prj_c_projeto: "Descontante",
        prj_c_rota: ""
      },
      {
        prj_pk: 2,
        prj_c_bg_color: "FFA500",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Etiquetas testeiras para frigoríficos.",
        prj_c_icone: "icon_frigolabel.png",
        prj_c_logo: "logo_frigolabel.png",
        prj_c_projeto: "Frigolabel",
        prj_c_rota: ""
      },
      {
        prj_pk: 3,
        prj_c_bg_color: "228B22",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Delivery online completo.",
        prj_c_icone: "icon_pedeon.png",
        prj_c_logo: "logo_pedeon.png",
        prj_c_projeto: "Pede On",
        prj_c_rota: ""
      },
      {
        prj_pk: 3,
        prj_c_bg_color: "ff4500",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Sistema de gestão comercial.",
        prj_c_icone: "icon_hypico.png",
        prj_c_logo: "logo_hypico.png",
        prj_c_projeto: "Hypico",
        prj_c_rota: ""
      },
      {
        prj_pk: 4,
        prj_c_bg_color: "1976d2",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Plataforma para avaliação de profissionais.",
        prj_c_icone: "icon_pointplus.png",
        prj_c_logo: "logo_pointplus.png",
        prj_c_projeto: "Point Plus",
        prj_c_rota: ""
      },
      {
        prj_pk: 1,
        prj_c_bg_color: "3985d1",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Plataforma de descontos.",
        prj_c_icone: "icon_descontante.png",
        prj_c_logo: "logo_descontante.png",
        prj_c_projeto: "Descontante",
        prj_c_rota: ""
      },
      {
        prj_pk: 2,
        prj_c_bg_color: "FFA500",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Etiquetas testeiras para frigoríficos.",
        prj_c_icone: "icon_frigolabel.png",
        prj_c_logo: "logo_frigolabel.png",
        prj_c_projeto: "Frigolabel",
        prj_c_rota: ""
      },
      {
        prj_pk: 3,
        prj_c_bg_color: "228B22",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Delivery online completo.",
        prj_c_icone: "icon_pedeon.png",
        prj_c_logo: "logo_pedeon.png",
        prj_c_projeto: "Pede On",
        prj_c_rota: ""
      },
      {
        prj_pk: 3,
        prj_c_bg_color: "ff4500",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Sistema de gestão comercial.",
        prj_c_icone: "icon_hypico.png",
        prj_c_logo: "logo_hypico.png",
        prj_c_projeto: "Hypico",
        prj_c_rota: ""
      },
      {
        prj_pk: 4,
        prj_c_bg_color: "1976d2",
        prj_c_fg_color: "fff",
        prj_c_descricao: "Plataforma para avaliação de profissionais.",
        prj_c_icone: "icon_pointplus.png",
        prj_c_logo: "logo_pointplus.png",
        prj_c_projeto: "Point Plus",
        prj_c_rota: ""
      }
    ];
  }

  /* getTema() {
    setTimeout(() => {
      const S = this.corTema;
      console.log(S);
      return S;
    }, 0);
  } */

  //#region lifecycles
  ngAfterViewInit() {
    // console.log("AfterViewInit");
    $(window).resize(() => {
      this._onViewportResize(); // on resize
    });
    this._onViewportResize(); // on init
  }
  //#endregion

  //#region functions
  private _onViewportResize() {
    // Ajusta altura dos tiles
    let size = $(".tile.one").width();
    // console.log(size);
    $(".tile").height(size);
    // $(".item").height(size);
    // $(".carousel").height(size);
  }
  //#endregion
}
