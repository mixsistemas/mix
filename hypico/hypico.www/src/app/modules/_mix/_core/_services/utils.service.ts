//#region ng
import { Injectable } from "@angular/core";
//#endregion

@Injectable()
export class UtilsService {
  /*
   * Generates a GUID string.
   * @returns {String} The generated GUID.
   * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
   * @author Slavik Meltser (slavik@meltser.info).
   * @link http://slavik.meltser.info/?p=142
   */
  private _p8(s: boolean = false) {
    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
  }

  guid(): string {
    return this._p8() + this._p8(true) + this._p8(true) + this._p8();
  }
}
