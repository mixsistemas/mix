<?php
header('Content-Type: application/json; charset=utf-8');

// classes
require_once __DIR__ . '/inc/classes/tables/Enderecos.php';
require_once __DIR__ . '/inc/classes/tables/Operadores.php';
require_once __DIR__ . '/inc/classes/tables/Usuarios.php';
/*
require_once __DIR__ . '/routes/web.php';
 */
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

require 'vendor/autoload.php';

// init
$app = new \Slim\App();

// cors
$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        // ->withHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});
/*
// ROUTES NON TABLE RELATED
$app->group('/', function () {
$this->get('', function (Request $request, Response $response, array $args) {
$response->getBody()->write('{"ver": "1", "server": "remote", "sys": "Point Plus"}');
return $response;
});
});
 */
// ROUTES CRUD
// enderecos
$app->group('/enderecos', function () {
    // C

    // R
    $this->post('/pag/usuario/{id_usuario}', function (Request $request, Response $response, array $args) {
        $id_usuario = (integer) $args['id_usuario'];
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblEnderecos())->R_pag($input, $id_usuario))
        );
        return $response;
    });

    // U

    // D
});

// operadores
$app->group('/operadores', function () {
    // C

    // R
    /*
    $this->post('/', function (Request $request, Response $response, array $args) {
    $input = $request->getParsedBody();
    $response->getBody()->write(
    json_encode((new TblOperadores())->R_operadores($input))
    // json_encode([$id, $input])
    );
    return $response;
    });
     */

    $this->post('/pag', function (Request $request, Response $response, array $args) {
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblOperadores())->R_pag($input))
            // json_encode($input)
        );
        return $response;
    });

    $this->post('/id/{id}', function (Request $request, Response $response, array $args) {
        $id = (int) $args['id'];
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblOperadores())->R_operador($id, $input))
            // json_encode([$id, $input])
        );
        return $response;
    });
    /*
    $this->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write(
    json_encode((new TblClientes())->R_clientes())
    );
    return $response;
    });
     */
    // U
    $this->post('/login', function (Request $request, Response $response, array $args) {
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblOperadores())->U_login($input))
        );
        return $response;
    });

    // D
});

// usuarios
$app->group('/usuarios', function () {
    // C
    $this->post('/criar', function (Request $request, Response $response, array $args) {
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblUsuarios())->C_usu($input))
        );
        return $response;
    });

    // R
    $this->post('/pag/tipo/{tipo}', function (Request $request, Response $response, array $args) {
        $tipo = (string) $args['tipo'];
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblUsuarios())->R_pag($input, $tipo))
        );
        return $response;
    });

    $this->post('/id/{id}', function (Request $request, Response $response, array $args) {
        $id = (int) $args['id'];
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblUsuarios())->R_usuario($id, $input))
            // json_encode([$id, $input])
        );
        return $response;
    });

    // U
    $this->post('/modificar', function (Request $request, Response $response, array $args) {
        $input = $request->getParsedBody();
        $response->getBody()->write(
            json_encode((new TblUsuarios())->U_usu($input))
        );
        return $response;
    });

    // D
});

/*
$app->group('/web', function () {
$this->post('/galeria/pag', function (Request $request, Response $response, array $args) {
$input = $request->getParsedBody();
$response->getBody()->write(
// json_encode($input)
json_encode(web_galeria_pag($input))
);
return $response;
});
});

// ROUTES CRUD

// clientes
$app->group('/clientes', function () {
// C

// R
$this->get('/', function (Request $request, Response $response, array $args) {
$response->getBody()->write(
json_encode((new TblClientes())->R_clientes())
);
return $response;
});

$this->get('/cpf-cnpj/{cpf_cnpj}/token/{token}', function (Request $request, Response $response, array $args) {
$cpf_cnpj = (string) $args['cpf_cnpj'];
$token = (string) $args['token'];
$response->getBody()->write(
json_encode((new TblClientes())->R_cliente_licenca($cpf_cnpj, $token))
);
return $response;
});

// U

// D
});

// formas_pgto
$app->group('/formas-pgto', function () {
// C

// R
$this->get('/', function (Request $request, Response $response, array $args) {
$response->getBody()->write(
json_encode((new TblFormasPgto())->R_formas_pgto())
);
return $response;
});

// U

// D
});

// updates
$app->group('/updates', function () {
// C

// R
$this->get('/pos/{pos}', function (Request $request, Response $response, array $args) {
$pos = (int) $args['pos'];
$response->getBody()->write(
json_encode((new TblUpdates())->R_updates($pos))
);
return $response;
});

// U

// D
});

 */
// NOTE: make sure this route is defined last (cors)
// Catch-all route to serve a 404 Not Found page if none of the routes match
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

// Run app
$app->run();
