<?php
require_once __DIR__ . '/../consts.php';

class Files
{
    /*
    Retorna paths úteis do sistema.
    @Input:
    Nenhum.
    @Output:
    {$data['htdocs']} <STRING> Diretório root do Web Server.
    {$data['tmp_dir']} <STRING> Diretório temporário do sistema.
     */
    public function system_paths()
    {
        $data = [
            'htdocs' => '',
            'tmp_dir' => '',
        ];

        $data['htdocs'] = rawurldecode($this->htdocs_dir());
        $data['tmp_dir'] = rawurldecode($this->tmp_dir());
        /*
        // Substitui \ por /
        $data['htdocs'] = str_replace("\\", "/", $data['htdocs']);
        $data['tmp_dir'] = str_replace("\\", "/", $data['tmp_dir']);
         */
        return $data;
    }

    /*
    Retorna um path indicado do Web Server (cria todos).
    @Input:
    {$subfolder} <'root' (default) | 'tmp' | 'api' | 'cups' | 'galeria' | 'hypico'>.
    @Output:
    <STRING> Diretório solicitado do Web Server.
     */
    public function htdocs_dir($subfolder = '')
    {
        // Vars
        $htdocs = $_SERVER["DOCUMENT_ROOT"];
        $path = $htdocs;

        $new = $htdocs . '/' . L_ASSETS;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_IMG;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_GALERIA;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_GALERIA . 'L/';
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_GALERIA . 'S/';
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_LOJA;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_LOJA . 'L/';
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_LOJA . 'S/';
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_ASSETS_FINALIZADORAS;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_TMP;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = $htdocs . '/' . L_HYPICO;
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $path = '';
        switch ($subfolder) {
            case 'root':
            case '':
                $path = $htdocs;
                break;

            case 'tmp':
                $path = $htdocs . '/' . L_TMP;
                break;

            case 'hypico':
                $path = $htdocs . '/' . L_HYPICO;
                break;

            case 'api':
                $path = $htdocs . '/' . L_API;
                break;

            case 'cups':
                $path = $htdocs . '/' . L_API_CUPS;
                break;

            case 'galeria':
                $path = $htdocs . '/' . L_ASSETS_GALERIA;
                break;
        } // switch

        return $path;
    }

    /*
    Retorna pasta temporária do sistema.
    @Input:
    Nenhum.
    @Output:
    <STRING> Diretório temporário do sistema.
     */
    public function tmp_dir()
    {
        return sys_get_temp_dir();
    }

    /*
    Retorna array de nomes de arquivos de uma pasta (sem recursão) com wildcards.
    @Input:
    {$dir} <string> Diretório de origem dos arquivos.
    {$mask} <string> Máscara de filtragem de arquivos. '*.*' (default).
    @Output:
    <STRING>[] Nomes dos arquivos encontrados.
     */
    public function get_file_list($dir, $mask = '*.*')
    {
        $ret = [];

        if (!empty($dir)) {
            // add trailing slash if missing
            if (substr($dir, -1) != "/") {
                $dir .= "/";
            } // if

            foreach (glob("{$dir}{$mask}") as $file) {
                $ret[] = basename($file);
            } // foreach
        } // if

        return $ret;
    }

    public function count_lines($file)
    {
        $lines = 0;
        if (!empty($file)) {
            $F = fopen($file, 'rb');
            try {
                while (!feof($F)) {
                    $lines += substr_count(fread($F, 8192), "\n");
                } // while
                return $lines;
            } catch (Exception $e) {
                fclose($F);
            } // try-catch
        } // if
    }
}
