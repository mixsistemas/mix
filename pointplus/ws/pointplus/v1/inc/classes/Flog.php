<?php

require_once __DIR__ . '/DateLib.php';
require_once __DIR__ . '/Files.php';

class Flog
{
    private $logall = false; // false = erros, true = todas

    /*
    Grava linha de log de erros/acessos de cada função chamada no back-end.
    @Input:
    Nenhum. Propriedade global {$logall} define se apenas erros (false) ou todas (true) chamadas serão logados.
    @Output:
    Nova linha no final do arquivo '<WEB_ROOT>/errors.log' ou '<WEB_ROOT>/all.log'.
     */
    public function log(
        $data,
        $fn,
        $ref,
        $crud = ''
    ) {
        $errors = '';
        $ok = false;
        $crud = empty($crud) ? ' ' : " ({$crud}) ";
        $data_op = (new DateLib())->get_current_timestamp();
        $filename = ($this->logall ? 'all' : 'errors') . '.log';
        $htdocs = (new Files())->htdocs_dir();
        if (isset($data)) {
            if (isset($data['ok'])) {
                $ok = $this->logall ? true : !$data['ok'];
            } // if
            if (isset($data['errors'])) {
                $errors = implode('|', $data['errors']);
            } // if
            if (empty($errors)) {
                $errors = 'ok';
            } // if
        } // if

        if ($ok) {
            file_put_contents(
                "{$htdocs}/{$filename}",
                "{$data_op} {$fn}@{$ref}{$crud}{$errors}" . PHP_EOL,
                FILE_APPEND
            );
        } // if
    }
}
