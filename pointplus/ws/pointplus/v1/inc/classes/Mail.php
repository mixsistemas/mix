<?php
class Mail
{
    // Privates
    private $_from;
    private $_to;
    private $_subject;
    private $_msg;
    private $_headers = '';

    // constructor
    public function __construct($to, $subject, $msg, $from = '')
    {
        $this->_to = $to;
        $this->_subject = $subject;
        $this->_msg = $msg;
        $this->_from = $from;
        
        // Always set content-type when sending HTML email
        $this->_headers = "MIME-Version: 1.0" . "\r\n";
        $this->_headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        if (!empty($from)):
            $this->_headers .= "From: <{$this->_from}>" . "\r\n";
        endif;
    }

    public function send()
    {
        mail(
            $this->_to,
            $this->_subject,
            $this->_msg,
            $this->_headers
        );
    }

}
