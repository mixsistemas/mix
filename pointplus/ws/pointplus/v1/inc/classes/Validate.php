<?php
class Validate
{
    public function cpf($cpf)
    {
        $cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
        // Valida tamanho
        if (strlen($cpf) != 11) {
            return false;
        }

        // Calcula e confere primeiro dígito verificador
        for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--) {
            $soma += $cpf{$i} * $j;
        }

        $resto = $soma % 11;
        if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto)) {
            return false;
        }

        // Calcula e confere segundo dígito verificador
        for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--) {
            $soma += $cpf{$i} * $j;
        }

        $resto = $soma % 11;
        return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
    }

    public function cnpj($cnpj = null)
    {

        // Verifica se um número foi informado
        if (empty($cnpj)) {
            return false;
        }

        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cnpj) != 14) {
            return false;
        }

        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' ||
            $cnpj == '11111111111111' ||
            $cnpj == '22222222222222' ||
            $cnpj == '33333333333333' ||
            $cnpj == '44444444444444' ||
            $cnpj == '55555555555555' ||
            $cnpj == '66666666666666' ||
            $cnpj == '77777777777777' ||
            $cnpj == '88888888888888' ||
            $cnpj == '99999999999999') {
            return false;

            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            $j = 5;
            $k = 6;
            $soma1 = "";
            $soma2 = "";

            for ($i = 0; $i < 13; $i++) {
                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;
                $soma2 += ($cnpj{$i} * $k);
                if ($i < 12) {
                    $soma1 += ($cnpj{$i} * $j);
                }
                $k--;
                $j--;
            }

            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

            return (($cnpj{12} == $digito1) and ($cnpj{13} == $digito2));
        }
    }

    /*
    Return 'CPF', 'CNPJ' ou ''.
     */
    public function cpf_cnpj($val)
    {
        $S = '';
        // Deixa apenas números no valor
        // $val = (string) preg_replace('/[^0-9]/', '', $val);
        $l = strlen($val);
        switch ($l) {
            case 11: // CPF?
                $S = $this->cpf($val) ? 'CPF' : '';
                break;
            case 14: // CNPJ?
                $S = $this->cnpj($val) ? 'CNPJ' : '';
                break;
            default: // Inválido.
                $S = '';
        } // switch
        return $S;
    /*
    if ($l === 11): // CPF?
    return $this->cpf($val) ? 'CPF' : '';
    elseif ($l === 14): // CNPJ?
    return $this->cnpj($val) ? 'CNPJ' : '';
    else: // Inválido.
    return '';
    endif;
     */
    }
}
