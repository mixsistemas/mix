<?php
class Format
{
    /*
    Mascara string de telefone.
    @Input:
    {$val} <STRING> Fone apenas com números e >= 10 caracteres.
    @Output:
    <STRING> Fone formatado.
    '' se nenhum {$val} indicado ou < 10.
     */
    public function fone($val)
    {
        $s = '';
        if (!empty($val)):
            $l = strlen($val);
            if ($l >= 11):
                $s = '(' . substr($val, 0, 2) . ') ' . substr($val, 2, 5) . '-' . substr($val, 7);
            elseif ($l == 10):
                $s = '(' . substr($val, 0, 2) . ') ' . substr($val, 2, 4) . '-' . substr($val, 6);
            endif;
        endif;

        return $s;
    }

    /*
    Mascara string de CPF/CNPJ.
    @Input:
    {$val} <STRING> Fone apenas com números e com 11 ou 14 caracteres.
    @Output:
    <STRING> CPF/CNPJ formatado.
    '' se nenhum {$val} indicado ou < 10.
     */
    public function cpf_cnpj($val)
    {
        $s = '';
        if (!empty($val)):
            $l = strlen($val);
            if ($l == 14):
                $s = substr($val, 0, 2) . '.' . substr($val, 2, 3) . '.' . substr($val, 5, 3) . '/' . substr($val, 8, 4) . '-' . substr($val, 12);
            elseif ($l == 11):
                $s = substr($val, 0, 3) . '.' . substr($val, 3, 3) . '.' . substr($val, 6, 3) . '-' . substr($val, 9);
            endif;
        endif;

        return $s;
    }

    /*
    Mascara string de CEP.
    @Input:
    {$val} <STRING> CEP apenas com números e 8 caracteres.
    @Output:
    <STRING> CEP formatado.
    '' se nenhum {$val} indicado ou != 8 caracteres.
     */
    public function cep($val)
    {
        $s = '';
        if (!empty($val)):
            $l = strlen($val);
            if ($l == 8):
                $s = substr($val, 0, 5) . '-' . substr($val, 5, 3);
            endif;
        endif;

        return $s;
    }

    /*
    Retorna número sem casas decimais (se inteiro) ou com 2 (se decimal).
    @Input:
    {$val} <INTEGER> ou <FLOAT>.
    @Output:
    <NUMBER> se inteiro.
    <STRING> 2 casas decimais se float.
     */
    public function int($val)
    {
        if (round($val, 0) == $val):
            return (int) $val;
        else:
            return number_format($val, 2, ',', '.');
        endif;
    }

    /*
    Converte boolean para 'S' ou 'N'.
    @Input:
    {$val} <BOOLEAN> ou <NUMBER>.
    @Output:
    <STRING> 'S' se true e 'N' se false.
     */
    public function int2Bol($val)
    {
        return $val > 0 ? 'S' : 'N';
    }

    /*
    Retorna string do número com 'n' casas decimais.
    @Input:
    {$val} <NUMBER>.
    {$decimals} <INTEGER> default 2.
    @Output:
    <STRING> Número formatado com casas decimais.
     */
    public function nro($val, $decimals = 2)
    {
        /* if (is_numeric($val)) {
        $val = floatval($val);
        } // if */

        $s = number_format($val, $decimals);
        $s = str_replace('.', ',', $s);
        return $s;
    }

    /*
    Retorna string do número com 'n' casas decimais ou inteiro.
    @Input:
    {$val} <NUMBER>.
    {$decimals} <INTEGER> default 2.
    @Output:
    <STRING> Número formatado com casas decimais ou inteiro.
     */
    public function qtde($val, $decimals = 2)
    {
        if (is_numeric($val)):
            $val = floatval($val);
        endif;

        if (is_numeric($val) && floor($val) == $val):
            $s = (string) floor($val);
        else:
            $s = number_format($val, $decimals);
            $s = str_replace('.', ',', $s);
        endif;

        return $s;
    }

    /*
    Retorna string do número com símbolo da moeda.
    @Input:
    {$val} <NUMBER>.
    {$prefix} <STRING> default 'R$ '.
    @Output:
    <STRING> Número prefixado com símbolo da moeda.
     */
    public function real($val, $prefix = 'R$ ')
    {
        if (is_numeric($val)):
            $val = floatval($val);
        endif;

        return $prefix . number_format($val, 2, ',', '.');
    }

    /*
    Reduz string de data para datetime.
    @Input:
    {$str} <STRING> String de data.
    {$prefix} <STRING> default '-'.
    @Output:
    <STRING> Data no formato 'dd-mm-yyyy'.
     */
    public function str2dt($str, $sep = '-')
    {
        $ret_ = '';
        $str = trim($str);
        if (strlen($str) >= 10):
            $ret_ = substr($str, 8, 2) .
            $sep .
            substr($str, 5, 2) .
            $sep .
            substr($str, 0, 4);
        endif;

        return $ret_;
    }

    /*
    Reduz string de data para timestamp.
    @Input:
    {$str} <STRING> String de data.
    {$prefix} <STRING> default '-'.
    @Output:
    <STRING> Data no formato 'dd-mm-yyyy hh:nn'.
     */
    public function str2ts($str, $sep = '-')
    {
        $ret_ = '';
        $str = trim($str);
        if (strlen($str) >= 16):
            $ret_ = substr($str, 8, 2) .
            $sep .
            substr($str, 5, 2) .
            $sep .
            substr($str, 0, 4) .
            ' ' .
            substr($str, 11, 5);
        endif;

        return $ret_;
    }
}
