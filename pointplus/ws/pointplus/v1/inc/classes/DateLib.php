<?php
require_once __DIR__ . '/../consts.php';
// require_once __DIR__ . '/tables/Loja.php';

class DateLib
{
    /*
    Retorna data/hora atuais baseado no timezone do estado indicado no cadastro da loja.
    @Input:
    Nenhum.
    @Output:
    <STRING> Data/hora atuais.
    <FALSE> Falha ao identificar timezone.
     */
    public function get_current_timestamp($uf = 'DF')
    {
        if ($uf) {
            $s = TIMEZONES_UFS[$uf];
            if ($s) {
                date_default_timezone_set($s);
                return date('Y-m-d H:i:s');
            } else {
                return false;
            } // else
        } else {
            return false;
        } // else
    }

    /*
    Formata string de data/hora.
    @Input:
    {$str} <STRING> Data/hora.
    {$fmt} <STRING> Formato.
    @Output:
    <STRING> Data/hora no formato indicado.
    '' Nenhum {$str} indicado.
     */
    public function format($str, $fmt = 'd-m-Y H:i')
    {
        if (empty($str)) {
            return '';
        } else {
            $date = new DateTime($str);
            return $date->format($fmt); // 31-07-2012
        } // else
    }

    /*
    Converte número de segundos para string de intervalo.
    @Input:
    {$segs} <INTEGER> Total de segundos.
    @Output:
    <STRING> Formato '(n)dias (n)horas (n)mins'.
     */
    public function secs2Interval($segs)
    {
        $dd = 0;
        $hh = 0;
        $mm = intdiv($segs, 60); // segs -> mins

        if ($mm >= 60) {
            $hh = intdiv($mm, 60); // mins -> horas
            $mm = $mm % 60;

            if ($hh >= 24) {
                $dd = intdiv($hh, 24); // horas -> dias
                $hh = $hh % 24;
            } // if
        } // if

        // return "dd: {$dd}, hh: {$hh}, mm: {$mm}";
        return trim(
            ($dd > 0 ? "{$dd}d" : '') .
            ($hh > 0 ? " {$hh}h" : '') .
            ($mm > 0 ? " {$mm}m" : '')
        );
    }
}
