<?php
date_default_timezone_set('America/Sao_Paulo');

require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Mail.php';
require_once __DIR__ . '/../Email.php';
require_once __DIR__ . '/../Format.php';
// require_once __DIR__ . '/../Flog.php';
require_once __DIR__ . '/../Sanitize.php';
require_once __DIR__ . '/../Str.php';
require_once __DIR__ . '/../Validate.php';
require_once __DIR__ . '/../tables/Operadores.php';

class TblUsuarios
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = false)
    {
        if ($conn):
            $this->conn = $conn;
        else:
            $this->conn = (new Db())->mysql_conn();
        endif;
    }

    // Public methods

    // C
    public function C_usu($input)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => 0,
        ];

        $data['input'] = $input;

        // Vars
        $cpf_cnpj = '';
        $criado_em = '';
        $email = '';
        $fone_cel = '';
        $fone_fixo = '';
        $fone_msg = '';
        $id = '';
        $id_operador = '';
        $modificado_em = '';
        $san = new Sanitize();
        $tipo = '';
        $usuario = '';

        // Params
        if (isset($input)):
            if (isset($input['auth'])):
                $auth = $input['auth'];
                $id_operador = isset($auth['id']) ? $san->int($auth['id']) : 0;
            endif;

            if (isset($input['usuario'])):
                $usu = $input['usuario'];
                $cpf_cnpj = isset($usu['usu_c_cpf_cnpj']) ? $san->str($usu['usu_c_cpf_cnpj']) : '';
                $criado_em = isset($usu['usu_dt_criado_em']) ? $san->str($usu['usu_dt_criado_em']) : '';
                $email = isset($usu['usu_c_email']) ? $san->str($usu['usu_c_email']) : '';
                $fone_cel = isset($usu['usu_c_fone_cel']) ? $san->str($usu['usu_c_fone_cel']) : '';
                $fone_fixo = isset($usu['usu_c_fone_fixo']) ? $san->str($usu['usu_c_fone_fixo']) : '';
                $fone_msg = isset($usu['usu_c_fone_msg']) ? $san->str($usu['usu_c_fone_msg']) : '';
                $id = isset($usu['usu_c_id']) ? $san->str($usu['usu_c_id']) : '';
                $modificado_em = isset($usu['usu_dt_modificado_em']) ? $san->str($usu['usu_dt_modificado_em']) : '';
                $tipo = isset($usu['usu_e_tipo']) ? $san->str($usu['usu_e_tipo']) : '';
                $usuario = isset($usu['usu_c_usuario']) ? $san->str($usu['usu_c_usuario']) : '';
            endif;
        endif;

        // Validation
        if (isset($auth)):
            // if $auth['tipo'] == 'ope'
            $resp = (new TblOperadores())->R_validate($auth);
            // $data['data'] = $resp;
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (empty($id)):
            $data['errors']['id'] = 'Nenhum identificador indicado.';
        else:
            $resp = $this->R_str_existe('usu_c_id', $id, 0);
            // $data['resp'] = $resp;
            if ($resp['ok'] && $resp['data']): 
                $data['errors']['id'] = 'Identificador já está sendo utilizado.';
            endif;
        endif;

        if (empty($tipo)):
            $data['errors']['tipo'] = 'Nenhum tipo de usuário indicado.';
        endif;

        if (empty($usuario)):
            $data['errors']['usuario'] = 'Nenhum nome de usuário indicado.';
        endif;

        if (empty($cpf_cnpj)):
            $data['errors']['cpfCnpj'] = 'Nenhum CPF/CNPJ indicado.';
        else:
            if (empty((new Validate)->cpf_cnpj($cpf_cnpj))):
                $data['errors']['cpfCnpj'] = 'CPF/CNPJ inválido.';
            else:
                $resp = $this->R_str_existe('usu_c_cpf_cnpj', $cpf_cnpj, 0);
                // $data['resp'] = $resp;
                if ($resp['ok'] && $resp['data']): 
                    $data['errors']['cpfCnpj'] = 'CPF/CNPJ já está sendo utilizado.';
                endif;
            endif;
        endif;

        if (empty($criado_em)):
            $data['errors']['criadoEm'] = 'Nenhuma data de criação indicada.';
        endif;

        if (empty($modificado_em)):
            $data['errors']['modificadoEm'] = 'Nenhuma data de modificação indicada.';
        endif;

        if (empty($email)):
            $data['errors']['email'] = 'Nenhum E-mail indicado.';
        else:
            $resp = $this->R_str_existe('usu_c_email', $email, 0);
            // $data['resp'] = $resp;
            if ($resp['ok'] && $resp['data']): 
                $data['errors']['email'] = 'E-mail já está sendo utilizado.';
            else:
                // Gera senha aleatória.
                $senha = (new Str())->random(6);
                $hash = password_hash($senha, PASSWORD_DEFAULT);
                $nome = strtoupper($usuario);
                $label_tipo = TIPOS_USU[$tipo];
            endif;
        endif;

        if ($id_operador < 1):
            $data['errors']['idOperador'] = 'Nenhum operador indicado.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    INSERT INTO
                        usuarios (
                            usu_fk_criado_por,
                            usu_fk_modificado_por,
                            usu_c_cpf_cnpj,
                            usu_c_email,
                            usu_c_fone_cel,
                            usu_c_fone_fixo,
                            usu_c_fone_msg,
                            usu_c_id,
                            usu_c_senha,
                            usu_c_usuario,
                            usu_dt_criado_em,
                            usu_dt_modificado_em,
                            usu_e_tipo
                        ) VALUES (
                            :criado_por,
                            :modificado_por,
                            :cpf_cnpj,
                            :email,
                            :fone_cel,
                            :fone_fixo,
                            :fone_msg,
                            :id,
                            :senha,
                            :usuario,
                            :criado_em,
                            :modificado_em,
                            :tipo
                        )
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':criado_por', $id_operador, PDO::PARAM_INT);
                $stmt->bindValue(':modificado_por', $id_operador, PDO::PARAM_INT);
                $stmt->bindValue(':cpf_cnpj', $cpf_cnpj);
                $stmt->bindValue(':email', $email);
                $stmt->bindValue(':fone_cel', $fone_cel);
                $stmt->bindValue(':fone_fixo', $fone_fixo);
                $stmt->bindValue(':fone_msg', $fone_msg);
                $stmt->bindValue(':id', $id);
                $stmt->bindValue(':senha', $hash);
                $stmt->bindValue(':usuario', $usuario);
                $stmt->bindValue(':criado_em', $criado_em);
                $stmt->bindValue(':modificado_em', $modificado_em);
                $stmt->bindValue(':tipo', $tipo);
                $stmt->execute();
                $data['data'] = (int) trim($this->conn->lastInsertId());

                $data['ok'] = true;

                // Envia e-mail com credenciais.
                if (!empty($email)):
                    $from = 'mix.sistemas.dev@google.com';
                    $subject = "{$nome}, seja bem vindo ao Point Plus.";
                    $msg = "
                    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Novo usuário Point Plus</title><meta http-equiv='Content-Type' content='text/html charset=UTF-8'></head><body><style></style><center><p style='margin-bottom:5px'><strong>{$nome}</strong>, seja bem vindo ao</p><img src='http://pedeon.com.br/pointplus.com.br/adm/assets/img/logo_pp.png' style='height:64px'><br><i>www.pointplus.com.br</i><br><br><p class='mb-5' style='margin-bottom:5px'>Tipo de acesso: <strong>{$label_tipo}</strong></p><table class='w-400' style='width:400px'><tbody><tr><th style='background-color:silver;border:1px solid gray;font-weight:700;padding:5px;text-align:center;text-shadow:1px 1px #fff'>Seu Identificador</th><th style='background-color:silver;border:1px solid gray;font-weight:700;padding:5px;text-align:center;text-shadow:1px 1px #fff'>Sua senha</th></tr><tr><td style='border:1px solid silver;padding:5px;text-align:center'>{$id}</td><td style='border:1px solid silver;padding:5px;text-align:center'>{$senha}</td></tr></tbody></table><br><h6 class='light-text' style='color:#999'>Essa é uma senha temporária, modifique-a assim que acessar o site.</h6></center></body></html>
                    ";

                    // $data['emailOk'] = (new Email($email, $subject, $msg, $from))->send();
                    $data['emailOk'] = (new Mail($email, $subject, $msg, $from))->send();
                endif;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'C_usu', 'usuarios', 'C');

        return $data;
    }

    // R
    public function R_str_existe($campo, $valor, $id_usuario)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => true,
        ];

        // Vars
        $san = new Sanitize();

        // Params
        $campo = isset($campo) ? $san->str($campo) : '';
        $valor = isset($valor) ? $san->str($valor) : '';
        $id_usuario = isset($id_usuario) ? $san->int($id_usuario) : 0;

        // Validation
        if (empty($campo)) {
            $data['errors']['campo'] = 'Nenhum nome de campo indicado.';
        } // if

        if (empty($valor)) {
            $data['errors']['valor'] = 'Nenhum valor indicado.';
        } // if

        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            $sql = "
                SELECT
                    COALESCE(COUNT(usu_pk), 0)
                FROM
                    usuarios
                WHERE
                    {$campo} = :valor
                    AND usu_pk <> :id_usuario
                LIMIT
                    1
                ;";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':valor', $valor);
            $stmt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
            $stmt->execute();
            $data['data'] = (boolean) $stmt->fetchColumn();

            $data['ok'] = true;
        } // if

        // (new Flog())->log($data, 'R_cpf_cnpj_existe', 'usuarios', 'R');

        return $data;
    }

    public function R_pag($input, $tipo)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => [
                'rows' => null,
                'count' => 0,
            ],
        ];

        // $data['input'] = $input;
        // $data['tipo'] = $tipo;

        // Vars
        $auth = null;
        $desc = false;
        $filter = '';
        $limit = 0;
        $offset = 0;
        $order_by = '';
        $page = 0;
        $san = new Sanitize();

        // Params
        $tipo = isset($tipo) ? $san->str($tipo) : '';
        if (isset($input)):
            if (isset($input['auth'])):
                $auth = $input['auth'];
            endif;

            if (isset($input['pag'])):
                $pag = $input['pag'];
                $filter = isset($pag['filter']) ? $san->str($pag['filter']) : '';
                $page = isset($pag['current']) ? $san->int($pag['current']) : 0;
                $limit = isset($pag['limit']) ? $san->int($pag['limit']) : 0;
                $offset = ($page - 1) * $limit;

                if (isset($pag['sorting'])):
                    $sorting = $pag['sorting'];
                    if (isset($sorting)):
                        $desc = isset($sorting['desc']) ? $sorting['desc'] : false;
                        $order_by = isset($sorting['field']) ? $san->str($sorting['field']) : '';
                    endif;
                endif;
            endif;
        endif;

        // Validation
        if (isset($auth)):
            $resp = (new TblOperadores())->R_validate($auth);
            // $data['data'] = $resp;
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (empty($tipo)):
            $resp['errors']['tipo'] = 'Tipo de usuário não indicado.';
        endif;

        if (!empty($order_by)):
            $order_by = " ORDER BY {$order_by} ";
            if ($desc > 0):
                $order_by .= 'DESC ';
            endif;
        endif;

        if (!empty($filter)):
            $filter = " AND ( usu_c_usuario LIKE '%{$filter}%' ) ";
        else:
            $filter = '';
        endif;

        if ($page < 1):
            $data['errors']['page'] = 'Nenhuma página indicada ou inválida.';
        endif;

        if ($limit < 1):
            $data['errors']['limit'] = 'Nenhum limite de entradas indicado ou inválido.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                // Calcula total.
                $sql = "
                    SELECT
                        COALESCE(COUNT(usu_pk), 0)
                    FROM
                        usuarios
                    WHERE
                        usu_e_tipo = :tipo
                        {$filter}
                    LIMIT
                        1
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':tipo', $tipo);
                $stmt->execute();
                $data['data']['count'] = (int) $stmt->fetchColumn();

                // Lazy load.
                $sql = "
                    SELECT
                        *
                    FROM
                        usuarios
                    WHERE
                        usu_e_tipo = :tipo
                        {$filter}
                        {$order_by}
                    LIMIT
                        :limit
                    OFFSET
                        :offset
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
                $stmt->bindValue(':tipo', $tipo);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows):
                    $data['data']['rows'] = $rows;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_usu_pag', 'usuarios', 'R');

        return $data;
    }

    public function R_usuario($id_usuario, $auth)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        $data['input'] = $auth;

        // Vars
        $san = new Sanitize();

        // Params
        $id_usuario = isset($id_usuario) ? $san->int($id_usuario) : 0;

        // Validation
        if ($id_usuario < 1):
            $data['errors']['usuario'] = 'Nenhum usuário indicado.';
        endif;

        if (isset($auth)):
            $resp = (new TblOperadores())->R_validate($auth);
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    SELECT
                        usuarios.*,
                        oc.ope_c_operador AS usu_c_criado_por,
                        om.ope_c_operador AS usu_c_modificado_por
                    FROM
                        usuarios
                    LEFT JOIN
                        operadores AS oc ON usu_fk_criado_por=oc.ope_pk
                    LEFT JOIN
                        operadores AS om ON usu_fk_modificado_por=om.ope_pk
                    WHERE
                        usu_pk = :id_usuario
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row):
                    $data['data'] = $row;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_usuario', 'usuarios', 'R');

        return $data;
    }

    // U
    public function U_usu($input)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        $data['input'] = $input;

        // Vars
        $ativo = 0;
        $cpf_cnpj = '';
        $email = '';
        $fone_cel = '';
        $fone_fixo = '';
        $fone_msg = '';
        $for = new Format();
        $id = '';
        $id_operador = '';
        $id_usuario = '';
        $modificado_em = '';
        $san = new Sanitize();
        $tipo = '';
        $usuario = '';

        // Params
        if (isset($input)):
            if (isset($input['auth'])):
                $auth = $input['auth'];
                $id_operador = isset($auth['id']) ? $san->int($auth['id']) : 0;
            endif;

            if (isset($input['usuario'])):
                $usu = $input['usuario'];

                $ativo = isset($usu['usu_b_ativo']) ? $san->int($usu['usu_b_ativo']) : 0;
                $cpf_cnpj = isset($usu['usu_c_cpf_cnpj']) ? $san->str($usu['usu_c_cpf_cnpj']) : '';
                $email = isset($usu['usu_c_email']) ? $san->str($usu['usu_c_email']) : '';
                $fone_cel = isset($usu['usu_c_fone_cel']) ? $san->str($usu['usu_c_fone_cel']) : '';
                $fone_fixo = isset($usu['usu_c_fone_fixo']) ? $san->str($usu['usu_c_fone_fixo']) : '';
                $fone_msg = isset($usu['usu_c_fone_msg']) ? $san->str($usu['usu_c_fone_msg']) : '';
                $id = isset($usu['usu_c_id']) ? $san->str($usu['usu_c_id']) : '';
                $id_usuario = isset($usu['usu_pk']) ? $san->str($usu['usu_pk']) : '';
                $modificado_em = isset($usu['usu_dt_modificado_em']) ? $san->str($usu['usu_dt_modificado_em']) : '';
                $tipo = isset($usu['usu_e_tipo']) ? $san->str($usu['usu_e_tipo']) : '';
                $usuario = isset($usu['usu_c_usuario']) ? $san->str($usu['usu_c_usuario']) : '';
            endif;
        endif;

        // Validation
        if (isset($auth)):
            $resp = (new TblOperadores())->R_validate($auth);
            // $data['data'] = $resp;
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (empty($id)):
            $data['errors']['id'] = 'Nenhum identificador indicado.';
        else:
            $resp = $this->R_str_existe('usu_c_id', $id, $id_usuario);
            // $data['resp'] = $resp;
            if ($resp['ok'] && $resp['data']): 
                $data['errors']['id'] = 'Identificador já está sendo utilizado.';
            endif;
        endif;

        if (empty($tipo)):
            $data['errors']['tipo'] = 'Nenhum tipo de usuário indicado.';
        endif;

        if (empty($usuario)):
            $data['errors']['usuario'] = 'Nenhum nome de usuário indicado.';
        endif;

        if (empty($cpf_cnpj)):
            $data['errors']['cpfCnpj'] = 'Nenhum CPF/CNPJ indicado.';
        else:
            if (empty((new Validate)->cpf_cnpj($cpf_cnpj))):
                $data['errors']['cpfCnpj'] = 'CPF/CNPJ inválido.';
            else:
                $resp = $this->R_str_existe('usu_c_cpf_cnpj', $cpf_cnpj, $id_usuario);
                // $data['resp'] = $resp;
                if ($resp['ok'] && $resp['data']): 
                    $data['errors']['cpfCnpj'] = 'CPF/CNPJ já está sendo utilizado.';
                endif;
            endif;
        endif;

        if (empty($modificado_em)):
            $data['errors']['modificadoEm'] = 'Nenhuma data de modificação indicada.';
        endif;

        if (empty($email)):
            $data['errors']['email'] = 'Nenhum e-mail indicado.';
        else:
            $resp = $this->R_str_existe('usu_c_email', $email, $id_usuario);
            // $data['resp'] = $resp;
            if ($resp['ok'] && $resp['data']): 
                $data['errors']['email'] = 'E-mail já está sendo utilizado.';
            else:
                $nome = strtoupper($usuario);
                $label_tipo = TIPOS_USU[$tipo];
            endif;
        endif;

        if ($id_operador < 1):
            $data['errors']['idOperador'] = 'Nenhum operador indicado.';
        endif;

        if ($id_usuario < 1):
            $data['errors']['idUsuario'] = 'Nenhum usuário indicado.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    UPDATE
                        usuarios
                    SET
                        usu_b_ativo = :ativo,
                        usu_fk_modificado_por = :modificado_por,
                        usu_c_cpf_cnpj = :cpf_cnpj,
                        usu_c_email = :email,
                        usu_c_fone_cel = :fone_cel,
                        usu_c_fone_fixo = :fone_fixo,
                        usu_c_fone_msg = :fone_msg,
                        usu_c_id = :id,
                        usu_c_usuario = :usuario,
                        usu_dt_modificado_em = :modificado_em
                    WHERE
                        usu_pk = :id_usuario
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':ativo', $ativo, PDO::PARAM_INT);
                $stmt->bindValue(':modificado_por', $id_operador, PDO::PARAM_INT);
                $stmt->bindValue(':cpf_cnpj', $cpf_cnpj);
                $stmt->bindValue(':email', $email);
                $stmt->bindValue(':fone_cel', $fone_cel);
                $stmt->bindValue(':fone_fixo', $fone_fixo);
                $stmt->bindValue(':fone_msg', $fone_msg);
                $stmt->bindValue(':id', $id);
                $stmt->bindValue(':usuario', $usuario);
                $stmt->bindValue(':modificado_em', $modificado_em);
                $stmt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
                $stmt->execute();

                $data['ok'] = true;

                // Envia e-mail com dados.
                if (!empty($email)):
                    $from = 'mix.sistemas.dev@google.com';
                    $subject = "{$nome}, seu cadastro sofreu modificações.";
                    $msg = "
                    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Modificação de usuário Point Plus</title><meta http-equiv='Content-Type' content='text/html charset=UTF-8'></head><body><style></style><center><h5 class='mb-15' style='margin-bottom:15px'>Seu cadastro sofreu modificações.</h5><p class='mb-5' style='margin-bottom:5px'>Tipo de acesso: <strong>{$label_tipo}</strong></p><table class='w-400' style='width:300px'><tbody><tr><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>ID</th><td style='border:1px solid silver;padding:5px'>{$id}</td></tr><tr class='high'><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>Nome</th><td style='border:1px solid silver;padding:5px'>{$usuario}</td></tr><tr><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>CPF/CNPJ</th><td style='border:1px solid silver;padding:5px'>{$for->cpf_cnpj($cpf_cnpj)}</td></tr><tr><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>E-mail</th><td style='border:1px solid silver;padding:5px'>{$email}</td></tr><tr class='high'><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>Fone fixo</th><td style='border:1px solid silver;padding:5px'>{$for->fone($fone_fixo)}</td></tr><tr><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>Fone celular</th><td style='border:1px solid silver;padding:5px'>{$for->fone($fone_cel)}</td></tr><tr class='high'><th style='background-color:silver;border:1px solid gray;padding:5px;text-shadow:1px 1px #fff'>Fone mensagens</th><td style='border:1px solid silver;padding:5px'>{$for->fone($fone_msg)}</td></tr></tbody></table><br><img src='http://pedeon.com.br/pointplus.com.br/adm/assets/img/logo_pp.png' style='height:64px'><br><i>www.pointplus.com.br</i><br></center></body></html>
                    ";

                    // $data['emailOk'] = (new Email($email, $subject, $msg, $from))->send();
                    $data['emailOk'] = (new Mail($email, $subject, $msg, $from))->send();
                endif;

            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'M_usu', 'usuarios', 'M');

        return $data;
    }
}
