<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../Db.php';
// require_once __DIR__ . '/../Flog.php';
require_once __DIR__ . '/../Sanitize.php';

class TblOperadores
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = false)
    {
        if ($conn):
            $this->conn = $conn;
        else:
            $this->conn = (new Db())->mysql_conn();
        endif;
    }

    // Public methods

    // C

    // R
    public function R_validate($auth)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => false,
        ];

        // Vars
        $id_operador = 0;
        $san = new Sanitize();
        $token = '';

        // Params
        if (isset($auth)):
            $id_operador = isset($auth['id']) ? $san->int($auth['id']) : 0;
            $token = isset($auth['token']) ? $san->str($auth['token']) : '';
        endif;

        // Validation
        if ($id_operador < 1):
            $resp['errors']['idOperador'] = 'Operador não indicado.';
        endif;

        if (empty($token)):
            $resp['errors']['token'] = 'Token não indicado.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    SELECT
                        COUNT(*)
                    FROM
                        operadores
                    WHERE
                        ope_pk = :id_operador
                        AND ope_c_token = :token
                    LIMIT
                        1
                    ;
                ';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':id_operador', $id_operador, PDO::PARAM_INT);
                $stmt->bindValue(':token', $token);
                $stmt->execute();
                $data['data'] = (boolean) $stmt->fetchColumn();

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_validate', 'operadores', 'R');

        return $data;
    }

    public function R_operador_cod($cod)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        // Vars
        $san = new Sanitize();

        // Params
        $cod = isset($cod) ? $san->int($cod) : 0;

        // Validation
        if ($cod < 1):
            $data['errors']['cod'] = 'Nenhum código indicado ou inválido.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    SELECT
                        *
                    FROM
                        operadores
                    WHERE
                        ope_i_cod = :cod
                        AND ope_b_ativo > 0
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':cod', $cod, PDO::PARAM_INT);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row):
                    $data['data'] = $row;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_operador_cod', 'operadores', 'R');

        return $data;
    }

    public function R_operador($id_operador, $auth)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        // $data['data']['id'] = $id_operador;
        // $data['data']['input'] = $input;

        // Vars
        $san = new Sanitize();

        // Params
        $id_operador = isset($id_operador) ? $san->int($id_operador) : 0;

        // Validation
        if ($id_operador < 1):
            $data['errors']['operador'] = 'Nenhum operador indicado.';
        endif;

        if (isset($auth)):
            $resp = $this->R_validate($auth);
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                $sql = '
                    SELECT
                        *
                    FROM
                        operadores
                    WHERE
                        ope_pk = :id_operador
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':id_operador', $id_operador, PDO::PARAM_INT);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row):
                    $data['data'] = $row;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_operador', 'operadores', 'R');

        return $data;
    }
    
    public function R_pag($input)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => [
                'rows' => null,
                'count' => 0,
            ],
        ];

        // $data['input'] = $input;

        // Vars
        $desc = false;
        $filter = '';
        $limit = 0;
        $offset = 0;
        $order_by = '';
        $page = 0;
        $san = new Sanitize();

        // Params
        if (isset($input)):
            if (isset($input['auth'])):
                $auth = $input['auth'];
            endif;

            if (isset($input['pag'])):
                $pag = $input['pag'];
                $filter = isset($pag['filter']) ? $san->str($pag['filter']) : '';
                $page = isset($pag['current']) ? $san->int($pag['current']) : 0;
                $limit = isset($pag['limit']) ? $san->int($pag['limit']) : 0;
                $offset = ($page - 1) * $limit;

                if (isset($pag['sorting'])):
                    $sorting = $pag['sorting'];
                    if (isset($sorting)):
                        $desc = isset($sorting['desc']) ? $sorting['desc'] : false;
                        $order_by = isset($sorting['field']) ? $san->str($sorting['field']) : '';
                    endif;
                endif;
            endif;
        endif;

        // Validation
        if (isset($auth)):
            $resp = $this->R_validate($auth);
            // $data['data'] = $resp;
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if (!empty($order_by)):
            $order_by = " ORDER BY {$order_by} ";
            if ($desc > 0):
                $order_by .= 'DESC ';
            endif;
        endif;

        if (!empty($filter)):
            $filter = " AND ( ope_c_operador LIKE '%{$filter}%' OR ope_i_cod = '{$filter}' ) ";
        else:
            $filter = '';
        endif;

        if ($page < 1):
            $data['errors']['page'] = 'Nenhuma página indicada ou inválida.';
        endif;

        if ($limit < 1):
            $data['errors']['limit'] = 'Nenhum limite de entradas indicado ou inválido.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                // Calcula total.
                $sql = "
                    SELECT
                        COALESCE(COUNT(ope_pk), 0)
                    FROM
                        operadores
                    WHERE
                        1 = 1
                        {$filter}
                    LIMIT
                        1
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $data['data']['count'] = (int) $stmt->fetchColumn();

                // Lazy load.
                $sql = "
                    SELECT
                        *
                    FROM
                        operadores
                    WHERE
                        1 = 1
                        {$filter}
                        {$order_by}
                    LIMIT
                        :limit
                    OFFSET
                        :offset
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows):
                    $data['data']['rows'] = $rows;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_ope_pag', 'operadores', 'R');

        return $data;
    }

    // U
    public function U_login($input)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        // $data['input'] = $input;

        // Vars
        $cod = 0;
        $senha = '';
        $operador = null;
        $san = new Sanitize();

        // Params
        if (isset($input)):
            $cod = isset($input['cod']) ? $san->int($input['cod']) : 0;
            $senha = isset($input['senha']) ? $san->str($input['senha']) : '';
        endif;

        // $data['cod'] = $cod;

        // Validation
        if ($cod < 1):
            $data['errors']['cod'] = 'Nenhum código indicado.';
        else:
            $resp = $this->R_operador_cod($cod);
            // $data['resp'] = $resp;
            if ($resp['ok']):
                if ($resp['data']):
                    $operador = $resp['data'];
                else:
                    $data['errors'] = $resp['errors'];
                endif;
            else:
                $data['errors']['cod'] = 'Erro lendo operador.';
            endif;
        endif;
        // $data['operador'] = $operador;

        if (empty($senha)):
            $data['errors']['senha'] = 'Nenhuma senha indicada.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                if ($operador):
                    // Grava token com duração de 1h.
                    // $operador['ope_c_token'] = bin2he(openssl_random_pseudo_bytes(16));
                    $operador['ope_c_token'] = md5(uniqid(rand(), true));
                    $operador['ope_dt_token'] = date('Y-m-d H:i:s', strtotime('+1 hour'));
                    $sql = '
                        UPDATE
                            operadores
                        SET
                            ope_c_token = :token,
                            ope_dt_token = :expiracao
                        WHERE
                            ope_pk = :id_operador
                        LIMIT
                            1
                        ;';

                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindValue(':id_operador', $operador['ope_pk'], PDO::PARAM_INT);
                    $stmt->bindValue(':token', $operador['ope_c_token']);
                    $stmt->bindValue(':expiracao', $operador['ope_dt_token']);
                    $stmt->execute();

                    $data['ok'] = true;

                    if (password_verify($senha, $operador['ope_c_senha'])):
                        $data['data'] = $operador;
                    endif;
                endif;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;
        
        // (new Flog())->log($data, 'U_login', 'operadores', 'U');

        return $data;
    }
}
