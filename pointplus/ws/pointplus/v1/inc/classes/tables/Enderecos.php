<?php
// date_default_timezone_set('America/Sao_Paulo');

require_once __DIR__ . '/../../consts.php';
// require_once __DIR__ . '/../Db.php';
// require_once __DIR__ . '/../Mail.php';
// require_once __DIR__ . '/../Email.php';
// require_once __DIR__ . '/../Format.php';
// // require_once __DIR__ . '/../Flog.php';
// require_once __DIR__ . '/../Sanitize.php';
// require_once __DIR__ . '/../Str.php';
// require_once __DIR__ . '/../Validate.php';
// require_once __DIR__ . '/../tables/Operadores.php';

class TblEnderecos
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = false)
    {
        if ($conn):
            $this->conn = $conn;
        else:
            $this->conn = (new Db())->mysql_conn();
        endif;
    }

    // Public methods

    // C

    // R
    public function R_pag($input, $id_usuario)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => [
                'rows' => null,
                'count' => 0,
            ],
        ];

        $data['input'] = $input;
        $data['id_usuario'] = $id_usuario;

        // Vars
        $auth = null;
        $desc = false;
        $filter = '';
        $limit = 0;
        $offset = 0;
        $order_by = '';
        $page = 0;
        $san = new Sanitize();

        // Params
        $tipo = isset($tipo) ? $san->str($tipo) : '';
        if (isset($input)):
            if (isset($input['auth'])):
                $auth = $input['auth'];
            endif;

            if (isset($input['pag'])):
                $pag = $input['pag'];
                $filter = isset($pag['filter']) ? $san->str($pag['filter']) : '';
                $page = isset($pag['current']) ? $san->int($pag['current']) : 0;
                $limit = isset($pag['limit']) ? $san->int($pag['limit']) : 0;
                $offset = ($page - 1) * $limit;

                if (isset($pag['sorting'])):
                    $sorting = $pag['sorting'];
                    if (isset($sorting)):
                        $desc = isset($sorting['desc']) ? $sorting['desc'] : false;
                        $order_by = isset($sorting['field']) ? $san->str($sorting['field']) : '';
                    endif;
                endif;
            endif;
        endif;

        // Validation
        if (isset($auth)):
            $resp = (new TblOperadores())->R_validate($auth);
            // $data['data'] = $resp;
            if (!$resp['data']):
                $data['errors']['auth'] = 'Acesso negado.';
            endif;
        else:
            $data['errors']['auth'] = 'Credenciais não indicadas.';
        endif;

        if ($id_usuario < 1):
            $resp['errors']['idUsuario'] = 'Nenhum usuário indicado.';
        endif;

        if (!empty($order_by)):
            $order_by = " ORDER BY {$order_by} ";
            if ($desc > 0):
                $order_by .= 'DESC ';
            endif;
        endif;

        if (!empty($filter)):
            $filter = " AND ( end_c_logradouro LIKE '%{$filter}%' ) ";
        else:
            $filter = '';
        endif;

        if ($page < 1):
            $data['errors']['page'] = 'Nenhuma página indicada ou inválida.';
        endif;

        if ($limit < 1):
            $data['errors']['limit'] = 'Nenhum limite de entradas indicado ou inválido.';
        endif;

        if (!$this->conn):
            $data['errors']['conexao'] = BAD_CONN;
        endif;

        if (empty($data['errors'])):
            try {
                // Calcula total.
                $sql = "
                    SELECT
                        COALESCE(COUNT(end_pk), 0)
                    FROM
                        enderecos
                    WHERE
                        end_fk_usuario = :id_usuario
                        {$filter}
                    LIMIT
                        1
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
                $stmt->execute();
                $data['data']['count'] = (int) $stmt->fetchColumn();

                // Lazy load.
                $sql = "
                    SELECT
                        *
                    FROM
                        enderecos
                    WHERE
                        end_fk_usuario = :id_usuario
                        {$filter}
                        {$order_by}
                    LIMIT
                        :limit
                    OFFSET
                        :offset
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
                $stmt->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows):
                    $data['data']['rows'] = $rows;
                endif;

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        endif;

        // (new Flog())->log($data, 'R_end_pag', 'enderecos', 'R');

        return $data;
    }

    // U

    // D
}
