<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

// Load Composer's autoloader
require_once __DIR__ . '/../../vendor/autoload.php';
// require_once __DIR__ . '/../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php'; // v 5.x

class Email
{
    // Privates
    private $_from;
    private $_to;
    private $_subject;
    private $_msg;
    private $_headers = '';

    // constructor
    public function __construct($to, $subject, $msg, $from)
    {
        $this->_to = $to;
        $this->_subject = $subject;
        $this->_msg = $msg;
        $this->_from = $from;
    }

    public function send()
    {
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            // $mail->Charset = 'UTF-8';
            $mail->setLanguage('pt_br');
            
            //Server settings
            // $mail->SMTPDebug = 2; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            // $mail->Host = 'smtp1.example.com;smtp2.example.com'; // Specify main and backup SMTP servers
            // $mail->Host = 'smtp.sendgrid.net';
            $mail->Host = 'pedeon.com.br';
            $mail->SMTPAuth = false; // Enable/disable SMTP authentication
            // $mail->Username = 'user@example.com'; // SMTP username
            // $mail->Password = 'secret'; // SMTP password
            // $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            // $mail->Port = 587; // TCP port to connect to
            
            //Recipients
            $mail->setFrom($this->_from, 'Point Plus');
            $mail->addAddress($this->_to, ''); // Add a recipient
            // $mail->addAddress('ellen@example.com'); // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name

            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = $this->_subject;
            $mail->Body = $this->_msg;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            // echo 'Message has been sent';
            return '';
        } catch (Exception $e) {
            // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            return $mail->ErrorInfo;
        } //  try-catch
    }
}
