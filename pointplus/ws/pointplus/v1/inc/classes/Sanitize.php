<?php
class Sanitize
{
    /*
    Sanitiza inteiro.
    @Input:
    {$val} <STRING> ou <INTEGER>.
    @Output:
    <INTEGER> Valor convertido/sanitizado. 0 se inválido.
     */
    public function int($val)
    {
        if (is_string($val)) {
            $val = intval($val);
        } // if

        if (filter_var($val, FILTER_VALIDATE_INT) === false) {
            return 0;
        } else {
            return (int) $val;
        } // else
    }

    /*
    Sanitiza float.
    @Input:
    {$val} <STRING> ou <FLOAT>.
    @Output:
    <FLOAT> Valor convertido/sanitizado. 0.00 se inválido.
     */
    public function flt($val)
    {
        $flt = str_replace(',', '.', $val);

        if (filter_var($val, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) === false) {
            return 0;
        } else {
            return $val == 'undefined' ? 0.00 : $val;
        } // else
    }

    /*
    Sanitiza string.
    @Input:
    {$str} <STRING>.
    @Output:
    <STRING> Valor convertido/sanitizado. '' se inválido.
     */
    public function str($str)
    {
        $str = urldecode($str);
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        } // if

        if (filter_var($str, FILTER_SANITIZE_STRING) === false) {
            return '';
        } else {
            return $str == 'undefined' ? '' : trim($str);
        } // else
    }
}
