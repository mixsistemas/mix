<?php

const BAD_CONN = 'Erro de conexão com banco de dados';
const NOME_SISTEMA = 'Point Plus';

/*
// Remote paths
const R_ASSETS_BARCODES = 'https://pedeon.com.br/assets/img/__/';
const R_ASSETS_FINALIZADORAS = 'https://pedeon.com.br/assets/img/finalizadoras/';
const R_ASSETS_GALERIA = 'https://pedeon.com.br/assets/img/_/';
const R_ASSETS_LOJA = 'https://pedeon.com.br/assets/img/lojas/';
const R_API_UPDATES = 'https://pedeon.com.br/ws/hypico/updates/';
*/

const UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const LOWER = 'abcdefghijklmnopqrstuvwxyz';
const NUM = '1234567890';

// date_default_timezone_set('Africa/Nairobi');
const TIMEZONES_UFS = array(
    'AC' => 'America/Rio_branco',
    'AL' => 'America/Maceio',
    'AP' => 'America/Belem',
    'AM' => 'America/Manaus',
    'BA' => 'America/Bahia',
    'CE' => 'America/Fortaleza',
    'DF' => 'America/Sao_Paulo',
    'ES' => 'America/Sao_Paulo',
    'GO' => 'America/Sao_Paulo',
    'MA' => 'America/Fortaleza',
    'MT' => 'America/Cuiaba',
    'MS' => 'America/Campo_Grande',
    'MG' => 'America/Sao_Paulo',
    'PR' => 'America/Sao_Paulo',
    'PB' => 'America/Fortaleza',
    'PA' => 'America/Belem',
    'PE' => 'America/Recife',
    'PI' => 'America/Fortaleza',
    'RJ' => 'America/Sao_Paulo',
    'RN' => 'America/Fortaleza',
    'RS' => 'America/Sao_Paulo',
    'RO' => 'America/Porto_Velho',
    'RR' => 'America/Boa_Vista',
    'SC' => 'America/Sao_Paulo',
    'SE' => 'America/Maceio',
    'SP' => 'America/Sao_Paulo',
    'TO' => 'America/Araguaia',
);

const TIPOS_USU = array(
    'rep' => 'Representante',
    'pro' => 'Profissional',
    'par' => 'Parceiro',
    'apr' => 'Atendente de profissional',
    'apa' => 'Atendente de parceiro',
    'cli' => 'Cliente'
);
