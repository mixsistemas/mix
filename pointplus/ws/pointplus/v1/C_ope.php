<?php
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . '/inc/classes/Db.php';
require_once __DIR__ . '/inc/classes/Flog.php';

$data = [
    'errors' => [],
    'ok' => false,
    'data' => 0,
];

$nome = 'admin';
$cod = 100;
$hash = password_hash('teste', PASSWORD_DEFAULT);

$conn = (new Db())->mysql_conn();
try {
    if ($conn) {
        $conn->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
        $conn->beginTransaction();

        $sql = "
            INSERT INTO
                operadores (
                    ope_b_adm,
                    ope_c_operador,
                    ope_c_senha,
                    ope_i_cod
                ) VALUES (
                    1,
                    :nome,
                    :hash,
                    :cod
                )
                ;";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':nome', $nome);
        $stmt->bindValue(':hash', $hash);
        $stmt->bindValue(':cod', $cod, PDO::PARAM_INT);
        $stmt->execute();
        $data['data'] = (int) trim($conn->lastInsertId());

        $conn->commit();
        $data['ok'] = true;
    } // if
} catch (PDOException $e) {
    $data['errors']['pdo'] = $e->getMessage();
    $conn->rollback();
} // try-catch

(new Flog())->log($data, 'C_ope_adm', 'operadores', 'C');

return $data;
