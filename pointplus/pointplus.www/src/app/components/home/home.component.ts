//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
// import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import * as $ from "jquery";
//#endregion

//#region app models
// import { ICredenciais } from "../../modules/_shared/_models/_interfaces";
// import { FormValidation } from "../../modules/_shared/_models/_classes";
//#endregion

@Component({
  selector: "pp-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss", "./welcome.scss"]
})
export class HomeComponent implements AfterViewInit {
  //#region lfiecycles
  ngAfterViewInit() {
    $(window).on("scroll", () => this.chkScrollPos());
    this.chkScrollPos();

    $(".navbar-nav>li>a, .login-button").on("click", function() {
      // console.log('clicked');
      // $('.navbar-collapse').collapse('hide');
      $(".navbar-collapse.collapse").removeClass("show");
    });
  }
  //#endregion

  //#region functions
  chkScrollPos() {
    // console.log('scrolling');
    if ($(window).scrollTop()) {
      // console.log('NOT at the top');
      $(".header_area").addClass("sticky");
      $(".welcome-heading").addClass("mt-100");
      // $("#logo").attr("src", "assets/img/logo_pp_custom.png");
    } else {
      // console.log('at the top');
      $(".header_area").removeClass("sticky");
      $(".welcome-heading").removeClass("mt-100");
      // $("#logo").attr("src", "assets/img/logo_pp.png");
    } //  else
  }
  //#endregion

  //#region methods
  onScrollTo(dest: string) {
    // console.log(dest);
    if (dest) {
      switch (dest.substring(1)) {
        case "cliente":
          setTimeout(() => {
            // this.onClienteFocusEvent.emit(true);
          }, 10);
          break;

        case "contate_nos":
          setTimeout(() => {
            // this.onContatoFocusEvent.emit(true);
          }, 10);
          break;

        case "login":
          setTimeout(() => {
            // this.onLoginFocusEvent.emit(true);
          }, 10);
          break;
      } // switch

      $("html, body").animate({ scrollTop: $(dest).offset().top }, 500);
    } // if
  }
  //#endregion
}
/*
  //#region publics
  auth: ICredenciais = {
    id: "",
    senha: "",
    lembrarId: true
  };
  fv: FormValidation;
  //#endregion

  //#region methods
  onClienteFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  onContatoFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  onLoginFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  //#endregion

  //#region functions
  chkScrollPos() {
    // console.log('scrolling');
    if ($(window).scrollTop()) {
      // console.log('NOT at the top');
      $(".header_area").addClass("sticky");
      $(".welcome-heading").addClass("mt-100");
      // $("#logo").attr("src", "assets/img/logo_pp_custom.png");
    } else {
      // console.log('at the top');
      $(".header_area").removeClass("sticky");
      $(".welcome-heading").removeClass("mt-100");
      // $("#logo").attr("src", "assets/img/logo_pp.png");
    } //  else
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();
  }

  ngAfterViewInit() {
    $(window).on("scroll", () => this.chkScrollPos());
    this.chkScrollPos();

    $(".navbar-nav>li>a, .login-button").on("click", function() {
      // console.log('clicked');
      // $('.navbar-collapse').collapse('hide');
      $(".navbar-collapse.collapse").removeClass("show");
    });
  }
  //#endregion

  //#region methods

  onContactSubmit(f: NgForm): void {
    console.log(f.value);
  }

  onSubmit(f: NgForm): void {
    console.log(f.value);
    f.resetForm();
    // this.fv.setErrosApi({ id: "id errado", senha: "senha invalida" });
    /*
    this._glbServ.busy = true;
    this._authServ.L_login(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.onSelected.emit(resp.data);
          if (!resp.data) {
            this.auth = { cod: "", senha: "" };
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  /* 
  //#region functions
  /* private _isMobile() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    // var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    // console.log(w);
    return w < 1024; 
  }
  //#endregion
 */
