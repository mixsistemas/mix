//#region ng
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
//#endregion

//#region app components
import { APP_ROUTING } from "./app.routing";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./components";
//#endregion

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [APP_ROUTING, BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
