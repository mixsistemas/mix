//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region app services
import { BsCoreService } from "./_services";
//#endregion

@NgModule({
  imports: [CommonModule],
  providers: [
    // services
    BsCoreService,
  ] // these should be singleton
})
export class BsCoreModule {}
