//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app components
import { HomeComponent } from "./components";
//#endregion

//#region routes
const APP_ROUTES: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home",
    component: HomeComponent
  },/* 
  {
    path: "profissionais",
    component: ProfissionaisComponent
  },
  {
    path: "consumidores",
    component: ConsumidoresComponent
  },
  {
    path: "parceiros",
    component: ParceirosComponent
  }, */
  { path: "**", pathMatch: "full", redirectTo: "/home" }
];
//#endregion

//#region routing
export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(
  APP_ROUTES,
  { useHash: true }
);
//#endregion
