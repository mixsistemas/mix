export interface IApiResponse {
  ok: boolean;
  errors: string[];
  data: any;
  pj?: any;
}
