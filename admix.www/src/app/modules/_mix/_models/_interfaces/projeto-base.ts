export interface IProjetoBase {
  prj_pk: number;
  prj_c_bg_color: string;
  prj_c_fg_color: string;
  prj_c_descricao: string;
  prj_c_icone: string;
  prj_c_logo: string;
  prj_c_projeto: string;
  prj_c_rota: string;
}
