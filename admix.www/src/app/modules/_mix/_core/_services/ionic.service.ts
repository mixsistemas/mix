//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region ionic
import { AlertController, ToastController } from "@ionic/angular";
//#endregion

@Injectable()
export class IonicService {
  //#region contructor
  constructor(
    private _alertCtrl: AlertController,
    private _toastCtrl: ToastController
  ) {}
  //#endregion

  //#region functions
  async toast(msg: string, color: string = "dark", dur: number = 3000) {
    const toast = await this._toastCtrl.create({
      message: msg,
      duration: dur,
      color: color,
      translucent: true
    });
    toast.present();
  }

  async alert(
    header: string = "",
    subheader: string = "",
    message: string = ""
  ) {
    const alert = await this._alertCtrl.create({
      header: header,
      subHeader: subheader,
      message: message,
      buttons: ["OK"]
    });

    await alert.present();
  }
  //#endregion
}
