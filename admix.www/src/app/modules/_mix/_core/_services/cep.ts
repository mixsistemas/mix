//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

@Injectable()
export class CepService {
  //#region models
  readonly VIA_CEP_API = "https://viacep.com.br/ws";
  //#endregion

  //#region constructor
  constructor(private _http: HttpClient) {}
  //#endregion

  // https://viacep.com.br/ws/01001000/json/
  cep(cep: string) {
    const URL = `${this.VIA_CEP_API}/${cep}/json/`;
    return this._http.get(URL);
  }

  // https://viacep.com.br/ws/RS/Porto%20Alegre/Domingos/json/
  logradouros(estado: string, cidade: string, logradouro: string) {
    const URL = `${this.VIA_CEP_API}/${estado}/${cidade}/${logradouro}/json/`;
    // console.log(URL);
    return this._http.get(URL);
  }
}
