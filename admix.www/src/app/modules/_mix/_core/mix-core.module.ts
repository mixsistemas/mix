//#region ng
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
//#endregion

//#region app services
import { CepService, IonicService, UtilsService } from "./_services";
//#endregion

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [
    // services
    CepService,
    IonicService,
    UtilsService
  ] // these should be singleton
})
export class MixCoreModule {}
