//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region app directives
import { FocusDirective } from "./_directives";
//#endregion

@NgModule({
  imports: [CommonModule],
  declarations: [FocusDirective],
  exports: [FocusDirective]
})
export class MixSharedModule {}
