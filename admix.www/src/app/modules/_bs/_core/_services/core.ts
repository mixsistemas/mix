//#region ng
import { EventEmitter, Injectable } from "@angular/core";
//#endregion

//#region 3rd
// import { NgxSpinnerService } from 'ngx-spinner';
//#endregion

@Injectable()
export class BsCoreService {
  static onSidemenuCloseEvent: EventEmitter<boolean> = new EventEmitter<
    boolean
  >(false);

  /*
  //#region busy
  _busy: boolean;
  get busy(): boolean {
    return this._busy;
  }
  set busy(status: boolean) {
    // console.log(`busy: ${status}`);
    this._busy = status;
    if (this.busy) {
      this._spinner.show();
    } else {
      this._spinner.hide();
    } // else
    this.onBusyChangedEvent.emit(status);
  }
  onBusyChangedEvent = new EventEmitter<boolean>(false);
  //#endregion

  //#region modal
  private _modal: number = 0;
  get modal(): number {
    return this._modal;
  }
  modalInc(): number {
    this._modal += 1;
    this.onModalChangedEvent.emit(this._modal);
    console.log(`modal: ${this._modal}`);
    return this._modal;
  }
  modalDec(): number {
    if (this._modal > 0) {
      this._modal -= 1;
    } // if
    this.onModalChangedEvent.emit(this._modal);
    console.log(`modal: ${this._modal}`);
    return this._modal;
  }
  onModalChangedEvent = new EventEmitter<number>();
  //#endregion
  /*
    //#region modal
    _modal: boolean;
    get modal(): boolean {
        return this._modal;
    }
    set modal(status: boolean) {
        console.log(`modal: ${status}`);
        this._modal = status;
        this.onModalChangedEvent.emit(status);
    }
    onModalChangedEvent = new EventEmitter<boolean>(true);
    //#endregion
    */

  //#region constructor
  //   constructor(private _spinner: NgxSpinnerService) {}
  constructor() {}
  //#endregion

  //#region lifecycles
  /* ngOnInit() {
    // this.busy = false;
    // this._modal = 0;
  } */
  //#endregion
}
