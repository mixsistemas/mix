//#endregion
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
import { AppRoutingModule } from "./app-routing.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import { HomeComponent } from "./components";
//#endregion

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
