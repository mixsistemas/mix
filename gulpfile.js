// vars
var gulp = require("gulp");

var src = {
  assets: "_assets/**/*.*",
  bs: "_bs/**/*.*",
  mix: "_mix/**/*.*"
  // browser: "_browser/**/*.*",
  // shared: "_shared/**/*.*"
};

var dest = {
  assets: {
    admix: "./admix.www/src/assets/",
    descontante: "./descontante/descontante.www/src/assets",
    hypico: "./hypico/hypico.www/src/assets/",
    mixsistemas: "./mixsistemas.www/src/assets/",
    pointplus: "./pointplus/pointplus.www/src/assets"
  },
  bs: {
    admix: "./admix.www/src/app/modules/_bs/",
    descontante: "./descontante/descontante.www/src/app/modules/_bs/",
    hypico: "./hypico/hypico.www/src/app/modules/_bs/",
    mixsistemas: "./mixsistemas.www/src/app/modules/_bs/",
    pointplus: "./pointplus/pointplus.www/src/app/modules/_bs/"
  },
  mix: {
    // pointplus: "./pointplus/pointplus.www/src/app/modules/_bs/",
    // descontante: "./descontante/descontante.www/src/app/modules/_bs/",
    admix: "./admix.www/src/app/modules/_mix/",
    hypico: "./hypico/hypico.www/src/app/modules/_mix/",
    pedeon_app: "./pedeon/pedeon.app/src/app/modules/_mix/"
    // mixsistemas: "./mixsistemas/mixsistemas.www/src/app/modules/_bs/"
  }
  /*
  browser: {
    adm: "./adm/src/app/modules/_browser/",
    cardapio: "./cardapio/src/app/modules/_browser/",
    pdv: "./pdv/src/app/modules/_browser/",
    retag: "./retag/src/app/modules/_browser/",
    setup: "./setup/src/app/modules/_browser/"
  },
  shared: {
    adm: "./adm/src/app/modules/",
    cardapio: "./cardapio/src/app/modules/",
    pdv: "./pdv/src/app/modules/",
    retag: "./retag/src/app/modules/",
    setup: "./setup/src/app/modules/"
  }
  */
};

// tasks
gulp.task("assets", function() {
  gulp
    .src(src.assets)
    .pipe(gulp.dest(dest.assets.admix))
    .pipe(gulp.dest(dest.assets.pointplus))
    .pipe(gulp.dest(dest.assets.descontante))
    .pipe(gulp.dest(dest.assets.mixsistemas))
    .pipe(gulp.dest(dest.assets.hypico));
});

gulp.task("bs", function() {
  gulp
    .src(src.bs)
    .pipe(gulp.dest(dest.bs.admix))
    .pipe(gulp.dest(dest.bs.pointplus))
    .pipe(gulp.dest(dest.bs.descontante))
    .pipe(gulp.dest(dest.bs.mixsistemas))
    .pipe(gulp.dest(dest.bs.hypico));
});

gulp.task("mix", function() {
  gulp
    .src(src.mix)
    .pipe(gulp.dest(dest.mix.admix))
    .pipe(gulp.dest(dest.mix.hypico))
    .pipe(gulp.dest(dest.mix.pedeon_app));
});

/*
gulp.task("browser", function() {
  gulp
    .src(src.browser)
    .pipe(gulp.dest(dest.browser.adm))
    .pipe(gulp.dest(dest.browser.cardapio))
    .pipe(gulp.dest(dest.browser.pdv))
    .pipe(gulp.dest(dest.browser.retag))
    .pipe(gulp.dest(dest.browser.setup));
});

gulp.task("shared", function() {
  gulp
    .src(src.shared)
    .pipe(gulp.dest(dest.shared.adm))
    .pipe(gulp.dest(dest.shared.cardapio))
    // .pipe(gulp.dest(dest.shared.pedeon))
    .pipe(gulp.dest(dest.shared.pdv))
    .pipe(gulp.dest(dest.shared.retag))
    .pipe(gulp.dest(dest.shared.setup));
});
*/
// watch task
gulp.task("default", function() {
  gulp.watch(src.assets, ["assets"]);
  gulp.watch(src.bs, ["bs"]);
  gulp.watch(src.mix, ["mix"]);
  // gulp.watch(src.browser, ["browser"]);
  // gulp.watch(src.shared, ["shared"]);
});
